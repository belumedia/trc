<?php namespace TheRightCrowd;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model {

	protected $primaryKey = "id";

   protected static function boot() {
      parent::boot();

      static::creating(function ($model) {
         $model->{$model->getKeyName()} = (string) $model->generateNewId();
      });
   }

   public function generateNewId() {
      return uniqid(rand());
   }

   /* Scopes */
   public function scopeForStartup($query, $start_up_id) {
      return $query->where('start_up_id', '=', $start_up_id);
   }

   public function get_embed_url(){
		 $parts = $this->video_url;
		 if (strpos($parts, 'watch?v=') !== false) {
			 $arr = explode('=', $parts);
			 $important = $arr[1];
			 return "https://www.youtube.com/embed/".$important."?rel=0";
		 } else {
			 $parts = explode("/", $this->video_url);
       return "https://www.youtube.com/embed/".$parts[3]."?rel=0";
		 }
   }

}
