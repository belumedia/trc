<?php namespace TheRightCrowd;

use Illuminate\Database\Eloquent\Model;

class StartUpStatus extends Model {

	public static function get_status_for_select() {
		
		$data = array();

		$sectors = Self::all();
		foreach($sectors as $sector){
			$data[$sector->value] = $sector->name;
		}
		
		return $data;
		
	}

}
