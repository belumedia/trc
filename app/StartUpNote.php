<?php namespace TheRightCrowd;

use Illuminate\Database\Eloquent\Model;

class StartUpNote extends Model {

	//
   public function author()
   {
      return $this->belongsTo('TheRightCrowd\User', 'user_id', 'id');
   }

}
