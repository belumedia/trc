<?php namespace TheRightCrowd;

use Illuminate\Database\Eloquent\Model;

class QuestionsAndAnswers extends Model {

	/* Scopes */
   public function scopeForStartup($query, $start_up_id) {
      return $query->where('start_up_id', '=', $start_up_id);
   }
   
   
   public function format_answer(){
	   return str_replace("\n", "</p><p>", $this->answer);
   }
   
   
  public function getPerson() {
  
     $person = User::find($this->question_by_user_id);
     return $person->first_name;
     
  }
  

}
