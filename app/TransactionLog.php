<?php namespace TheRightCrowd;

use Illuminate\Database\Eloquent\Model;

class TransactionLog extends Model {

	//
   protected $fillable = ['from_id','from_name','to_id','to_name','description','amount','stripe_charge_id','stripe_refund_id',]; 

}
