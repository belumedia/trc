<?php namespace TheRightCrowd;

use Validator;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['first_name', 'last_name', 'street', 'street_2', 'city', 'county', 'postcode', 'email', 'telephone', 'password', 'accept_terms', 'bonus'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
		protected $hidden = ['password', 'remember_token'];

	public $can_invest = false;
	public $investor_type = "";
	public $investor_type_text = "";
	public $investor_type_set = false;
	public $investor_terms_specific = false;
	public $investor_terms_general = false;

	/* --------------------------------------------------------------------------------------------
		SIPP Validation
	-------------------------------------------------------------------------------------------- */
	public static $sipp_rules = [
		'first_name' => 'required',
		'last_name' => 'required',
		'email' => 'required|email|unique:users',
		'street' => 'required',
		'city' => 'required',
		'county' => 'required',
		'postcode' => 'required',
	];

	public static $sipp_rules_update = [
		'first_name' => 'required',
		'last_name' => 'required',
		'email' => 'required|email',
		'street' => 'required',
		'city' => 'required',
		'county' => 'required',
		'postcode' => 'required',
	];


	public $sipp_errors;

	/* validation method */
	public function is_valid_sipp_model(){
		$pass = false;
		$validation = Validator::make($this->attributes, $this->rules);
		if($validation->passes()) $pass = true;
		$this->sipp_errors = $validation->messages();
		return $pass;
	}


	/* --------------------------------------------------------------------------------------------
		Date Object
	-------------------------------------------------------------------------------------------- */
	public function updated_date_object() {
		return new \DateTime($this->updated_at);
	}

	public function created_date_object() {
		return new \DateTime($this->created_at);
	}

	/* --------------------------------------------------------------------------------------------
		Get Investment Status
	-------------------------------------------------------------------------------------------- */
	public function get_user_investment_status() {

		if($this->is_high_networth==1){
			$this->investor_type = "HIGH_NET_WORTH";
			$this->investor_type_text = "High Net Worth Investor";
			$this->investor_type_set = true;
		}

		if($this->is_professional==1){
			$this->investor_type = "PROFESSIONAL";
			$this->investor_type_text = "Professional Investor";
			$this->investor_type_set = true;
		}

		if($this->is_restricted==1){
			$this->investor_type = "RESTRICTED";
			$this->investor_type_text = "Restricted Investor";
			$this->investor_type_set = true;
		}

		if($this->accept_terms_high_networth==1){
			$this->investor_terms_specific = true;
		}

		if($this->accept_terms_professional==1){
			$this->investor_terms_specific = true;
		}

		if($this->accept_general_terms==1){
			$this->investor_terms_specific = true;
		}

		if($this->accept_general_terms==1){
			$this->investor_terms_general = true;
		}

		if($this->investor_terms_general&&$this->investor_terms_specific){
			$this->can_invest = true;
		}

		return $this->can_invest;
	}


	public function get_investment_status(){

		$result = "Not Investing";

		$this->get_user_investment_status();
		if($this->can_invest) $result = $this->investor_type_text;

		return $result;

	}

	/* Start-Up Methods */

	public function count_start_ups(){

		return StartUps::where('user_id', '=', $this->id)->count();

	}

	public function get_start_ups() {

		return StartUps::where('user_id', '=', $this->id)->get();

	}

	/* Start-Up Methods */
   public function count_investments(){

		return StartupInvestment::where('investor_id', '=', $this->id)->count();

	}

	public function get_investments() {

		return StartupInvestment::where('investor_id', '=', $this->id)->orderBy('updated_at', 'DESC')->get();

	}

}
