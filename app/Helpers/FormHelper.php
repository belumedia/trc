<?php
	
class FormHelper {
	
	
	public static function options_date_day() {
		
		$profile_day_options = [];
		for($i=1; $i<32; $i++){
			$profile_day_options[$i] = $i;
		}
		
		return $profile_day_options;
   	}
   
   public static function options_date_month() {
      
      $profile_month_options = [];
      $profile_month_options[1] = "January";
      $profile_month_options[2] = "February";
      $profile_month_options[3] = "March";
      $profile_month_options[4] = "April";
      $profile_month_options[5] = "May";
      $profile_month_options[6] = "June";
      $profile_month_options[7] = "July";
      $profile_month_options[8] = "August";
      $profile_month_options[9] = "September";
      $profile_month_options[10] = "October";
      $profile_month_options[11] = "November";
      $profile_month_options[12] = "December";
      
      return $profile_month_options;
   	}
   
	public static function options_date_year() {
		
		$profile_year_options = [];
		for($i=date('Y'); $i>date('Y')-100; $i--){
			$profile_year_options[$i] = $i;
		}
		
		return $profile_year_options;
	}
	
	
	public static function options_investment_period() {
		
		$profile_year_options = [];
		for($i=date('Y'); $i>date('Y')-100; $i--){
			$profile_year_options[$i] = $i;
		}
		
		return $profile_year_options;
	}
	
	public static function options_date_year_funding_expire() {
		
		$profile_year_options = [];
		for($i=date('Y')+5; $i>date('Y')-5; $i--){
			$profile_year_options[$i] = $i;
		}
		
		return $profile_year_options;
	}
	
	
}