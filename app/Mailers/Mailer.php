<?php namespace TheRightCrowd\Mailers;

abstract class Mailer {
 
   public static function send($user, $subject, $view, $data = []){
   
      \Mail::send($view, $data, function($message) use ($user, $subject) {
         $message->to($user->email, $user->first_name." ".$user->last_name);
         $message->subject($subject);
      });
      
   }
   
}