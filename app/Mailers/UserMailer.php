<?php namespace TheRightCrowd\Mailers;

class UserMailer extends Mailer {


   /* ------------------------------------------------------------------------------------
    *  Welcome Email
   ------------------------------------------------------------------------------------ */
   public static function send_welcome_email($receiver, $username, $password) {

      $view = "emails.welcome";
      $subject = "Welcome to The Right Crowd";

      $data['username'] = $username;

      self::send($receiver, $subject, $view, $data);
   }


   /* ------------------------------------------------------------------------------------
    *  Notify Investor Funding Confirmed
   ------------------------------------------------------------------------------------ */
   public static function send_investment_confirmed_investor($receiver, $amount, $company) {

      $view = "emails.invesment_confirmed_investor";
      $subject = "Investment Confirmation";

      $data['name'] = $receiver->first_name;
      $data['amount'] = $amount;
      $data['company'] = $company;

      self::send($receiver, $subject, $view, $data);
   }

   /* ------------------------------------------------------------------------------------
    *  Notify Question Asked
   ------------------------------------------------------------------------------------ */
   public static function send_question_asked($receiver, $question, $company) {

      $view = "emails.company_question";
      $subject = "Someone has asked a question";

      $data['name'] = $receiver->first_name;
      $data['question'] = $question;
      $data['company'] = $company;

      self::send($receiver, $subject, $view, $data);
   }


   /* ------------------------------------------------------------------------------------
    *  Notify Startup Funding Success
   ------------------------------------------------------------------------------------ */
   public static function send_investment_success_startup($receiver, $investment_total, $company) {

      $view = "emails.invesment_success_start_up";
      $subject = "Congratulations your investment has been successful!";

      $data['name'] = $receiver->first_name;
      $data['investment_total'] = $investment_total;
      $data['company'] = $company;

      self::send($receiver, $subject, $view, $data);
   }


   /* ------------------------------------------------------------------------------------
    *  Notify Investor Funds have Transfered
   ------------------------------------------------------------------------------------ */
   public static function send_investment_transfered_investor($receiver, $amount, $company) {

      $view = "emails.invesment_transfered_investor";
      $subject = "Your investment has been transferred";

      $data['name'] = $receiver->first_name;
      $data['amount'] = $amount;
      $data['company'] = $company;

      self::send($receiver, $subject, $view, $data);
   }

   /* ------------------------------------------------------------------------------------
    *  Notify Investor Transfer has been reversed
	------------------------------------------------------------------------------------ */
   public static function send_investment_reversed_investor($receiver, $amount, $company) {

      $view = "emails.invesment_reversed_investor";
      $subject = "Your investment has been refunded";

      $data['name'] = $receiver->first_name;
      $data['amount'] = $amount;
      $data['company'] = $company;

      self::send($receiver, $subject, $view, $data);
   }


   /* ------------------------------------------------------------------------------------
    *  Notify Investor Funding Failed
   ------------------------------------------------------------------------------------ */
   public static function send_investment_failed_investor($receiver, $company) {

      $view = "emails.investment_failed_investor";
      $subject = "Your investment is not proceeding";

      $data['name'] = $receiver->first_name;
      $data['company'] = $company;

      self::send($receiver, $subject, $view, $data);
   }


   /* ------------------------------------------------------------------------------------
    *  Notify Start-Up Funding Failed
   ------------------------------------------------------------------------------------ */
   public static function send_investment_failed_startup($receiver, $company) {

      $view = "emails.investment_failed_startup";
      $subject = "Unfortunately funding has been unsuccessful";

      $data['name'] = $receiver->first_name;
      $data['company'] = $company;

      self::send($receiver, $subject, $view, $data);
   }


   /* ------------------------------------------------------------------------------------
    *  SIPP Welcome Email
   ------------------------------------------------------------------------------------ */
   public static function send_sipp_welcome_email($receiver, $username, $password) {

      $view = "emails.welcome";
      $subject = "Welcome to The Right Crowd";

      $data['username'] = $username;
      $data['password'] = $password;

      self::send($receiver, $subject, $view, $data);
   }



  }
