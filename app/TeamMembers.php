<?php

namespace TheRightCrowd;

use Illuminate\Database\Eloquent\Model;

class TeamMembers extends Model {

   protected $primaryKey = "id";

   protected static function boot() {
      parent::boot();

      static::creating(function ($model) {
         $model->{$model->getKeyName()} = (string) $model->generateNewId();
      });
   }

   public function generateNewId() {
      return uniqid(rand());
   }

   /* Scopes */

   public function scopeForStartup($query, $start_up_id) {
      return $query->where('start_up_id', '=', $start_up_id);
   }

}
