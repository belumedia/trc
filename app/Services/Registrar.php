<?php namespace TheRightCrowd\Services;

use TheRightCrowd\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;
use Illuminate\Support\Facades\Session;

use TheRightCrowd\Mailers\Mailer;
use TheRightCrowd\Mailers\UserMailer;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */

	public function validator(array $data)
	{

		$rules = [
			'first_name' => 'required|max:255',
			'last_name' => 'required|max:255',
			'street' => 'required|max:255',
			'city' => 'required|max:255',
			'county' => 'required|max:255',
			'postcode' => 'required|max:255',
			'telephone' => 'required|max:255',
			'email' => 'required|confirmed|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
			'accept_terms' => 'required|min:1',
			// 'bonus' => 'size:4|exists:users_bonus,code',
			'bonus' => 'max:255',
		];

		$messages = [
			'accept_terms.required' => 'You need to accept our terms and conditions!',
		];

		$validator = Validator::make($data, $rules, $messages);

		return $validator;
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{

		$new_user = User::create([
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'street' => $data['street'],
			'street_2' => $data['street_2'],
			'city' => $data['city'],
			'county' => $data['county'],
			'postcode' => $data['postcode'],
			'telephone' => $data['telephone'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
			'bonus' => $data['bonus'],
			'accept_terms' => $data['accept_terms'],
		]);

		UserMailer::send_welcome_email($new_user, $data['email'], $data['password']);

		//Display Tracking Code
		if(Session::get('email_marketing')==true){
			Session::flash('email_marketing_show_tracking', '1');
		}

		return $new_user;
	}

}
