<?php namespace TheRightCrowd;

use Illuminate\Database\Eloquent\Model;

class StartUpReview extends Model {

	//
   /* Relationships */
   
   public function startups()
   {
      return $this->belongsTo('TheRightCrowd\StartUps', 'start_up_id', 'id');
   }
   
   public function reviewer()
   {
      return $this->belongsTo('TheRightCrowd\User', 'user_id', 'id');
   }
}
