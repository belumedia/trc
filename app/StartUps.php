<?php

namespace TheRightCrowd;

use Illuminate\Database\Eloquent\Model;

class StartUps extends Model {

   protected $primaryKey = "id";
   
   public $incrementing = false;
   
   protected $fillable = ['user_id', 'logo_file', 'full_business_name', 'brand_name', 'investment_total', 'equity_offered', 'business_summuary', 'investment_summuary', 'business_address', 'business_postcode', 'company_number', 'incorporation_date', 'product_details', 'market_impact', 'acheivements', 'monetisation_strategy', 'use_of_funds', 'market_analysis', 'marketing_strategy', 'competition_strategy', 'relevant_market_experience', 'sector_1_id', 'sector_2_id', 'sector_3_id', 'seis_eis_tax_relief', 'eis_shares_compliance', 'controlled_by_another_company', 'own_shares_in_other_company', 'partner_in_other_partnership', 'gross_assets', 'total_exmployees', 'business_engagement_confirmation', 'stripe_secret_key', 'stripe_publishable_key'];
   protected $guarded = ['id', 'created_at', 'updated_at',];
   
   public $status_class = "label-default";
   public $status_text = "Draft";
   public $status_select = "DRAFT";
   public $can_edit = true;
   
   public $invested_total_string = "&pound;0";
   public $invested_total_integer = 0;
   public $invested_percentage = "0%";
   
   public $transfered_total_string = "&pound;0";
   public $transfered_total_integer = 0;
   
   public $review_avg_score = 0;
   public $review_total_score = 0;
   public $review_count = 0;

   public $funding_has_expire_date = true;
   public $funding_expire_days = 0;

   /* GUUID Support */

   protected static function boot() {
      parent::boot();

      static::creating(function ($model) {
         $model->{$model->getKeyName()} = (string) $model->generateNewId();
      });
   }

   public function generateNewId() {
      return uniqid(rand());
   }

   /* Scopes */

   public function scopeOpenForInvestment($query) {
      return $query->where('is_open_for_investment', '=', '1');
   }

   /* Mutators */
   public function display_investment_target() {
   		return "&pound;".number_format($this->investment_total, 0);
   }
   

   /* Start-Up Status Management */

   public function clear_status() {

      $this->in_review = 0;
      $this->is_rejected = 0;
      $this->is_approved = 0;
      $this->is_open_for_investment = 0;
      $this->is_funded = 0;
      $this->is_failed_funding = 0;
      $this->is_complete = 0;
   }

   public function set_status_to_draft() {
      $this->clear_status();
      $this->save();
   }

   public function set_status_to_review() {
      $this->clear_status();
      $this->in_review = 1;
      $this->in_review_date = date('Y-m-d');
      $this->save();
   }

   public function set_status_to_rejected($explanation) {
      $this->clear_status();
      $this->is_rejected = 1;
      $this->is_rejected_explanation = $explanation;
      $this->is_rejected_date = date('Y-m-d');
      $this->save();
   }

   public function set_status_to_approved() {
      $this->clear_status();
      $this->is_approved = 1;
      $this->is_approved_date = date('Y-m-d');
      $this->save();
   }

   public function set_status_to_open_to_investment() {
      $this->clear_status();
      $this->is_open_for_investment = 1;
      $this->is_open_for_investment_date = date('Y-m-d');
      $this->save();
   }

   public function set_status_to_is_funded() {
      $this->clear_status();
      $this->is_funded = 1;
      $this->is_funded_date = date('Y-m-d');
      $this->save();
   }

   public function set_status_to_is_failed_funding() {
      $this->clear_status();
      $this->is_failed_funding = 1;
      $this->is_failed_funding_date = date('Y-m-d');
      $this->save();
   }

   public function set_status_to_is_complete() {
      $this->clear_status();
      $this->is_complete = 1;
      $this->is_complete_date = date('Y-m-d');
      $this->save();
   }

   public function get_sart_up_status() {

      if ($this->in_review == 1) {
         $this->status_text = "In Review";
         $this->status_class = "label-info";
         $this->status_select = "REVIEW";
         $this->can_edit = false;
      } else if ($this->is_rejected == 1) {
         $this->status_text = "Application Rejected";
         $this->status_class = "label-danger";
         $this->status_select = "REJECTED";
         $this->can_edit = false;
      } else if ($this->is_approved == 1) {
         $this->status_text = "Approved";
         $this->status_class = "label-primary";
         $this->status_select = "APPROVED";
         $this->can_edit = false;
      } else if ($this->is_open_for_investment == 1) {
         $this->status_text = "Open for Investment";
         $this->status_class = "label-primary";
         $this->status_select = "OPEN";
         $this->can_edit = false;
      } else if ($this->is_funded == 1) {
         $this->status_text = "Funding Completed";
         $this->status_class = "label-success";
         $this->status_select = "FUNDED";
         $this->can_edit = false;
      } else if ($this->is_failed_funding == 1) {
         $this->status_text = "Funding Failed";
         $this->status_class = "label-danger";
         $this->status_select = "FAILED";
         $this->can_edit = false;
      } else if ($this->is_complete == 1) {
         $this->status_text = "Complete";
         $this->status_class = "label-success";
         $this->status_select = "COMPLETE";
         $this->can_edit = false;
      } else if ($this->is_edit_complete()) {
         $this->status_text = "Ready for Review";
         $this->status_class = "label-primary";
      }
   }

   public function set_status_by_select($status) {

      if ($status == "DRAFT")
         $this->set_status_to_draft();
      if ($status == "REVIEW")
         $this->set_status_to_review();
      if ($status == "REJECTED")
         $this->set_status_to_rejected("");
      if ($status == "APPROVED")
         $this->set_status_to_approved();
      if ($status == "OPEN")
         $this->set_status_to_open_to_investment();
      if ($status == "FUNDED")
         $this->set_status_to_is_funded();
      if ($status == "FAILED")
         $this->set_status_to_is_failed_funding();
      if ($status == "COMPLETE")
         $this->set_status_to_is_complete();
   }

   /* Company Edit Completion Status */

   public function is_edit_compnay_complete() {

      $complete = true;

      if (strlen($this->full_business_name) < 1)
         $complete = false;
      //if (strlen($this->brand_name) < 1)
      //   $complete = false;
      if (strlen($this->investment_total) < 1)
         $complete = false;
      if (strlen($this->equity_offered) < 1)
         $complete = false;
      if (strlen($this->business_summuary) < 1)
         $complete = false;
      if (strlen($this->investment_summuary) < 1)
         $complete = false;
      if (strlen($this->business_address) < 1)
         $complete = false;
      if (strlen($this->business_postcode) < 1)
         $complete = false;
      if (strlen($this->company_number) < 1)
         $complete = false;
      if (strlen($this->incorporation_date) < 1)
         $complete = false;

      return $complete;
   }

   public function is_edit_business_complete() {

      $complete = true;

      if (strlen($this->product_details) < 1)
         $complete = false;
      if (strlen($this->market_impact) < 1)
         $complete = false;
      if (strlen($this->acheivements) < 1)
         $complete = false;
      if (strlen($this->monetisation_strategy) < 1)
         $complete = false;
      if (strlen($this->use_of_funds) < 1)
         $complete = false;

      return $complete;
   }

   public function is_edit_market_complete() {

      $complete = true;

      if (strlen($this->market_analysis) < 1)
         $complete = false;
      if (strlen($this->marketing_strategy) < 1)
         $complete = false;
      if (strlen($this->competition_strategy) < 1)
         $complete = false;
      if (strlen($this->relevant_market_experience) < 1)
         $complete = false;

      return $complete;
   }

   public function is_edit_executive_complete() {

      $complete = false;

      if (TeamMembers::where('start_up_id', '=', $this->id)->count() > 0)
         $complete = true;

      return $complete;
   }

   public function is_edit_documents_complete() {

      $complete = false;

      if (Documents::where('start_up_id', '=', $this->id)->count() > 0)
         $complete = true;

      return $complete;
   }

   public function is_edit_seis_complete() {

      $complete = false;

      if (!empty($this->seis_eis_tax_relief))
         $complete = true;
      if (!empty($this->eis_shares_compliance))
         $complete = true;
      if (!empty($this->controlled_by_another_company))
         $complete = true;
      if (!empty($this->own_shares_in_other_company))
         $complete = true;
      if (!empty($this->partner_in_other_partnership))
         $complete = true;
      if (!empty($this->own_shares_in_other_company))
         $complete = true;
      if (!empty($this->gross_assets))
         $complete = true;

      return $complete;
   }

   public function is_edit_bank_complete() {

      $complete = false;
	  
	  $user = User::find($this->user_id);
	  if(!empty($user)){
		  if($user->stripe_connected) $complete = true;
		}
		
      return $complete;
   }

   public function is_edit_complete() {

      $complete = true;

      if (!$this->is_edit_bank_complete())
         $complete = false;
      if (!$this->is_edit_business_complete())
         $complete = false;
      if (!$this->is_edit_compnay_complete())
         $complete = false;
      if (!$this->is_edit_documents_complete())
        // $complete = false;
      if (!$this->is_edit_executive_complete())
         $complete = false;
      if (!$this->is_edit_market_complete())
         $complete = false;
      if (!$this->is_edit_seis_complete())
         $complete = false;

      return $complete;
   }

   /*
    * Calculate total invested in Start-Up
    */
   
   public function total_investment() {
      
      $investments = StartupInvestment::where('start_up_id', '=', $this->id)->invested()->get();
      foreach($investments as $investment){
         $this->invested_total_integer = $this->invested_total_integer + $investment->amount;
      }
      if($this->invested_total_integer>0){
         $this->invested_total_string = "&pound;".number_format($this->invested_total_integer, 0);
         $this->invested_percentage = round(($this->invested_total_integer / $this->investment_total)*100)."%";
      }
   }
   
   public function total_transfered() {
      
      $investments = StartupInvestment::where('start_up_id', '=', $this->id)->transfered()->get();
      foreach($investments as $investment){
         $this->transfered_total_integer = $this->transfered_total_integer + $investment->amount;
      }
      
      if($this->transfered_total_integer>0){
         $this->transfered_total_string = "&pound;".number_format($this->transfered_total_integer, 0);
      }
   }
   
   /*
    * Start-Up Date Functions
    */
    
    public function has_funding_expire_date(){
	    
	    $result = true;
	    
	    if(empty($this->investment_expire)) $result = false;
	    if($this->investment_expire=="0000-00-00") $result = false;
		
		$this->funding_has_expire_date = $result;
		
		return $result;	    
    }
    
    public function calculate_expire_days(){
	    
	    $diff = 0;
	    
	    if($this->has_funding_expire_date()){
	    
	    	$date1 = new \DateTime();
			$date2 = new \DateTime($this->investment_expire);
			
			$diff = $date2->diff($date1)->format("%a");
		}
				    
	    $this->funding_expire_days = $diff;
	    
	    return $diff;
    }
   
   /*
    * Is Start-Up Followed
    */
    
    public function is_investor_followed($investor_id) {
	    $result = false;
	    if(StartUpsFollow::where('investor_id', '=', $investor_id)->where('start_up_id', '=', $this->id)->count()>0) $result = true;
	    return $result;
    }
    
    public function calculate_review_score() {
	    
	    $this->review_avg_score = 0;
	    $this->review_total_score = 0;
	    $this->review_count = 0;
	    
	    $reviews = StartUpReview::where('start_up_id', '=', $this->id)->get();
	    foreach($reviews as $review){
		    $this->review_total_score = $this->review_total_score + $review->rating;
		    $this->review_count++;
	    }
	    
	    if($this->review_count>0){
		    $this->review_avg_score = round(($this->review_total_score / $this->review_count), 2);
	    }
	    
    }
    

}
