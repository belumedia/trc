<?php namespace TheRightCrowd;

use Illuminate\Database\Eloquent\Model;

class Sectors extends Model {

	//
	public static function get_sectors_for_select() {
		$data = array();
		
		$data[''] = "Not Applicable";
		
		$sectors = Self::all();
		foreach($sectors as $sector){
			$data[$sector->value] = $sector->name;
		}
		
		return $data;
		
	}
	
}
