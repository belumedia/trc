<?php namespace TheRightCrowd;

use Illuminate\Database\Eloquent\Model;
use \DB;

class StartupInvestment extends Model {

	protected $primaryKey = "id";
   public $assigned_id = "";
   public $status = "Incomplete";
   public $admin_status = "Waiting Investor";

   public $can_make_payment = true;
   public $can_see_share_certificate = false;
   public $can_cancel = true;

   public $admin_can_transfer = false;
   public $admin_can_revesrse = false;
   public $admin_can_cancel = true;

   protected $fillable = ['start_up_id','investor_id','amount'];

   protected static function boot() {
      parent::boot();

      static::creating(function ($model) {
         $model->{$model->getKeyName()} = (string) $model->generateNewId();
      });
   }

   public function generateNewId() {
      $this->assigned_id = uniqid(rand());
      return $this->assigned_id;
   }

   /* Relationships */

   public function startups()
   {
      return $this->belongsTo('TheRightCrowd\StartUps', 'start_up_id', 'id');
   }

   public function investor()
   {
      return $this->belongsTo('TheRightCrowd\User', 'investor_id', 'id');
   }

   /* Scopes */
   public function scopeIncomplete($query) {
      $query->where('held', '!=', '1');
      $query->where('transfered', '!=', '1');
      return $query;
   }

   public function scopePending($query) {
      $query->where('held', '=', '1');
      $query->where('transfered', '!=', '1');
      return $query;
   }

   public function scopeComplete($query) {
      $query->where('transfered', '=', '1');
      return $query;
   }


   public function scopeInvested($query) {
      return $query->where('held', '=', '1');
   }

   public function scopeTransfered($query) {
      return $query->where('transfered', '=', '1');
   }

   /* Methods */

   public function get_investment_status(){

      if($this->held==1){
         $this->status = "Waiting for Startup";
         $this->admin_status = "Ready to Transfer";
         $this->can_make_payment = false;
         $this->can_cancel = false;
         $this->admin_can_transfer = true;
         $this->admin_can_cancel = false;
      }

      if($this->transfered==1){
         $this->status = "Funded";
         $this->admin_status = "Transfered";
         $this->can_make_payment = false;
         $this->can_see_share_certificate = true;
         $this->can_cancel = false;
         $this->admin_can_transfer = false;
         $this->admin_can_revesrse = true;
         $this->admin_can_cancel = false;
      }

      if($this->reversed==1){
         $this->status = "Transaction Reversed";
         $this->admin_status = "Reversed";
         $this->can_make_payment = false;
         $this->can_cancel = false;
         $this->admin_can_transfer = true;
         $this->admin_can_cancel = false;
      }

   }

   public function display_formatted_amount(){
      return "&pound;".number_format($this->amount, 2);
   }





   public function display_formatted_date(){
      //dd($this->updated_at);

      //return date('jS M Y H:i', $this->updated_at->toDateTimeString());
   }


   public static function get_investments_for_sipp($sipp_id){
	   return self::join('users', 'startup_investments.investor_id', '=', 'users.id')->where('users.sip_provider_acc_id', $sipp_id)->get();
   }



}
