<?php namespace TheRightCrowd\Http\Controllers;

use Auth;
use Session;

use TheRightCrowd\Http\Requests;
use TheRightCrowd\Http\Controllers\Controller;

use Illuminate\Http\Request;

use TheRightCrowd\User;
use TheRightCrowd\StartupInvestment;

use TheRightCrowd\Mailers\Mailer;
use TheRightCrowd\Mailers\UserMailer;

class SippProviderController extends Controller {

	/* --------------------------------------------------------
	Investors		
	-------------------------------------------------------- */
	public function show_investors(){
		
		$investors = User::where('sip_provider_acc_id', '=', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
		return view('sipp.show_investors')->with([
			'investors' => $investors,
		]);

	}

	public function add_investor() {
		return view('sipp.add_investor');
	}

	public function save_investor(Request $request) {
		
		$this->validate($request, User::$sipp_rules);
		
		$password = trim(uniqid(rand()),8);
		
		$new_investor = new User();
		$new_investor->email = $request->email;
		$new_investor->first_name = $request->first_name;
		$new_investor->last_name = $request->last_name;
		$new_investor->street = $request->street;
		$new_investor->street_2 = $request->street_2;
		$new_investor->city = $request->city;
		$new_investor->county = $request->county;
		$new_investor->postcode = $request->postcode;
		$new_investor->telephone = $request->telephone;
		$new_investor->password = bcrypt($password);
		$new_investor->is_sipp_acc = true;
		$new_investor->sip_provider_acc_id = Auth::user()->id;

		$new_investor->save();
		
		UserMailer::send_sipp_welcome_email($new_investor, $request->email, $password);
		
		return redirect()->route('sipp.investors');
	}

	public function edit_investor($id){
		$investor = User::find($id);
		return view('sipp.edit_investor')->with([
			'investor' => $investor,
		]);
	}

	public function update_investor(Request $request){
		
		$this->validate($request, User::$sipp_rules_update);

		$investor = User::find($request->id);
		$investor->email = $request->email;
		$investor->first_name = $request->first_name;
		$investor->last_name = $request->last_name;
		$investor->street = $request->street;
		$investor->street_2 = $request->street_2;
		$investor->city = $request->city;
		$investor->county = $request->county;
		$investor->postcode = $request->postcode;
		$investor->telephone = $request->telephone;
		if(strlen($request->new_password)>0){
			$investor->password = bcrypt($request->new_password);
		}
		$investor->is_sipp_acc = true;
		$investor->sip_provider_acc_id = Auth::user()->id;
		
		$investor->save();
		
		return redirect()->route('sipp.investors');
		
	}
	
	public function view_investor($id) {
		
		$investor = User::find($id);
		
		return view('sipp.view_investor')->with([
			'investor' => $investor,
		]);
	}

	/* --------------------------------------------------------
	Investments		
	-------------------------------------------------------- */

	public function show_investments() {
		
		$investments = StartupInvestment::get_investments_for_sipp(Auth::user()->id);
		
		return view('sipp.show_investments')->with([
			'investments' => $investments,
		]);		
		
	}

	public function update_status($id, $status){
		
		$investment = StartupInvestment::find($id);
		
		$investment->reversed = 0;
		$investment->held = 0;
		$investment->transfered = 0;
		
		if($status==-1){
			$message = "Investment has been reversed.";
			$investment->reversed = 1;
		}
		if($status==1){
			$message = "Investment has been marked as held.";
			$investment->held = 1;
		}
		if($status==2){
			$message = "Investment has been marked as transfered.";
			$investment->transfered = 1;
		}
		
		$investment->save();
		
		if($status==-2){
			$message = "This investment has been declined.";
			$investment->delete();
		}
		
		
		Session::flash('message', $message);
		
		return redirect()->route('sipp.view-investor', ['id' => $investment->investor_id]);
	}




}
