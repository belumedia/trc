<?php

namespace TheRightCrowd\Http\Controllers;

use Auth;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Hashing;
use Intervention\Image\ImageManagerStatic as Image;

use TheRightCrowd\User;
use TheRightCrowd\StartUps;
use TheRightCrowd\Sectors;
use TheRightCrowd\Documents;
use TheRightCrowd\TeamMembers;
use TheRightCrowd\Videos;
use TheRightCrowd\QuestionsAndAnswers;
use TheRightCrowd\StartupInvestment;
use TheRightCrowd\StartUpsFollow;

use TheRightCrowd\Mailers\Mailer;
use TheRightCrowd\Mailers\UserMailer;

class MemberController extends Controller {

   public function __construct() {
      $this->middleware('auth');
   }

   public function index() {

      $top_picks = StartUps::where('top_pick', '=', '1')->limit(3)->get();

      $followed_start_ups = [];
      $followed = StartUpsFollow::where('investor_id', '=', Auth::user()->id)->get();
      foreach($followed as $follow){
	      $start_up = StartUps::find($follow->start_up_id);
	      if(!empty($start_up)) $followed_start_ups[] = $start_up;
      }


      return view('members.home')->with([
           'top_picks' => $top_picks,
           'followed_start_ups' => $followed_start_ups,
      ]);
   }

	/* ------------------------------------------------------------------------------------------
		Show Investor Options
	------------------------------------------------------------------------------------------ */

	public function display_investor_options() {

		$investor = Auth::User();
		$investor->get_user_investment_status();

		if($investor->investor_type_set==false) {
			return view('members.set_inverstor_type');

		} else if($investor->investor_terms_specific==false) {
			return view('members.set_inverstor_general_terms');

		} else if($investor->investor_terms_general==false) {
			return view('members.set_inverstor_general_terms');
		}
	}

	public function update_investor_options_type($type) {

		$investor = Auth::User();

		if($type=="HIGH_NET_WORTH"){
			$investor->is_high_networth = true;
			$investor->accept_terms_high_networth = true;

		}
		if($type=="SOPHISTICATED"){
			$investor->is_professional = true;
			$investor->accept_terms_professional = true;
		}
		if($type=="RESTRICTED"){
			$investor->is_restricted = true;
			$investor->accept_terms_restricted = true;
		}

		$investor->save();

		return redirect()->route('member-investor-type');

	}

	public function update_investor_general_terms() {

		$investor = Auth::User();
		$investor->accept_general_terms = true;
		$investor->save();

		return redirect()->route('member-browse-start-ups');

	}


   /* ------------------------------------------------------------------------------------------
     Browse Start-Ups
     ------------------------------------------------------------------------------------------ */

   public function browse_start_ups() {

      $start_ups = StartUps::openForInvestment()->get();

      return view('members.browse_start_ups')->with([
                  'start_ups' => $start_ups,
      ]);
   }

   public function company_view($id) {

        if(Auth::user()->get_user_investment_status()){

	      $start_up = StartUps::find($id);
	      $start_up->total_investment();
	      $start_up->calculate_expire_days();

	      $team_members = TeamMembers::forStartup($id)->get();
	      $documents = Documents::forStartup($id)->get();
	      $videos = Videos::forStartup($id)->get();
	      $questions = QuestionsAndAnswers::forStartup($id)->where('answer', '!=', '')->get();

	      return view('members.company_view')->with([
	                  'start_up' => $start_up,
	                  'team_members' => $team_members,
	                  'documents' => $documents,
	                  'videos' => $videos,
	                  'questions' => $questions,
	      ]);

      	} else {

	    	return redirect()->route('member-investor-type');
	    }
   }

   public function company_ask_question(Request $data){

      $start_up = StartUps::find($data->start_up_id);

      // $user = User::find($start_up->user_id);

      $question = new QuestionsAndAnswers();
      $question->start_up_id = $data->start_up_id;
      $question->question = $data->question;
      $question->question_by_user_id = Auth::User()->id;
      $question->save();

      UserMailer::send_question_asked(User::find($start_up->user_id), $question->question, $start_up->full_business_name);
      Session::flash('message', 'Your question has been sent!');

      // Mail::send('email.template', $data, function($message) use ($user) {
      //   $message->to($user->email, $user->first_name)
      //           ->subject('You have got a new question !');
      // });

      // Mail::send('email.template', $data, function($message) {
      //   $message->to($user->email, $user->first_name)->subject('You have got a new question!');
      // });

      return redirect()->route('member-company-view', [
                  'id' => $data->start_up_id,
      ]);
      //Try to get the startup user id -> -> which user id is it - > get email from the user -> send an email
   }

   public function company_follow($id){

		StartUpsFollow::where('investor_id', '=', Auth::user()->id)->where('start_up_id', '=', $id)->delete();

   		$follow = new StartUpsFollow();
   		$follow->investor_id = Auth::user()->id;
   		$follow->start_up_id = $id;
   		$follow->save();

   		Session::flash('message', 'You are now following this company!');

   		return redirect()->route('member-company-view', [
        	'id' => $id,
		]);
   }

    public function company_un_follow($id){

		StartUpsFollow::where('investor_id', '=', Auth::user()->id)->where('start_up_id', '=', $id)->delete();

   		Session::flash('message', 'You are no longer following this company!');

   		return redirect()->route('member-company-view', [
        	'id' => $id,
		]);
   }

   public function company_invest($id){
      $start_up = StartUps::find($id);
      return view('members.company_invest')->with([
                  'start_up' => $start_up,
      ]);
   }

   public function company_post_investment(Request $data){

      //dd($data);

      $redirect = redirect()->route('member-company-invest', [
              'id' => $data->start_up_id,
              ])->withInput();

      if($data->investment_amount>0){
         if($data->understand_risks != null){
            if($data->accept_terms != null){

               $investment = new StartupInvestment([
                   'start_up_id' => $data->start_up_id,
                   'investor_id' => Auth::user()->id,
                   'amount' => $data->investment_amount,
               ]);

               $investment->save();

               //dd($investment);

               //dd($investment->assigned_id);

               $redirect = redirect()->route('member-company-payment', [
                        'start_up_id' => $data->start_up_id,
                        'investment_id' => $investment->assigned_id,
                       ]);

            } else {
               Session::flash('message', 'You must accept the Right Crowd Terms of Service.');
            }
         } else {
            Session::flash('message', 'You must confirm that you understand the risks of making an investment.');
         }
      } else {
         Session::flash('message', 'Please enter investment amount.');
      }

      return $redirect;

   }

   public function company_payment($start_up_id, $investment_id){

      $start_up = StartUps::find($start_up_id);
      $investment = StartupInvestment::find($investment_id);

      Session::put('investment-id', $investment->id);

      return view('members.company_payment')->with([
            'start_up' => $start_up,
            'investment' => $investment,
      ]);

   }

   public function company_complete($investment_id){

      $investment = StartupInvestment::find($investment_id);
      $start_up = StartUps::find($investment->start_up_id);

      return view('members.company_complete')->with([
            'start_up' => $start_up,
            'investment' => $investment,
      ]);
   }


   /* ------------------------------------------------------------------------------------------
     Investment Management
     ------------------------------------------------------------------------------------------ */

   public function your_investments() {

      $investments_incomplete = StartupInvestment::where('investor_id', '=', Auth::user()->id)->incomplete()->orderBy('updated_at', 'DESC')->get();
      $investments_pending = StartupInvestment::where('investor_id', '=', Auth::user()->id)->pending()->orderBy('updated_at', 'DESC')->get();
      $investments_complete = StartupInvestment::where('investor_id', '=', Auth::user()->id)->complete()->orderBy('updated_at', 'DESC')->get();

      return view('members.your_investments')->with([
          'investments_incomplete' => $investments_incomplete,
          'investments_pending' => $investments_pending,
          'investments_complete' => $investments_complete,

      ]);
   }

   public function destroy_investment($id) {
      StartupInvestment::find($id)->delete();
      Session::flash('message', 'Your investment has been canceld!');
      return redirect()->route('member-your-investments');
   }


   /* ------------------------------------------------------------------------------------------
     Start Up Management
     ------------------------------------------------------------------------------------------ */

   public function your_start_ups() {

      $user_id = Auth::User()->id;

      $start_ups = StartUps::where('user_id', '=', $user_id)->get();

      return view('members.your_start_ups')->with([
                  'start_ups' => $start_ups,
      ]);
   }

   public function add_start_up() {

      $start_up = new StartUps([
          'user_id' => Auth::User()->id,
          'full_business_name' => 'New Start-Up',
      ]);

      $start_up->save();

      return redirect()->route('member-edit-startup-company', [
                  'id' => $start_up->id,
      ]);
   }

   /* ------------------------------------------------------------------------------------------
      Answer Questions
     ------------------------------------------------------------------------------------------ */

	public function start_up_list_questions($id) {

	  $start_up = StartUps::find($id);
      $questions = QuestionsAndAnswers::forStartup($id)->get();

      return view('members.company_questions_list')->with([
                  'start_up' => $start_up,
                  'questions' => $questions,
      ]);
	}

	public function start_up_answer_question($id) {

		$question = QuestionsAndAnswers::find($id);
		$start_up = StartUps::find($question->start_up_id);

		$questions = QuestionsAndAnswers::forStartup($id)->get();

		return view('members.company_questions_answer')->with([
		          'start_up' => $start_up,
		          'question' => $question,
		]);
	}

	public function start_up_answer_question_save(Request $data) {

		$question = QuestionsAndAnswers::find($data->question_id);
		$question->answer = $data->answer;
		$question->save();

		return redirect()->route('member-startup-questions-list', [
                'start_up_id' => $question->start_up_id,
               ]);

	}



   /* ------------------------------------------------------------------------------------------
   Start / Overview Section
   ------------------------------------------------------------------------------------------ */

   public function start_up_start($id) {

      $start_up = StartUps::find($id);
      $sectors = Sectors::get_sectors_for_select();
      $user = Auth::user();

      return view('members.edit_start_up_start')->with([
                  'start_up' => $start_up,
                  'user' => $user,
      ]);
   }

   /* ------------------------------------------------------------------------------------------
   Edit Business
   ------------------------------------------------------------------------------------------ */

   public function edit_start_up($id) {

      $start_up = StartUps::find($id);
      $sectors = Sectors::get_sectors_for_select();

      $start_up_date = new \DateTime($start_up->incorporation_date);

      return view('members.edit_start_up_company')->with([
                  'start_up' => $start_up,
                  'sectors' => $sectors,
                  'incorporation_date_day' => $start_up_date->format('d'),
                  'incorporation_date_month' => $start_up_date->format('m'),
                  'incorporation_date_year' => $start_up_date->format('Y'),

      ]);
   }

   public function update_start_up_company_details(Request $data) {

      $data->investment_total = floatval(str_replace(",", "", $data->investment_total));
      $data->equity_offered = intval($data->equity_offered);

      $incorporation_date = new \DateTime($data->incorporation_date_year."-".$data->incorporation_date_month."-".$data->incorporation_date_day);

      $data->incorporation_date = $incorporation_date->format('Y-m-d');

      $start_up = StartUps::find($data->id);
      //$start_up->logo_file = $data->logo_file;
      $start_up->full_business_name = $data->full_business_name;
      $start_up->brand_name = $data->brand_name;
      $start_up->investment_min = str_replace(",", "", $data->investment_min);
      $start_up->investment_total = $data->investment_total;
      $start_up->equity_offered = $data->equity_offered;
      $start_up->investment_days = $data->investment_days;
      $start_up->business_summuary = $data->business_summuary;
      $start_up->investment_summuary = $data->investment_summuary;
      $start_up->business_address = $data->business_address;
      $start_up->business_postcode = $data->business_postcode;
      $start_up->company_number = $data->company_number;
      $start_up->incorporation_date = $data->incorporation_date;
      $start_up->sector_1_id = $data->sector_1_id;
      $start_up->sector_2_id = $data->sector_2_id;
      $start_up->sector_3_id = $data->sector_3_id;

      $start_up->save();

      return redirect()->route('member-edit-startup-business', [
                  'id' => $start_up->id,
      ]);
   }

   /* ------------------------------------------------------------------------------------------
   Edit Business Plan
   ------------------------------------------------------------------------------------------ */

   public function edit_start_up_business_plan($id) {

      $start_up = StartUps::find($id);
      return view('members.edit_start_up_business')->with([
                  'start_up' => $start_up,
      ]);
   }

   public function update_start_up_business(Request $data) {

      $start_up = StartUps::find($data->id);
      $start_up->product_details = $data->product_details;
      $start_up->market_impact = $data->market_impact;
      $start_up->acheivements = $data->acheivements;
      $start_up->monetisation_strategy = $data->monetisation_strategy;
      $start_up->use_of_funds = $data->use_of_funds;

      $start_up->save();

      return redirect()->route('member-edit-startup-market', [
                  'id' => $start_up->id,
      ]);
   }

   /* ------------------------------------------------------------------------------------------
   Edit Market Research
   ------------------------------------------------------------------------------------------ */

   public function edit_start_up_market($id) {

      $start_up = StartUps::find($id);
      return view('members.edit_start_up_market')->with([
                  'start_up' => $start_up,
      ]);
   }

   public function update_start_up_market(Request $data) {

      $start_up = StartUps::find($data->id);
      $start_up->market_analysis = $data->market_analysis;
      $start_up->marketing_strategy = $data->marketing_strategy;
      $start_up->competition_strategy = $data->competition_strategy;
      $start_up->relevant_market_experience = $data->relevant_market_experience;

      $start_up->save();

      return redirect()->route('member-edit-startup-team', [
                  'id' => $start_up->id,
      ]);
   }

   /* ------------------------------------------------------------------------------------------
   Edit Executive Team
   ------------------------------------------------------------------------------------------ */

   public function edit_start_up_team($id) {

      $start_up = StartUps::find($id);
      $team_members = TeamMembers::where('start_up_id', '=', $id)->get();

      return view('members.edit_start_up_team')->with([
                  'start_up' => $start_up,
                  'team_members' => $team_members,
      ]);
   }

   public function update_start_up_team_save(Request $data) {


      $team_member = new TeamMembers();
      $team_member->name = $data->name;
      $team_member->start_up_id = $data->start_up_id;
      $team_member->position = $data->position;
      $team_member->age = $data->age;
      $team_member->personal_bio = $data->personal_bio;
      $team_member->involvement = $data->involvement;
      $team_member->save();

      Session::flash('message', 'Your executive team member has been added!');

      return redirect()->route('member-edit-startup-team', [
                  'id' => $data->start_up_id,
      ]);
   }

   public function update_start_up_edit_team_edit($id) {

      $team_member = TeamMembers::find($id);
      $start_up = StartUps::find($team_member->start_up_id);

      return view('members.edit_start_up_team_member')->with([
          'team_member' => $team_member,
          'start_up' => $start_up,
      ]);
   }


   public function update_start_up_team_update(Request $data) {

      $team_member = TeamMembers::find($data->id);
      $team_member->name = $data->name;
      $team_member->position = $data->position;
      $team_member->age = $data->age;
      $team_member->personal_bio = $data->personal_bio;
      $team_member->involvement = $data->involvement;
      $team_member->save();

      Session::flash('message', $data->name.' has been updated!');

      return redirect()->route('member-edit-startup-team', [
                  'id' => $team_member->start_up_id,
      ]);

   }

   public function update_start_up_remove_team($id, $start_up_id) {

      $team_member = TeamMembers::find($id)->delete();

      Session::flash('message', 'Team Member has been removed!');

      return redirect()->route('member-edit-startup-team', [
                  'id' => $start_up_id,
      ]);
   }

      public function edit_start_up_team_photo($id, $start_up_id) {

	  $team_member = TeamMembers::find($id);
      $start_up = StartUps::find($start_up_id);

      Session::put('team_member_id', $id);

      return view('members.edit_start_up_team_photo')->with([
                  'start_up' => $start_up,
                  'team_member' => $team_member,
      ]);
   }

   public function update_start_up_team_photo_upload(Request $data) {


    	$team_member = TeamMembers::find(Session::get('team_member_id'));
    	$start_up = StartUps::find($team_member->start_up_id);

		$destination = public_path()."/team_photo_uploads/";
		if(!Storage::exists($destination)) Storage::makeDirectory($destination);


		//File Stuff
		$file_name = $team_member->id;
		$file_name_original = "original_".$file_name.".jpg";
		$file_name_resized = "resized_".$file_name.".jpg";

		$upload_file = $data->file('photo_file');

		if (!empty($upload_file)){

			$data->file('photo_file')->move($destination, $file_name_original);

			$img = Image::make($destination.$file_name_original);
			$img->resize(400, 400, function ($constraint) {
			      $constraint->aspectRatio();
			      $constraint->upsize();
			});

			$img->save($destination.$file_name_resized);

			$team_member->photo_url = $file_name;
			$team_member->save();

			Session::flash('message', "Your image has been uploaded to this profile.");

		} else {

			Session::flash('error_message', 'File provided is invalid, please try again!');

		}

		return redirect()->route('member-update-startup-team-photo', [
		    'id' => $team_member->id,
		    'start_up_id' => $start_up->id,
		]);
   }


   /* ------------------------------------------------------------------------------------------
   Edit Documents
   ------------------------------------------------------------------------------------------ */

   public function edit_start_up_documents($id) {


      $start_up = StartUps::find($id);
      $documents = Documents::where('start_up_id', '=', $id)->get();

      return view('members.edit_start_up_documents')->with([
                  'start_up' => $start_up,
                  'documents' => $documents,
      ]);
   }

   public function update_start_up_document_save(Request $data) {

      if ($data->file('document_file') != null) {

         if ($data->file('document_file')->isValid()) {

            $file_name = uniqid(rand()) . "__" . $data->file('document_file')->getClientOriginalName() . $data->file('document_file')->getExtension();


            $document = new Documents();
            $document->start_up_id = $data->start_up_id;
            $document->title = $data->title;
            $document->file_type = $data->file('document_file')->getMimeType();
            $document->file_size = $data->file('document_file')->getSize();
            $document->description = $data->description;
            $document->file_location = $file_name;
            $document->save();

            $data->file('document_file')->move(public_path() . "/uploads/", $file_name);


            Session::flash('message', 'Your document has been uploaded!');


         } else {
            Session::flash('message_error', '<strong>Your document could not been uploaded!</strong><br>Please try a different file type.');
         }
      } else {
         Session::flash('message_error', '<strong>No document selected!</strong><br>Please provide a document to upload.');
      }


      return redirect()->route('member-edit-startup-documents', [
                  'id' => $data->start_up_id,
      ]);
   }

   public function update_start_up_remove_document($id, $start_up_id) {

      $document = Documents::find($id);

      //Storage::delete(public_path()."/uploads/".$document->file_location);

      $document->delete();

      Session::flash('message', 'Document has been removed!');

      return redirect()->route('member-edit-startup-documents', [
                  'id' => $start_up_id,
      ]);
   }

   /* ------------------------------------------------------------------------------------------
   Manage Logo
   ------------------------------------------------------------------------------------------ */

   public function edit_start_up_logo($id) {

      $start_up = StartUps::find($id);
      Session::put('start_up_id', $id);

      return view('members.edit_start_up_logo')->with([
                  'start_up' => $start_up,
      ]);
   }

   public function update_start_up_logo_upload(Request $data) {

    $start_up = StartUps::find(Session::get('start_up_id'));

		$destination = public_path()."/photo_uploads/";
		if(!Storage::exists($destination)) Storage::makeDirectory($destination);


		//File Stuff
		$file_name = Session::get('start_up_id');
		$file_name_original = "original_".$file_name.".jpg";
		$file_name_resized = "resized_".$file_name.".jpg";

		$upload_file = $data->file('logo_file');

		if (!empty($upload_file)){

			$data->file('logo_file')->move($destination, $file_name_original);

			$img = Image::make($destination.$file_name_original);
			$img->resize(400, 250, function ($constraint) {
			      $constraint->aspectRatio();
			      $constraint->upsize();
			});

			$img->save($destination.$file_name_resized);

			$start_up->logo_file = $file_name;
			$start_up->save();

			Session::flash('message', "Your image has been uploaded to this profile.");

		} else {

			Session::flash('error_message', 'File provided is invalid, please try again!');

		}

		return redirect()->route('member-edit-startup-logo', [
		    'id' => $data->start_up_id,
		]);
   }

   /* ------------------------------------------------------------------------------------------
   Manage Videos
   ------------------------------------------------------------------------------------------ */

   public function edit_start_up_videos($id) {

      $start_up = StartUps::find($id);
      $videos = Videos::where('start_up_id', '=', $id)->get();

      return view('members.edit_start_up_videos')->with([
                  'start_up' => $start_up,
                  'videos' => $videos,
      ]);
   }

   public function update_start_up_video_save(Request $data) {

      $video = new Videos();
      $video->start_up_id = $data->start_up_id;
      $video->video_url = $data->video_url;
      $video->save();

      Session::flash('message', 'Your video has been added!');

      return redirect()->route('member-edit-startup-videos', [
                  'id' => $data->start_up_id,
      ]);
   }

   public function update_start_up_remove_video($id, $start_up_id) {

      $video = Videos::find($id)->delete();
      Session::flash('message', 'Video has been removed!');
      return redirect()->route('member-edit-startup-videos', [
                  'id' => $start_up_id,
      ]);
   }

   /* ------------------------------------------------------------------------------------------
   Edit SEIS
   ------------------------------------------------------------------------------------------ */

   public function edit_start_up_seis($id) {

      $start_up = StartUps::find($id);

      Session::put('start_up_id', $id);

      return view('members.edit_start_up_seis')->with([
                  'start_up' => $start_up,
      ]);
   }

   public function update_start_up_seis(Request $data) {

      $start_up = StartUps::find(Session::get('start_up_id'));

      $start_up->seis_eis_tax_relief = $data->input('seis_eis_tax_relief', '');
      $start_up->eis_shares_compliance = $data->input('eis_shares_compliance', '');
      $start_up->controlled_by_another_company = $data->input('controlled_by_another_company', '');
      $start_up->own_shares_in_other_company = $data->input('own_shares_in_other_company', '');
      $start_up->partner_in_other_partnership = $data->input('partner_in_other_partnership', '');
      $start_up->gross_assets = $data->input('gross_assets', '');
      $start_up->business_engagement_confirmation = $data->input('business_engagement_confirmation', '');

      $start_up->save();

      return redirect()->route('member-edit-startup-seis', [
                  'id' => $start_up->id,
      ]);
   }

   /* ------------------------------------------------------------------------------------------
   Edit Bank Account
   ------------------------------------------------------------------------------------------ */

   public function edit_start_up_bank($id) {

      $start_up = StartUps::find($id);
      $user = Auth::User();
      return view('members.edit_start_up_bank')->with([
               'start_up' => $start_up,
               'user' => $user,
      ]);
   }

   public function update_start_up_bank(Request $data) {

      $start_up = StartUps::find($data->id);
      $start_up->stripe_secret_key = $data->stripe_secret_key;
      $start_up->stripe_publishable_key = $data->stripe_publishable_key;
      $start_up->save();

      return redirect()->route('member-edit-startup-company', [
                  'id' => $start_up->id,
      ]);
   }

   /* ------------------------------------------------------------------------------------------
   Submit for Approval
   ------------------------------------------------------------------------------------------ */

   public function update_start_up_submit_for_review($id) {

      $start_up = StartUps::find($id);
      $start_up->set_status_to_review();

      return redirect()->route('member-your-startups');
   }

   /* ------------------------------------------------------------------------------------------
   Destroy Start-Up
   ------------------------------------------------------------------------------------------ */

   public function destroy_startup($id) {
      StartUps::find($id)->delete();
      Session::flash('message', 'Your start-up has been removed!');
      return redirect()->route('member-your-startups');
   }

   /* ------------------------------------------------------------------------------------------
     Member Profile
     ------------------------------------------------------------------------------------------ */

	public function your_profile() {

		$user = Auth::user();

		return view('members.your_profile')->with([
		          'user' => $user,
		]);
	}

	public function your_profile_edit() {

		$user = Auth::user();

		return view('members.your_profile_edit')->with([
		          'user' => $user,
		]);
	}

	public function your_profile_update(Request $data) {

		$user = Auth::user();

		$user->first_name = $data->first_name;
		$user->last_name = $data->last_name;
		$user->telephone = $data->telephone;
		$user->street = $data->street;
		$user->street_2 = $data->street_2;
		$user->city = $data->city;
		$user->county = $data->county;
		$user->postcode = $data->postcode;

		$user->save();

		Session::flash('message', 'Your contact details have been updated!');
		return redirect()->route('member-your-proflie');

	}

	public function your_profile_change_password() {

		$user = Auth::user();

		return view('members.your_profile_password')->with([
		          'user' => $user,
		]);
	}

	public function your_profile_update_password(Request $data) {

      $user = Auth::user();

      if(!Auth::attempt(['email' => $user->email, 'password' => $data->get('password')])){

         Session::flash('error_message', "You did not enter your current password correctly.");
         return redirect()->back();

      } else if(strlen($data->get('new_password'))<6){

         Session::flash('error_message', "Your new password is too short, must be more than 6 characters.");
         return redirect()->back();


      } else if($data->get('new_password')!=$data->get('retype_new_password')){

         Session::flash('error_message', "Your new passwords did not match.");
         return redirect()->back();

      } else {

         $user->password = \Hash::make($data->get('new_password'));
         $user->save();

         Session::flash('message', "Your password has been chnaged.");
         return redirect()->route('member-your-proflie');

      }
	}




}
