<?php namespace TheRightCrowd\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use TheRightCrowd\StartUps;

class PageController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/* Investors Section */
	public function index()
	{
		$start_ups = StartUps::where('is_featured', '=', '1')->orderBy('updated_at', 'DESC')->get();
		return view('pages.welcome', [
           'start_ups' => $start_ups,
        ]);
	}

	/* Investors Section */
	public function landing_page(Request $data)
	{
		//Record Variables Here
		$app_id = $data->input('appid');

		if($app_id){
			Session::put('app_id', $app_id);
			Session::put('email_marketing', true);
		}

		$start_ups = StartUps::where('is_featured', '=', '1')->orderBy('updated_at', 'DESC')->get();
		return view('pages.welcome', [
           'start_ups' => $start_ups,
        ]);
	}

	public function investors()
	{
		return view('pages.investors');
	}

	public function opportunities()
	{
      $start_ups = StartUps::where('is_featured', '=', '1')->orderBy('updated_at', 'DESC')->get();
		return view('pages.opportunities', [
           'start_ups' => $start_ups,
           ]);
	}

	public function opportunities_view($id) {

		$start_up = StartUps::find($id);
		$start_up->total_investment();
	    $start_up->calculate_expire_days();

		return View('pages.company_view', [
			'start_up' => $start_up,
		]);

	}


	/* Companies Section */
	public function companies()
	{
		return view('pages.welcome_company');
	}

	public function companies_get_started()
	{
		return view('pages.companies_get_started');
	}

	public function companies_case_studies() {
		return view('pages.companies_case_studies');
	}

   public function companies_case_studies_case_1() {
		return view('pages.companies_case_study_1');
	}

   public function companies_case_studies_case_2() {
		return view('pages.companies_case_study_2');
	}

   public function companies_case_studies_case_3() {
		return view('pages.companies_case_study_3');
	}


	/* General Pages */
	public function about_us() {
		return view('pages.about_us');
	}

	public function faq() {
		return view('pages.faq');
	}

	/* Terms & Conditions */

	public function terms_conditions() {
		return view('pages.terms_conditions');
	}

	public function website_terms() {
		return view('pages.website_terms');
	}

	public function privacy_policy() {
		return view('pages.privacy_policy');
	}

	public function membership_terms(){
		return view('pages.membership_terms');
	}

	public function investor_terms(){
		return view('pages.investor_terms');
	}

	public function investment_rules(){
		return view('pages.investment_rules');
	}

	public function risk_warning(){
		return view('pages.risk_warning');
	}

	public function company_terms(){
		return view('pages.company_terms');
	}

	public function special_offer_terms(){
		return view('pages.special_offer_terms');
	}

	/* Join */
	public function join_investor() {
		Session::put('join-layout', 'investors');
		return redirect('auth/register');
	}

	public function join_company() {

		Session::put('join-layout', 'companies');
		return redirect('auth/register');
	}

	public function signin_investor() {
		Session::put('signin-layout', 'investors');
		return redirect('auth/login');
	}

	public function signin_company() {

		Session::put('signin-layout', 'companies');
		return redirect('auth/login');
	}

	/* Error Page */

	public function error_page() {
		return view('pages.error');
	}


}
