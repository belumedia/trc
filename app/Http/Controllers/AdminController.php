<?php namespace TheRightCrowd\Http\Controllers;

use Auth;

use TheRightCrowd\Http\Requests;
use TheRightCrowd\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use TheRightCrowd\User;
use TheRightCrowd\StartUps;
use TheRightCrowd\StartUpStatus;
use TheRightCrowd\StartupInvestment;
use TheRightCrowd\Sectors;
use TheRightCrowd\Documents;
use TheRightCrowd\TeamMembers;
use TheRightCrowd\Videos;
use TheRightCrowd\StartUpReview;
use TheRightCrowd\StartUpNote;

use TheRightCrowd\Mailers\Mailer;
use TheRightCrowd\Mailers\UserMailer;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return "Hello Admin";
	}


	/* ------------------------------------------------------------------------------------------
	User Manager
	------------------------------------------------------------------------------------------ */


	public function show_users() {

		$users = User::orderBy('created_at', 'DESC')->get();
		return view('admin.show_users')->with([
			'users' => $users,
		]);
	}

	public function view_user($id) {

		$user = User::find($id);
		return view('admin.view_user')->with([
			'user' => $user,
		]);

	}

	public function user_update_access(Request $data){

		$user = User::find($data->user_id);

		if($data->is_sipp_provider==null) $data->is_sipp_provider = false;

		$user->allow_admin = $data->allow_admin;
		$user->allow_admin_review = $data->allow_admin_review;
		$user->allow_admin_options = $data->allow_admin_options;
		$user->allow_admin_users = $data->allow_admin_users;
		$user->allow_admin_investors = $data->allow_admin_investors;
		$user->is_sipp_provider = $data->is_sipp_provider;

		$user->save();

		return redirect()->route('admin.users_view', [
			'id' => $user->id,
		]);
	}


	/* ------------------------------------------------------------------------------------------
	Investments
	------------------------------------------------------------------------------------------ */


	public function show_investments() {

		$investments = StartupInvestment::take(30)->orderBy('updated_at', 'DESC')->get();

		return view('admin.show_investments')->with([
			'investments' => $investments,
		]);
	}


	/* ------------------------------------------------------------------------------------------
	Start-up Manager
	------------------------------------------------------------------------------------------ */


	public function show_start_ups() {

		$start_ups = StartUps::orderBy('in_review', 'DESC')->get();
		return view('admin.show_start_ups')->with([
			'start_ups' => $start_ups,
		]);
	}

	public function view_start_up($id) {

		$start_up = StartUps::find($id);
		$start_up->get_sart_up_status();

		$start_up->total_investment();
		$start_up->total_transfered();
		$start_up->calculate_review_score();

		$start_up_statuses = StartUpStatus::get_status_for_select();
		$investments = StartupInvestment::where('start_up_id', '=', $start_up->id)->get();
		$reviews = StartUpReview::where('start_up_id', '=', $start_up->id)->get();

		$start_up_investment_expire_date = new \DateTime($start_up->investment_expire);
		$startup_no_expire = false;

		if(!$start_up->has_funding_expire_date()){

			$startup_no_expire = true;

			if(!empty($start_up->investment_days)){
				$start_up_investment_expire_date = new \DateTime();
				$interval = new \DateInterval('P'.$start_up->investment_days.'D');
				$start_up_investment_expire_date->add($interval);
			}
		}

		$start_up->calculate_expire_days();

		$notes = StartUpNote::where('start_up_id', '=', $id)->get();

		return view('admin.view_start_up')->with([
			'start_up' => $start_up,
			'start_up_statuses' => $start_up_statuses,
			'investments' => $investments,
			'reviews' => $reviews,
			'investment_expire_day' => $start_up_investment_expire_date->format('d'),
			'investment_expire_month' => $start_up_investment_expire_date->format('m'),
			'investment_expire_year' => $start_up_investment_expire_date->format('Y'),
			'startup_no_expire' => $startup_no_expire,
			'notes' => $notes,
		]);
	}

	public function update_start_up_status(Request $data){

		$start_up = StartUps::find($data->id);
		$start_up->set_status_by_select($data->status_select);

		if($data->startup_no_expire!=1){
			$start_up_investment_expire_date = new \DateTime($data->investment_expire_year."-".$data->investment_expire_month."-".$data->investment_expire_day);
			$start_up->investment_expire = $start_up_investment_expire_date->format('Y-m-d');
		} else {
			$start_up->investment_expire = null;
		}

		$start_up->save();

		return redirect()->route('admin.startups_view', [
			'id' => $data->id,
		]);

	}

	public function update_start_up_display(Request $data){

		$start_up = StartUps::find($data->id);

		if($data->top_pick==null) $data->top_pick = 0;
		if($data->is_featured==null) $data->is_featured = 0;

		$start_up->top_pick = $data->top_pick;
		$start_up->is_featured = $data->is_featured;

		$start_up->save();

		return redirect()->route('admin.startups_view', [
			'id' => $data->id,
		]);

	}

	public function update_start_up_review(Request $data){


		$start_up = StartUps::find($data->id);

		$start_up->review = $data->review;
		$start_up->review_score = $data->review_score;
		$start_up->review_user_id = Auth::User()->id;

		$start_up->save();

		return redirect()->route('admin.startups_view', [
			'id' => $data->id,
		]);

	}

	public function save_start_up_notes(Request $data){

		$notes = new StartUpNote();

		$notes->user_id = Auth::User()->id;
		$notes->notes = $data->notes;
		$notes->start_up_id = $data->id;
		$notes->save();

		return redirect()->route('admin.startups_view', [
			'id' => $data->id,
		]);
	}

	/* ------------------------------------------------------------------------------------------
	Reviews
	------------------------------------------------------------------------------------------ */
	public function reviews () {

		$start_ups = StartUps::where('in_review', '=', '1')->orderBy('updated_at', 'DESC')->get();
		return view('admin.show_reviews')->with([
			'start_ups' => $start_ups,
		]);
	}

	public function review_add ($id) {

		$start_up = StartUps::find($id);

		$team_members = TeamMembers::forStartup($id)->get();
		$documents = Documents::forStartup($id)->get();
		$videos = Videos::forStartup($id)->get();

		Session::put('start_up_id', $id);

		return view('admin.view_reviews_add')->with([
			'start_up' => $start_up,
			'team_members' => $team_members,
			'documents' => $documents,
			'videos' => $videos,
		]);
	}

	public function review_save (Request $data) {

		$start_up_id = Session::get('start_up_id');

		$review = new StartUpReview();
		$review->user_id = Auth::User()->id;
		$review->start_up_id = $start_up_id;
		$review->rating = $data->review_score;
		$review->review = $data->review;
		$review->save();

		Session::flash('message', 'Your review has been saved!');
		return redirect()->route('admin.reviews');

	}


	/* ------------------------------------------------------------------------------------------
	Web Site Options
	------------------------------------------------------------------------------------------ */

	public function show_options() {

		$users = User::all();
		return view('admin.show_users')->with([
			'users' => $users,
		]);
	}


	/* ------------------------------------------------------------------------------------------
	Start-Up Mailers
	------------------------------------------------------------------------------------------ */

	public function send_funding_success_email($id) {

		$start_up = StartUps::find($id);
		UserMailer::send_investment_success_startup(User::find($start_up->user_id), $start_up->display_investment_target(), $start_up->full_business_name);

		return redirect()->route('admin.startups_view', [
			'id' => $id,
		]);
	}

	public function send_funding_failed_email($id) {

		$start_up = StartUps::find($id);
		UserMailer::send_investment_failed_startup(User::find($start_up->user_id), $start_up->display_investment_target(), $start_up->full_business_name);

		return redirect()->route('admin.startups_view', [
			'id' => $id,
		]);

	}

}
