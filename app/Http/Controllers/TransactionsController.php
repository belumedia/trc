<?php namespace TheRightCrowd\Http\Controllers;

use Auth;

use TheRightCrowd\Http\Requests;
use TheRightCrowd\Http\Controllers\Controller;

use TheRightCrowd\User;
use TheRightCrowd\StartUps;
use TheRightCrowd\StartupInvestment;
use TheRightCrowd\TransactionLog;

use Illuminate\Http\Request;
use Illuminate\Config;
use Illuminate\Support\Facades\Session;

use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Stripe\Token;

use TheRightCrowd\Mailers\Mailer;
use TheRightCrowd\Mailers\UserMailer;

class TransactionsController extends Controller {

	/**
	 * Connect to Customers Stripe Account
	 */
	public function connect_customer_account(Request $data) {

      $token_request_body = array(
         'client_secret' => config('stripe.stripe.secret'),
         'grant_type' => 'authorization_code',
         'code' => $data->code,
       );

      $req = curl_init("https://connect.stripe.com/oauth/token");
      curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($req, CURLOPT_POST, true );
      curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));

      $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
      $resp = json_decode(curl_exec($req), true);
      curl_close($req);

      if(isset($resp['access_token'])){

         $user = User::find(Auth::user()->id);
         $user->stripe_connected = true;
         $user->stripe_access_token = $resp['access_token'];
         $user->stripe_publishable_key = $resp['stripe_publishable_key'];
         $user->stripe_user_id = $resp['stripe_user_id'];
         $user->stripe_scope = $resp['scope'];
         $user->save();

         Session::flash('message', 'Your Stripe account is now connected to The Right Crowd');

      } else {

         Session::flash('message', 'We could not connect your Stripe account to The Right Crowd');

      }

      return redirect()->route('member-your-proflie');
	}

	/**
	 * Capture Customers & Payment Method
	 */
	public function capture_customer(Request $data) {

      $investment_id = Session::get('investment-id');

      $investment = StartupInvestment::find($investment_id);
      $start_up = StartUps::find($investment->start_up_id);
      $user = Auth::user();

      $description = "Investment of ".$investment->amount." in ".$start_up->full_business_name;

      Stripe::setApiKey(config('stripe.stripe.secret'));

      try {

	      //Create the customer
	      $customer = Customer::create([
	          'source' => $data->stripeToken,
	          'description' => $description,
	          'email' => $user->email,
	          'metadata' => [
	              'investment_transaction_id' => $investment_id,
	              'investment_start_up_id' => $investment->start_up_id,
	              'investment_investor_id' => $user->id,
	              'investment_start_up_name' => $start_up->full_business_name,
	              'investment_amount' => $investment->amount,
	              'investment_transfered' => '0',
	              ]
	      ]);

	      //Store Customer ID locally for payment later
	      $investment->stripe_customer_id = $customer->id;
	      $investment->held = true;
	      $investment->held_date = date('Y/m/d H:i:s');
	      $investment->save();

	      UserMailer::send_investment_confirmed_investor(Auth::user(), $investment->display_formatted_amount(), $start_up->full_business_name);

	      return redirect()->route('member-company-complete', ['investment_id' => $investment->id]);

		//Catch Card Errors
		} catch(\Stripe\Error\Card $e) {

			$body = $e->getJsonBody();
			$err  = $body['error'];

			/*
			print('Type is:' . $err['type'] . "\n");
			print('Code is:' . $err['code'] . "\n");
			// param is '' in this case
			print('Param is:' . $err['param'] . "\n");
			print('Message is:' . $err['message'] . "\n");
			*/

			Session::flash('message', $err['message']);

			return redirect()->route('member-company-payment', ['id' => $investment_id, 'start_up_id' => $investment->start_up_id]);

		}
	}

   /**
	 * Transfer Single Investment
	 */
   public function transfer_investment($id) {

      $investment = StartupInvestment::find($id);
      $start_up = StartUps::find($investment->start_up_id);
      $start_up_user = User::find($start_up->user_id);

      $charge_id = $this->perform_transaction_transer($start_up_user->stripe_access_token, $investment->stripe_customer_id, $investment->amount);

      $investment->reversed = 0;
      $investment->transfered = 1;
      $investment->transfered_date = date('Y/m/d H:i:s');
      $investment->stripe_chrage_id = $charge_id;
      $investment->save();

      $this->log_transaction($investment->investor_id, "Investor", $start_up_user->id, $start_up_user->email, "Investment Transfered", $investment->amount, $charge_id, "");

      UserMailer::send_investment_transfered_investor(Auth::user(), $investment->display_formatted_amount(), $start_up->full_business_name);

      Session::flash('message', 'Investment transfered to start-up');
      return redirect()->route('admin.startups_view', ['id' => $start_up->id]);
   }

   /**
	 * Reverse Transfer
	 */
   public function revese_transfer($id){


      $investment = StartupInvestment::find($id);
      $start_up = StartUps::find($investment->start_up_id);
      $start_up_user = User::find($start_up->user_id);

      Stripe::setApiKey($start_up_user->stripe_access_token);

      $charge = Charge::retrieve($investment->stripe_chrage_id);
      $refund = $charge->refunds->create();

      $investment->transfered = 0;
      $investment->reversed = 1;
      $investment->reversed_date = date('Y/m/d H:i:s');
      $investment->stripe_refund_id = $refund->id;
      $investment->save();

      $this->log_transaction($start_up_user->id, $start_up_user->email, $investment->investor_id, "Investor", "Investor Refundedd", ($refund->amount/100), $charge->id, $refund->id);

      UserMailer::send_investment_reversed_investor(Auth::user(), $investment->display_formatted_amount(), $start_up->full_business_name);

      Session::flash('message', 'Investment transfered to start-up');
      return redirect()->route('admin.startups_view', ['id' => $start_up->id]);

   }

   /**
	 * Private Perform Stripe Transfer
	 */
   private function perform_transaction_transer($start_up_stripe_token, $stripe_customer_id, $amount) {

      $fee = ($amount * 0.045)*100;

      $amount = $amount*100;

      Stripe::setApiKey(config('stripe.stripe.secret'));

      $token = Token::create(["customer" => $stripe_customer_id], $start_up_stripe_token);

      $charge = \Stripe\Charge::create([
                  "amount" => $amount,
                  "currency" => "gbp",
                  "source" => $token,
                  "description" => "Right Crowd Payment Transfer",
                  "application_fee" => $fee,
                  ], $start_up_stripe_token
      );

      return $charge->id;
   }

   /**
   * Private Perform Stripe Transfer
   */
   private function log_transaction($from_id, $from_name, $to_id, $to_name, $description, $amount, $charge_id = "", $refund_id = "") {

      $transaction = new TransactionLog([
          'from_id' => $from_id,
          'from_name' => $from_name,
          'to_id' => $to_id,
          'to_name' => $to_name,
          'description' => $description,
          'amount' => $amount,
          'stripe_charge_id' => $charge_id,
          'stripe_refund_id' => $refund_id,
          ]);

      $transaction->save();

      return $transaction;
   }



}
