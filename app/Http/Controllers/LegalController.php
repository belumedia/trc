<?php namespace TheRightCrowd\Http\Controllers;

use TheRightCrowd\Http\Requests;
use TheRightCrowd\Http\Controllers\Controller;

use Illuminate\Http\Request;

class LegalController extends Controller {

	public function investment_risk(){
      return view('legal.investment_risk');
   }
   
   public function investor_full_terms(){
      return view('legal.standard_investor_terms');
   }

   public function investee_company_terms(){
      return view('legal.investee_company_terms');
   }

   public function share_offer_terms(){
      return view('legal.share_offer_terms');
   }



}
