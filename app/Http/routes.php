<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Static Front Pages */
Route::get('/', ['as' => 'home', 'uses'=>'PageController@index']);

Route::get('/landing-page', ['as' => 'landing-page', 'uses'=>'PageController@landing_page']);

//Route::get('/investors', ['middleware' => 'auth', 'as' => 'investors', 'uses'=>'PageController@investors']);

Route::get('/investors', ['as' => 'investors', 'uses'=>'PageController@investors']);
Route::get('/opportunities', ['as' => 'opportunities', 'uses'=>'PageController@opportunities']);
Route::get('/opportunity/view/{id}', ['as' => 'opportunity-view', 'uses'=>'PageController@opportunities_view']);

Route::get('/join/investor', ['as' => 'join-investor', 'uses'=>'PageController@join_investor']);
Route::get('/join/company', ['as' => 'join-company', 'uses'=>'PageController@join_company']);

Route::get('/signin/investor', ['as' => 'signin-investor', 'uses'=>'PageController@signin_investor']);
Route::get('/signin/company', ['as' => 'signin-company', 'uses'=>'PageController@signin_company']);


Route::get('/companies', ['as' => 'companies', 'uses'=>'PageController@companies']);
Route::get('/companies-get-started', ['as' => 'companies-get-started', 'uses'=>'PageController@companies_get_started']);
Route::get('/case-studies', ['as' => 'case-studies', 'uses'=>'PageController@companies_case_studies']);
Route::get('/case-studies/case-notbox', ['as' => 'case-studies-case-1', 'uses'=>'PageController@companies_case_studies_case_1']);
Route::get('/case-studies/case-ticket-hq', ['as' => 'case-studies-case-2', 'uses'=>'PageController@companies_case_studies_case_2']);
Route::get('/case-studies/case-new-media', ['as' => 'case-studies-case-3', 'uses'=>'PageController@companies_case_studies_case_3']);

Route::get('/about-us', ['as' => 'about-us', 'uses'=>'PageController@about_us']);
Route::get('/faqs', ['as' => 'faqs', 'uses'=>'PageController@faq']);
Route::get('/terms-conditions', ['as' => 'terms-conditions', 'uses'=>'PageController@terms_conditions']);
Route::get('/website-terms-conditions', ['as' => 'website-terms', 'uses'=>'PageController@website_terms']);

Route::get('/investor-terms', ['as' => 'investor-terms', 'uses'=>'PageController@investor_terms']);
Route::get('/investment-rules', ['as' => 'investment-rules', 'uses'=>'PageController@investment_rules']);
Route::get('/risk-warning', ['as' => 'risk-warning', 'uses'=>'PageController@risk_warning']);
Route::get('/company-terms', ['as' => 'company-terms', 'uses'=>'PageController@company_terms']);
Route::get('/special-offer-terms', ['as' => 'special-offer-terms', 'uses'=>'PageController@special_offer_terms']);


Route::get('/privacy-policy', ['as' => 'privacy-policy', 'uses'=>'PageController@privacy_policy']);
Route::get('/membership-terms', ['as' => 'membership-terms', 'uses'=>'PageController@membership_terms']);

Route::get('/error', ['as' => 'error-page', 'uses'=>'PageController@error_page']);


Route::get('home', 'MemberController@index');

Route::get('member/home', ['as' => 'member-home', 'uses'=>'MemberController@index']);

/* Investing in Start-Ups */

Route::get('member/browse-startups', ['as' => 'member-browse-start-ups', 'uses'=>'MemberController@browse_start_ups']);

Route::get('member/investor-type', ['as' => 'member-investor-type', 'uses'=>'MemberController@display_investor_options']);
Route::get('member/investor-set-type/{type}', ['as' => 'member-investor-set-type', 'uses'=>'MemberController@update_investor_options_type']);
Route::get('member/investor-set-general-terms', ['as' => 'member-investor-set-general', 'uses'=>'MemberController@update_investor_general_terms']);


Route::get('member/company/view/{id}', ['as' => 'member-company-view', 'uses'=>'MemberController@company_view']);
Route::post('member/company/post/question', ['as' => 'member-company-ask-question', 'uses'=>'MemberController@company_ask_question']);
Route::get('member/company/follow/{id}', ['as' => 'member-company-follow', 'uses'=>'MemberController@company_follow']);
Route::get('member/company/un-follow/{id}', ['as' => 'member-company-un-follow', 'uses'=>'MemberController@company_un_follow']);


Route::get('member/company/invest/{id}', ['as' => 'member-company-invest', 'uses'=>'MemberController@company_invest']);
Route::post('member/company/post/investment', ['as' => 'member-company-post-investment', 'uses'=>'MemberController@company_post_investment']);

Route::get('member/company/payment/{start_up_id}/{investment_id}', ['as' => 'member-company-payment', 'uses'=>'MemberController@company_payment']);
Route::post('member/company/post/payment', ['as' => 'member-company-post-payment', 'uses'=>'MemberController@company_post_payment']);

Route::get('member/transactions/connect', ['as' => 'member-transaction-connect', 'uses'=>'TransactionsController@connect_customer_account']);
Route::post('member/transactions/capture-customer', ['as' => 'member-company-transaction-capture-customer', 'uses'=>'TransactionsController@capture_customer']);

Route::get('member/company/complete/{id}', ['as' => 'member-company-complete', 'uses'=>'MemberController@company_complete']);



Route::get('member/transactions/transfer/{id}', ['as' => 'member-transaction-transfer', 'uses'=>'TransactionsController@transfer_investment']);
Route::get('member/transactions/reverse/{id}', ['as' => 'member-transaction-reverse', 'uses'=>'TransactionsController@revese_transfer']);

/* Investments */

Route::get('member/your-investments', ['as' => 'member-your-investments', 'uses'=>'MemberController@your_investments']);
Route::delete('member/delete-investment/{id}', ['as' => 'investment-destroy', 'uses'=>'MemberController@destroy_investment']);


/* startups */

Route::get('member/your-startups', ['as' => 'member-your-startups', 'uses'=>'MemberController@your_start_ups']);

Route::get('member/add-startup', ['as' => 'member-add-startup', 'uses'=>'MemberController@add_start_up']);

Route::get('member/edit-startup/start/{id}', ['as' => 'member-edit-startup-start', 'uses'=>'MemberController@start_up_start']);
Route::get('member/edit-startup/company/{id}', ['as' => 'member-edit-startup-company', 'uses'=>'MemberController@edit_start_up']);
Route::get('member/edit-startup/business-plan/{id}', ['as' => 'member-edit-startup-business', 'uses'=>'MemberController@edit_start_up_business_plan']);
Route::get('member/edit-startup/market-research/{id}', ['as' => 'member-edit-startup-market', 'uses'=>'MemberController@edit_start_up_market']);
Route::get('member/edit-startup/executive-team/{id}', ['as' => 'member-edit-startup-team', 'uses'=>'MemberController@edit_start_up_team']);
Route::get('member/edit-startup/document-upload/{id}', ['as' => 'member-edit-startup-documents', 'uses'=>'MemberController@edit_start_up_documents']);
Route::get('member/edit-startup/logo/{id}', ['as' => 'member-edit-startup-logo', 'uses'=>'MemberController@edit_start_up_logo']);
Route::get('member/edit-startup/videos/{id}', ['as' => 'member-edit-startup-videos', 'uses'=>'MemberController@edit_start_up_videos']);
Route::get('member/edit-startup/seis-eis/{id}', ['as' => 'member-edit-startup-seis', 'uses'=>'MemberController@edit_start_up_seis']);
Route::get('member/edit-startup/bank-account/{id}', ['as' => 'member-edit-startup-bank', 'uses'=>'MemberController@edit_start_up_bank']);

Route::get('member/questions-startup/list/{id}', ['as' => 'member-startup-questions-list', 'uses'=>'MemberController@start_up_list_questions']);
Route::get('member/questions-startup/answer/{id}', ['as' => 'member-startup-questions-answer', 'uses'=>'MemberController@start_up_answer_question']);
Route::post('member/questions-startup/answer-save/', ['as' => 'member-startup-questions-save-answer', 'uses'=>'MemberController@start_up_answer_question_save']);


Route::post('member/update-startup/company/{id}', ['as' => 'member-update-startup-company', 'uses'=>'MemberController@update_start_up_company_details']);
Route::post('member/update-startup/business-plan/{id}', ['as' => 'member-update-startup-business-plan', 'uses'=>'MemberController@update_start_up_business']);
Route::post('member/update-startup/market-research/{id}', ['as' => 'member-update-startup-market-research', 'uses'=>'MemberController@update_start_up_market']);
Route::post('member/update-startup/executive-team/{id}', ['as' => 'member-update-startup-executive-team', 'uses'=>'MemberController@update_start_up_executive_team']);
Route::post('member/update-startup/seis-eis', ['as' => 'member-update-startup-seis-eis', 'uses'=>'MemberController@update_start_up_seis']);
Route::post('member/update-startup/bank-account/{id}', ['as' => 'member-update-startup-bank-account', 'uses'=>'MemberController@update_start_up_bank']);

Route::post('member/update-startup/team/save', ['as' => 'member-update-startup-team-member-save', 'uses'=>'MemberController@update_start_up_team_save']);
Route::get('member/update-startup/team/edit/{id}', ['as' => 'member-update-startup-team-edit', 'uses'=>'MemberController@update_start_up_edit_team_edit']);
Route::post('member/update-startup/team/update', ['as' => 'member-update-startup-team-member-update', 'uses'=>'MemberController@update_start_up_team_update']);
Route::get('member/update-startup/team/remove/{id}/{start_up_id}', ['as' => 'member-update-startup-team-remove', 'uses'=>'MemberController@update_start_up_remove_team']);
Route::get('member/update-startup/team/photo/{id}/{start_up_id}', ['as' => 'member-update-startup-team-photo', 'uses'=>'MemberController@edit_start_up_team_photo']);
Route::post('member/update-startup/team/photo-upload', ['as' => 'member-update-startup-team-photo-upload', 'uses'=>'MemberController@update_start_up_team_photo_upload']);


Route::post('member/update-startup/document/save', ['as' => 'member-update-startup-document-save', 'uses'=>'MemberController@update_start_up_document_save']);
Route::get('member/update-startup/document/remove/{id}/{start_up_id}', ['as' => 'member-update-startup-document-remove', 'uses'=>'MemberController@update_start_up_remove_document']);

Route::post('member/update-startup/logo/upload', ['as' => 'member-update-startup-logo-upload', 'uses'=>'MemberController@update_start_up_logo_upload']);

Route::post('member/update-startup/video/save', ['as' => 'member-update-startup-video-save', 'uses'=>'MemberController@update_start_up_video_save']);
Route::get('member/update-startup/video/remove/{id}/{start_up_id}', ['as' => 'member-update-startup-video-remove', 'uses'=>'MemberController@update_start_up_remove_video']);

Route::get('member/update-startup/set-for-review/{id}', ['as' => 'member-update-startup-set-for-review', 'uses'=>'MemberController@update_start_up_submit_for_review']);

Route::delete('member/delete-startup/{id}', ['as' => 'start-up-destroy', 'uses'=>'MemberController@destroy_startup']);

Route::get('member/your-profile', ['as' => 'member-your-proflie', 'uses'=>'MemberController@your_profile']);
Route::get('member/your-profile-edit', ['as' => 'member-your-proflie-edit', 'uses'=>'MemberController@your_profile_edit']);
Route::post('member/your-profile-update', ['as' => 'member-your-proflie-update', 'uses'=>'MemberController@your_profile_update']);
Route::get('member/your-profile-password-change', ['as' => 'member-your-proflie-password-change', 'uses'=>'MemberController@your_profile_change_password']);
Route::post('member/your-profile-password-update', ['as' => 'member-your-proflie-password-update', 'uses'=>'MemberController@your_profile_update_password']);



Route::get('legal/investment_risk', ['as' => 'legal-investment-risk', 'uses'=>'LegalController@investment_risk']);
Route::get('legal/investor_full_terms', ['as' => 'legal-investor-full-terms', 'uses'=>'LegalController@investor_full_terms']);
Route::get('legal/investee_company_terms', ['as' => 'legal-investee-company-terms', 'uses'=>'LegalController@investee_company_terms']);
Route::get('legal/share_offer_terms', ['as' => 'legal-share-offer-terms', 'uses'=>'LegalController@share_offer_terms']);

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


/* -----------------------------------------------------------------------------------------------------------
 * Admin
   ----------------------------------------------------------------------------------------------------------- */
Route::group(['prefix'=> 'admin', 'middleware' => 'auth.administrator'], function() {

	// Show Dashboard (url: http://yoursite.com/admin)
	Route::get('/', ['as' => 'admin.home', 'uses' => 'AdminController@index']);


	Route::get('/users', ['as' => 'admin.users', 'uses' => 'AdminController@show_users']);
	Route::get('/users/view/{id}', ['as' => 'admin.users_view', 'uses' => 'AdminController@view_user']);
	Route::post('users/update-access', ['as' => 'admin.user_update_access', 'uses'=>'AdminController@user_update_access']);

	Route::get('/startups', ['as' => 'admin.startups', 'uses' => 'AdminController@show_start_ups']);
	Route::get('/startups/view/{id}', ['as' => 'admin.startups_view', 'uses' => 'AdminController@view_start_up']);
	Route::post('startups/update-status/{id}', ['as' => 'admin.startups_update_status', 'uses'=>'AdminController@update_start_up_status']);
	Route::post('startups/update-display/{id}', ['as' => 'admin.startups_update_display', 'uses'=>'AdminController@update_start_up_display']);
	Route::post('startups/update-review', ['as' => 'admin.startups_update_review', 'uses'=>'AdminController@update_start_up_review']);
	Route::post('startups/save-notes', ['as' => 'admin.startups_save_notes', 'uses'=>'AdminController@save_start_up_notes']);

	Route::get('startups/send-success/{id}', ['as' => 'admin.startups_send_success', 'uses'=>'AdminController@send_funding_success_email']);
	Route::get('startups/send-failed/{id}', ['as' => 'admin.startups_send_failed', 'uses'=>'AdminController@send_funding_failed_email']);

	Route::get('/investments', ['as' => 'admin.investments', 'uses' => 'AdminController@show_investments']);
	Route::get('/site_options', ['as' => 'admin.site_options', 'uses' => 'AdminController@show_options']);

	Route::get('/reviews', ['as' => 'admin.reviews', 'uses' => 'AdminController@reviews']);
	Route::get('/review-add/{id}', ['as' => 'admin.review-add', 'uses' => 'AdminController@review_add']);
	Route::post('/review-save', ['as' => 'admin.review-save', 'uses' => 'AdminController@review_save']);

});


/* -----------------------------------------------------------------------------------------------------------
 * Admin
   ----------------------------------------------------------------------------------------------------------- */
Route::group(['prefix'=> 'sipp'], function() {

    // Show Dashboard (url: http://yoursite.com/admin)

	Route::get('/investors', ['as' => 'sipp.investors', 'uses' => 'SippProviderController@show_investors']);
	Route::get('/add-investor', ['as' => 'sipp.add-investor', 'uses' => 'SippProviderController@add_investor']);
	Route::post('/save-investor', ['as' => 'sipp.save-investor', 'uses' => 'SippProviderController@save_investor']);
	Route::get('/edit-investor/{id}', ['as' => 'sipp.edit-investor', 'uses' => 'SippProviderController@edit_investor']);
	Route::post('/update-investor', ['as' => 'sipp.update-investor', 'uses' => 'SippProviderController@update_investor']);
	Route::get('/view-investor/{id}', ['as' => 'sipp.view-investor', 'uses' => 'SippProviderController@view_investor']);

	Route::get('/show-investments', ['as' => 'sipp.show-investments', 'uses' => 'SippProviderController@show_investments']);
	Route::get('/investment-update/{id}/{status}', ['as' => 'sipp.update-investment', 'uses' => 'SippProviderController@update_status']);

});
