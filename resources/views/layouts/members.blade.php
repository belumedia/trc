<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="@yield('meta_description', 'The Right Crowd')">
      <meta name="keywords" content="@yield('meta_keywords', 'The Right Crowd')">
      <meta name="author" content="The Right Crowd Ltd.">
      <title>@yield('title', 'The Right Crowd')</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" type="text/css">
      <link href='http://fonts.googleapis.com/css?family=Lato:400,700|Lora:700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="{!! asset('css/main.css'); !!}" type="text/css">
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72943728-1', 'auto');
        ga('send', 'pageview');
      </script>
   </head>
   <script type="text/javascript">
    var _mfq = _mfq || [];
    (function() {
      var mf = document.createElement("script");
      mf.type = "text/javascript"; mf.async = true;
      mf.src = "//cdn.mouseflow.com/projects/604e4b45-dd57-4723-b327-f145b0cd39b9.js";
      document.getElementsByTagName("head")[0].appendChild(mf);
    })();
  </script>
   <body>
      <div class="container-fluid">

         @if(Auth::user()->allow_admin||Auth::user()->is_sipp_provider)
         <div class="row header-top-menu">
            <div class="col-sm-1 col-lg-2"></div>
            <div class="col-sm-10 col-lg-8">
	          @if(Auth::user()->is_sipp_provider)
               <ul>
                  <li><a href="{!! route('sipp.show-investments') !!}">Investments</a></li>
                  <li><a href="{!! route('sipp.investors') !!}">Investors</a></li>
                  <li><strong>SIPP Provider Options</strong></li>
               </ul>
	          @endif
	          @if(Auth::user()->allow_admin)
               <ul>
                  @if(Auth::user()->allow_admin_review)<li><a href="{!! route('admin.reviews') !!}">Reviews</a></li>@endif
                  @if(Auth::user()->allow_admin_review)<li><a href="{!! route('admin.startups') !!}">Startups</a></li>@endif
                  @if(Auth::user()->allow_admin_users)<li><a href="{!! route('admin.users') !!}">Users</a></li>@endif
                  <!-- @if(Auth::user()->allow_admin_investors)<li><a href="{!! route('admin.investments') !!}">Investments</a></li>@endif -->
                  <!-- @if(Auth::user()->allow_admin_options)<li><a href="{!! route('admin.site_options') !!}">Site Options</a></li>@endif -->
                  <li><strong>Admin Options</strong></li>
               </ul>
               @endif
            </div>
         </div>
         @endif

         <div class="row header-logo-bar">
            <div class="col-sm-1 col-lg-2"></div>
            <div class="col-sm-10 col-lg-8">
               <a href="{{ route('member-home') }}"><img src="{!! asset('img/logo_green_black_text.png'); !!}" class="img-responsive"></a>
            </div>
            <div class="col-sm-1 col-lg-2"></div>
         </div>
         <div class="row header-main-menu">
            <div class="col-sm-1 col-lg-2"></div>
            <div class="col-sm-7 col-lg-5">
               <ul>
                  <li><a href="{{ route('member-home') }}">Home</a></li>
                  <!-- <li><a href="{{ route('home') }}">Home</a></li> -->
                  <li><a href="{{ route('member-browse-start-ups') }}">Browse Startups</a></li>
                  <li><a href="{{ route('member-your-investments') }}">Your Investments</a></li>
                  @if(!Auth::user()->is_sipp_acc)<li><a href="{{ route('member-your-startups') }}">Your Start-Ups</a></li>@endif
               </ul>
            </div>
            <div class="col-sm-3 col-lg-3">
               <ul class="header-right-menu">
                  <li><a href="{{ url('/auth/logout') }}">Sign Out</a></li>
                  @if(!Auth::user()->is_sipp_acc)<li><a href="{{ route('member-your-proflie') }}">Your Profile</a></li>@endif
               </ul>
            </div>
            <div class="col-sm-1 col-lg-2"></div>
         </div>

         @yield('content')

         @include('components.footer')
