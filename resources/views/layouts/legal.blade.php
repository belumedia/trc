<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="@yield('meta_description', 'The Right Crowd')">
      <meta name="keywords" content="@yield('meta_keywords', 'The Right Crowd')">
      <meta name="author" content="The Right Crowd Ltd.">
      <title>@yield('title', 'The Right Crowd')</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" type="text/css">
      <link href='http://fonts.googleapis.com/css?family=Lato:400,700|Lora:700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="{!! asset('css/main.css'); !!}" type="text/css">
      <style>

	    ol {
		    counter-reset: item
		}

		li {
			display: block
		}

		li:before {
			content: counters(item, ".") " ";
			counter-increment: item
		}

    </style>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-72943728-1', 'auto');
    ga('send', 'pageview');
    </script>
   </head>
   <script type="text/javascript">
    var _mfq = _mfq || [];
    (function() {
      var mf = document.createElement("script");
      mf.type = "text/javascript"; mf.async = true;
      mf.src = "//cdn.mouseflow.com/projects/604e4b45-dd57-4723-b327-f145b0cd39b9.js";
      document.getElementsByTagName("head")[0].appendChild(mf);
    })();
  </script>
   <body style="background-color: #fff;">
      <div class="container-fluid">
         @yield('content')
      </div>
   </body>
</html>
