@extends('emails.email_template')

@section('content')

<p>Dear {{ $name }}</p>
	
<p>Thank you for your investment commitment of {{ $amount }} in {{ $company }}.</p>

<p>Upon the successful completion of the fund raising by {{ $company }} your funds will be transferred to the company and you shall receive confirmation by email of your completed investment and notification of share holding within {{ $company }} </p>

<p>The Right Crowd will keep you up to date of the company’s progress by email.</p>
		
<p>Please check back at www.therightcrowd.com for new and exciting opportunities  to expand your investment base and spread your risk.</p>
			
@endsection

