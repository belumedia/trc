@extends('emails.email_template')

@section('content')

<p>Dear {{ $name }}</p>

<p>Unfortunately you did not manage to raise the required amount to fulfil your investment request for {{ $company }}.</p>

<p>Please speak to our partner company <a href="http://www.satiogrowth.com">Equity For Growth Ltd</a> to look at Corporate Finance options on a raising elsewhere or re structuring the company to try again on <a href="http://www.therightcrowd.com">The Right Crowd</a>.</p>

<p>If you have any questions or queries please don’t hesitate to contact The Right Crowd.</p>

@endsection
