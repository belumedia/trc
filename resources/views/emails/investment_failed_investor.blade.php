@extends('emails.email_template')

@section('content')

<p>Dear {{ $name }}</p>
	
<p>Unfortunately {{ $company }} didn’t meet its investment target. Your funds have not been debited from your account and are still available for your investment opportunities.</p>

<p>If you have any questions or queries please don’t hesitate to contact The Right Crowd.</p>

<p>Please continue to keep checking www.therightcrowd.com to see more exciting opportunities for your investment. </p>
					
@endsection

