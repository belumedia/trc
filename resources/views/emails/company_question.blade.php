@extends('emails.email_template')

@section('content')

<p>Dear {{ $name }}</p>
	
<p>Someone has asked the following question regarding your start-up {{ $company }}.</p>

<p>"{{ $question }}"</p>
		
<p>Please log into http://www.therightcrowd.com to answer this question.</p>
			
@endsection

