@extends('emails.email_template')

@section('content')
	
	<p>Welcome to The Right Crowd, the definitive name in Crowdfunding.</p>
	
	<p>Your Username is {{ $username }}</p>
	
	<p>Our Executive Board is constantly reviewing new companies looking to raise capital via our site so please check back to see what exciting new opportunities are available. </p>
					
	<p>For any further requests please don’t hesitate to contact us.</p>
	
@endsection

