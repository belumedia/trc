@extends('emails.email_template')

@section('content')

<p>Dear {{ $name }}</p>
	
<p>Congratulations on your successful investment in {{ $company }} your investment of {{ $amount }} has been processed and sent to the company. .</p>

<p>Your share certificate details will follow in the next few days and we will keep you updated on {{ $company }}</p>

<p>If you have any questions or queries please don’t hesitate to contact The Right Crowd</p>

<p>Please continue to keep checking www.therightcrowd.com to see more exciting opportunities for your investment.</p>
			
@endsection

