@extends('emails.email_template')

@section('content')
	
	<p>Hi,</p>
	
	<p>Please click the following link to reset your password allow you access to your account to make investments in exciting new opportunities on The Right Crowd.</p>
	
	<p>Click here to reset your password: {{ url('password/reset/'.$token) }}</p>
	
	<p>If you have any questions or queries please don’t hesitate to contact The Right Crowd.</p>
					
	<p>Please continue to keep checking www.therightcrowd.com to see more exciting opportunities for your investment. </p>
	
@endsection

