@extends('emails.email_template')

@section('content')

<p>Dear {{ $name }}</p>
	
<p>Your investment in {{ $company }} for {{ $amount }} has been refunded.</p>

<p>If you have any questions or queries please don’t hesitate to contact The Right Crowd</p>

<p>Please continue to keep checking www.therightcrowd.com to see more exciting opportunities for your investment.</p>
			
@endsection

