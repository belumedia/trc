<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>The Right Crowd</title>

<style type="text/css">

.ExternalClass {width:100%;}

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
    }
table td {border-collapse:collapse;}

p {margin:0; padding:0; margin-bottom:0;}

h1, h2, h3, h4, h5, h6 {
color: black;
line-height: 100%;
}
a, a:link {
color:#2A5DB0;
text-decoration: underline;
}

               /****** END RESETTING DEFAULTS ********/

               /****** EDITABLE STYLES        ********/

body, #body_style {
    background:#E6E6E6;
    min-height:1000px;
    color:#000;
    font-family:Arial, Helvetica, sans-serif;
    font-size:12px;
}

span.yshortcuts { color:#E6E6E6; background-color:none; border:none;}
span.yshortcuts:hover,
span.yshortcuts:active,
span.yshortcuts:focus {color:#E6E6E6; background-color:none; border:none;}

/*Optional:*/
a:visited { color: #3c96e2; text-decoration: none}
a:focus   { color: #3c96e2; text-decoration: underline}
a:hover   { color: #3c96e2; text-decoration: underline}

.header-base {
	border-bottom: 10px solid #F2F2F2;
}

.body-text {
	padding: 25px;
}

p {
	font-size: 14px;
	margin-bottom: 20px;
}

.social-links {
	text-align: center;
	background-color: #122D56;
}

.social-links img {
	margin: 0px 5px;
}

.footer-text {
	color: #ffffff;
	padding: 25px;
}

.footer-text p {
	font-size: 12px !important;
}

</style>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-72943728-1', 'auto');
ga('send', 'pageview');
</script>
</head>
<script type="text/javascript">
  var _mfq = _mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/604e4b45-dd57-4723-b327-f145b0cd39b9.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>
<body style="background:#E6E6E6; min-height:1000px; color:#000; font-family:Arial, Helvetica, sans-serif; font-size:14px" alink="#FF0000" link="#FF0000" bgcolor="#000000" text="#FFFFFF" yahoo="fix">
<div id="body_style" style="padding:0px 15px 15px;">
        <table cellpadding="15" cellspacing="0" border="0" bgcolor="#ffffff" width="650" align="center">
            <tr>
                <td width="650" class="header-base"><img src="{!! asset('img/logo_green_black_text.png') !!}" border="0"></td>
            </tr>
			<tr>
                <td width="650" class="body-text">

	               	@yield('content')

					<p>Regards</p><br>

					<p>The Right Crowd Team<br>
					therightcrowd.com</p>

                </td>
            </tr>

            <tr>
                <td width="650" bgcolor="#0C598B" color="#ffffff" class="social-links">
					<a href="https://twitter.com/TRCFunding" target="_blank"><img src="{!! asset('img/social_twitter.png') !!}"></a>
					<a href="https://www.linkedin.com/company/therightcrowd-com" target="_blank"><img src="{!! asset('img/social_linkedin.png') !!}"></a>
					<a href="https://www.facebook.com/pages/The-Right-Crowd/1005482409478208" target="_blank"><img src="{!! asset('img/social_facebook.png') !!}"></a>
                </td>
            </tr>
            <tr>
                <td width="650" bgcolor="#0C598B" color="#ffffff" class="footer-text">
	                <p>Copyright © 2015 The Right Crowd All rights reserved. You are receiving this email because you opted in to receive notifications of service updates when you registered for The Right Crowd.</p>
	                <p>The Right Crowd Limited is registered in England and Wales under Companies House Register number 0939850.  The Right Crowd Limited is an appointed representative of Equity For Growth Ltd which is authorised and regulated by the Financial Conduct Authority with registration number 475953.  Equity For Growth Ltd permissions can be checked on the FCA's Register by visiting the FCA's website www.fca.gov.uk/register/ or by contacting the FCA on 0845 606 1234. </p>
	                <p>The information contained in this message is confidential and/or legally privileged material. It is intended for the addressee(s) only. Any unauthorised use, dissemination of the information, or unauthorised copying/forwarding of this message is prohibited. If you are not the intended addressee, please notify the sender immediately by return e-mail and delete this message. The contents of this communication are not intended for distribution to, or use by, any person or entity in any location where such distribution or use would be contrary to law or regulation, or which would subject us to any registration requirements within such location. This communication is not intended to nor should it be taken to create any legal relations or contractual relationship. This message and any files attached with it are provided for informational purposes only and any reference to securities or related financial instruments should be regarded as marketing material unless clearly marked otherwise. Marketing materials are not prepared in accordance with legal requirements designed to promote the independence of investment research and are not subject to any prohibition on dealing ahead of the dissemination of investment research. This message should not be construed as an offer, recommendation or solicitation to buy or sell any securities or related financial instruments. All market prices, data and other information are not warranted as to completeness or accuracy and are subject to change without notice. Investments can give rise to substantial risk and may not be suitable for all investors. Any potential investor should seek their own independent investment, tax and accounting advice before entering into any investments. Information about any views expressed in this message are those of the individual sender, except where the message states otherwise and the sender is authorised to state them. Internet communications cannot be guaranteed to be secure or error free as information may be intercepted, corrupted, lost, arrive late or contain viruses. The Right Crowd Limited and its affiliates or associated companies do not accept liability for any errors, admission or losses which arise from this internet transmission. We reserve the right to monitor all communications for compliance with legal, regulatory and professional standards.’</p>
                </td>
            </tr>
        </table>

    </div>


</body>
</html>
