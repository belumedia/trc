@extends('emails.email_template')

@section('content')

<p>Dear {{ $name }}</p>

<p>Congratulations on your successful raising for {{ $company }} a total amount of {{ $investment_total }} has been processed and  will be sent to your designated account in 15 business days after deducting The Right Crowd Fees. </p>

<p>The Investors will have received notification from The Right confirming their investment has been successful and our accountants will issue the share certificates on your behalf.</p>

<p>If you have any questions or queries please don’t hesitate to contact The Right Crowd.</p>

<p>Please continue to keep checking www.therightcrowd.com to see more exciting opportunities for your investment.</p>
		
@endsection

