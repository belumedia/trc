@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<?php $active = "logo"; ?>
		@include('partials.start_up_navigator')
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">


		@if(Session::has('error_message'))
			<br>
			<div class="alert alert-danger" role="alert">{!! Session::get('error_message'); !!}</div>
		@endif


		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif

		<h2>Logo</h2>
		<p>Please use this page to upload a company logo, make sure that your image is bigger than 400px x 250px.</p>
		<p>Also make sure that the file size of your image is no bigger than 2mb.</p>


		{!! Form::open(['route' => 'member-update-startup-logo-upload', 'files' => true]) !!}
		<div class="panel panel-default">
		  <div class="panel-body">
		    <div class="form-group">
	            {!! Form::file('logo_file', null, ['class' => 'form-control', 'placeholder' => 'Your company logo as jpeg or gif']); !!}
	        </div>
	        <button class="btn btn-primary btn-sm">Upload Logo</button>
		  </div>
		</div>
		{!! Form::hidden('start_up_id', $start_up->id); !!}
		</form>

		<h5>Current Logo</h5>

		@if(!empty($start_up->logo_file))
			<img src="{{ asset('photo_uploads/resized_'.$start_up->logo_file.'.jpg') }}" border="0">
		@else
			<h3>No logo image currently...</h3>
			<p>Use the form above to upload a new logo.</p>
		@endif

		<hr>
        <a href="{{ route('member-your-startups') }}" class="btn btn-default pull-right">Exit</a>
        <a href="{{ route('member-edit-startup-seis', ['id' => $start_up->id]) }}" class="btn btn-primary">Continue</a>

		 </form>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
