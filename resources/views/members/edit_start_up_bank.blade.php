@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>{{ $start_up->full_business_name }}</h1>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-4 col-lg-2">
      <?php $active = "bank"; ?>
      @include('partials.start_up_navigator')
   </div>
   <div class="col-sm-8 col-lg-6 content-sub-page">

      <h2>Payments</h2>
      <p>In order to receive payments from investors your going to need to have your own Stripe account.</p>
      <p>Please click on the connect button below to either connect your existing account with Stripe or to create a new one.</p>
      <p>Please note that all payment transfers will be subject to 2.4% + 20p transaction charge.</p>
      @if($user->stripe_connected)
      <div class="alert alert-success" role="alert"><span class="glyphicon glyphicon-ok"></span>  <strong>Stripe Connected</strong> - Your stripe account is connected.</div>
      @else
      <div class="row">
         <div class="col-sm-4">
            <br>
            <a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id={{ Config::get('stripe.stripe.client_id') }}&scope=read_write&stripe_landing=register&redirect_uri=https://www.therightcrowd.com/member/transactions/connect" class="btn btn-primary btn-block">New Stripe Account</a><br>
         </div>
         <div class="col-sm-8">
            <h3>New Stripe Customer</h3>
            <p>If you don't have a stripe account, please click on this button to create an account on Stripe.</p>
         </div>
      </div>
      <hr>
      <div class="row">
         <div class="col-sm-4">
            <br>
            <a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id={{ Config::get('stripe.stripe.client_id') }}&scope=read_write&stripe_landing=login&redirect_uri=https://www.therightcrowd.com/member/transactions/connect" class="btn btn-primary btn-block">Connect to Stripe</a><br>
         </div>
         <div class="col-sm-8">
            <h3>Existing Stripe Member</h3>
            <p>If you already have a stripe account, please click on this button to connect your Stripe account to The Right Crowd.</p>
         </div>
      </div>

      @endif


      <!--
      <div role="tabpanel">

        <ul class="nav nav-tabs" role="tablist">
         <li role="presentation" class="active"><a href="#stripe" aria-controls="home" role="tab" data-toggle="tab">Stripe</a></li>
         <li role="presentation"><a href="#gocardless" aria-controls="profile" role="tab" data-toggle="tab">Go Cardless</a></li>
         <li role="presentation"><a href="#paypal" aria-controls="home" role="tab" data-toggle="tab">PayPal</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="stripe">



            <ol>
               <li>Sign-in to your Stripe account</li>
               <li>In the control panel click on your name / company in the top right hand corner.</li>
               <li>Select "Account Settings".</li>
               <li>In the pop-up window click on API keys.</li>
               <li>Copy your live keys into the field below.</li>
            </ol>
             <hr>
             <div class="form-group">
                  <label for="stripe_secret_key">Secret Key</label>
                  {!! Form::text('stripe_secret_key', null, ['class' => 'form-control', 'placeholder' => 'sk_live_XXXXXXXXXXXXXXXXXXX']); !!}
                  <span id="helpBlock" class="help-block">Stripe API Secret Key</span>
              </div>
              <div class="form-group">
                  <label for="stripe_publishable_key">Publishable Key</label>
                  {!! Form::text('stripe_publishable_key', null, ['class' => 'form-control', 'placeholder' => 'pk_live_XXXXXXXXXXXXXXXXXXX']); !!}
                  <span id="helpBlock" class="help-block">Stripe API Publishable Key</span>
              </div>

          </div>
          <div role="tabpanel" class="tab-pane text-center" id="gocardless"><h5>GoCardless will available shortly!</h5></div>
         <div role="tabpanel" class="tab-pane text-center" id="paypal"><h5>PayPal will available shortly!</h5></div>
        </div>

      </div>
      -->
   <hr>
   <a href="{{ route('member-your-startups') }}" class="btn btn-default pull-right">Exit</a>


</div>
<div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
