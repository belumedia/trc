@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<?php $active = "business"; ?>
		@include('partials.start_up_navigator')
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">

		<h2>Business Plan</h2>
		<hr>
			{!! Form::model($start_up, array('route' => array('member-update-startup-business-plan', $start_up->id))) !!}

			<h4>Your Product or Service</h4>
	        <div class="form-group">
	            <label for="product_details">Full details about your product or service</label>
	            <span id="helpBlock" class="help-block">An overview of what product(s) or service(s) your business intends to offer.</span>
							<span id="chars"></span>
	            {!! Form::textarea('product_details', null, ['class' => 'form-control', 'maxlength' => '600']); !!}

	        </div>
	        <div class="form-group">
	            <label for="market_impact">Differentiation &amp; Market Impact</label>
	            <span id="helpBlock" class="help-block">How does your business differentiate its products or services from others. What impact will this have on your market(s)?</span>
	            {!! Form::textarea('market_impact', null, ['class' => 'form-control', 'maxlength' => '600']); !!}
	        </div>
	        <div class="form-group">
	            <label for="acheivements">Achievements</label>
	            <span id="helpBloc" class="help-block">Describe what you have achieved so far with your business.</span>
	            {!! Form::textarea('acheivements', null, ['class' => 'form-control',  'maxlength' => '600', 'rows' => '4']); !!}
	        </div>
	        <div class="form-group">
	            <label for="monetisation_strategy">Monetisation strategy</label>
	            <span id="helpBloc" class="help-block">How will you/do you make money? Current or potential revenue streams.</span>
	            {!! Form::textarea('monetisation_strategy', null, ['class' => 'form-control',  'maxlength' => '600' , 'rows' => '4']); !!}
	        </div>
	        	        <div class="form-group">
	            <label for="use_of_funds">Use of funds</label>
	            <span id="helpBloc" class="help-block">What would you use funding for?</span>
	            {!! Form::textarea('use_of_funds', null, ['class' => 'form-control',  'maxlength' => '600' , 'rows' => '4']); !!}
	        </div>



			<hr>
	        <a href="{{ route('member-your-startups') }}" class="btn btn-default pull-right">Exit</a>
	        <button class="btn btn-primary">Save &amp; Continue</button>

		 </form>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection
