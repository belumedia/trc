@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>{{ $start_up->full_business_name }}</h1>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-4 col-lg-2">
      <a href="#" class="thumbnail">
     	@if(!empty($start_up->logo_file))
			<img src="{!! asset('img/company_photo_holder.png'); !!}" class="content-company-profile-img" style="background-image: url({!! asset('photo_uploads/resized_'.$start_up->logo_file.'.jpg'); !!});"> 
		@else
			<img src="{!! asset('img/no_photo_startup.jpg'); !!}" class="content-company-profile-img">
		@endif
      </a>
   </div>
   <div class="col-sm-8 col-lg-6 content-full-page">

      @if(Session::has('message'))
      <br>
      <div class="alert alert-danger" role="alert">{!! Session::get('message'); !!}</div>
      @endif

      

      <h2>Thank You!</h2>
      <hr>
	  <p>Your investment has been confirmed and the amount you have chosen to Invest will be debited from the card provided at the end of the funding period.</p>
	  <p>The Right Crowd will send you updates by email keeping you informed of the funding progress.</p>
	  <p>Please check for other opportunities and remember to always think about spreading your investments across a number of different companies.</p>
      <hr>
      <a href="{{ route('member-your-investments') }}" class="btn btn-default pull-right">My Investments</a>
      <a href="{{ route('member-browse-start-ups') }}" class="btn btn-primary">Browse More Start-Ups</a>
      
      


   </div>

</div>
@endsection