@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h1>Investor</h1>
	<p>Please select which type of investor you are and accept the terms.</p>
	<br>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Certified High Net Worth Individual</h3>
					</div>
					<div class="panel-body content-investor-panel">
						<p>I declare that I am a certified high net worth individual for the purposes of the Financial Services and Markets Act 2000 (Financial Promotion) Order 2001.</p>
						<div class="content-scroll-container">
							<p>I understand that this means: </p>
							<ol type="A">
								<li>I can receive financial promotions that may not have been approved by a person authorised by the Financial Services Authority;</li>
								<li>the content of such financial promotions may not conform to rules issued by the Financial Services Authority;</li>
								<li><strong>by signing this statement I may lose significant rights;</strong></li>
								<li>I may have no right to complain to either of the following:
									<ol type="I">
										<li>the Financial Services Authority; or </li>
										<li>the Financial Ombudsman Scheme; </li>
									</ol>
								</li>
								<li>I may have no right to seek compensation from the Financial Services Compensation Scheme. </li>
							</ol>
							<p>I am a certified high net worth individual because <strong>at least one of the following applies:</strong></p>
							<ol type="A">
								<li>I had, during the financial year immediately preceding the date below, an annual income to the value of £100,000 or more;</li>
								<li>I held, throughout the financial year immediately preceding the date below, net assets to the value of £250,000 or more. Net assets for these purposes do not include:
									<ol type="I">
										<li>the property which is my primary residence or any loan secured on that residence;</li>
										<li>any rights of mine under a qualifying contract of insurance within the meaning of the Financial Services and Markets Act 2000 (Regulated Activities) Order 2001; or</li>
										<li>any benefits (in the form of pensions or otherwise) which are payable on the termination of my service or on my death or retirement and to which I am (or my dependants are), or may be, entitled.</li>
									</ol>
								</li>
								
							</ol>
						</div>
						<p><strong>I accept that I can lose my property and other assets from making investment decisions based on financial promotions. </strong></p>
						<p>I am aware that it is open to me to seek advice from someone who specialises in advising on investments.</p>
					</div>
					<div class="panel-footer">
						<div class="checkbox">
					    <label>
					      <input type="checkbox" id="checkbox-1" onClick="set_button_active(1);"> I confirm that the above declaration is true and complete
					    </label>
					  </div>
						<a href="{!! route('member-investor-set-type', ['type' => 'HIGH_NET_WORTH' ]) !!}" class="btn btn-primary btn-block disabled" id="button-1">Accept & Continue</a>
					</div>

				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Self-Certified Sophisticated Investor</h3>
					</div>
					<div class="panel-body content-investor-panel">
						<p>I declare that I am a self-certified sophisticated investor for the purposes of the Financial Services and Markets Act (Financial Promotion) Order 2001. <br><br></p>
						<div class="content-scroll-container">
							<p>I understand that this means: </p>
							<ol type="A">
								<li>I can receive financial promotions that may not have been approved by a person authorised by the Financial Services Authority;</li>
								<li>the content of such financial promotions may not conform to rules issued by the Financial Services Authority;</li>
								<li><strong>by signing this statement I may lose significant rights;</strong></li>
								<li>I may have no right to complain to either of the following:
									<ol type="I">
										<li>the Financial Services Authority; or </li>
										<li>the Financial Ombudsman Scheme; </li>
									</ol>
								</li>
								<li>I may have no right to seek compensation from the Financial Services Compensation Scheme. </li>
							</ol>
							<p>I am a self-certified sophisticated investor because <strong>at least one of the following applies:</strong></p>
							<ol type="A">
								<li>I am a member of a network or syndicate of business angels and have been so for at least the last six months prior to the date below; </li>
								<li>I have made more than one investment in an unlisted company in the two years prior to the date below; </li>
								<li>I am working, or have worked in the two years prior to the date below, in a professional capacity in the private equity sector, or in the provision of finance for small and medium enterprises;</li>
								<li>I am currently, or have been in the two years prior to the date below, a director of a company with an annual turnover of at least £1 million. </li>
							</ol>
						</div>
						<p><strong>I accept that I can lose my property and other assets from making investment decisions based on financial promotions. </strong></p>
						<p>I am aware that it is open to me to seek advice from someone who specialises in advising on investments.</p>
					</div>
					<div class="panel-footer">
						<div class="checkbox">
					    <label>
					      <input type="checkbox" id="checkbox-2" onClick="set_button_active(2);"> I confirm that the above declaration is true and complete
					    </label>
					  </div>
						<a href="{!! route('member-investor-set-type', ['type' => 'SOPHISTICATED' ]) !!}" class="btn btn-primary btn-block disabled" id="button-2">Accept & Continue</a>
					</div>

				</div>

				
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Restricted Investor</h3>
					</div>
					<div class="panel-body content-investor-panel">
						<p>I make this statement so that I can receive promotional communications relating to non-readily realisable securities as a restricted investor.</p>
							<div class="content-scroll-container">
							<p>I declare that I qualify as a restricted investor because:</p>
							<ol type="A">
								<li>in the twelve months preceding the date below, I have not invested more than 10% of my net assets in non-readily realisable securities; and</li>
								<li>I undertake that in the twelve months following the date below, I will not invest more than 10% of my net assets in non-readily realisable securities.</li>
							</ol>
							<p>Net assets for these purposes do not include:</p>
							<ol type="A">
								<li>the property which is my primary residence or any money raised through a loan secured on that property;</li>
								<li>any rights of mine under a qualifying contract of insurance; or</li>
								<li>any benefits (in the form of pensions or otherwise) which are payable on the termination of my service or on my death or retirement and to which I am (or my dependents are), or may be entitled.</li>
							</ol>
						</div>
						<p><strong>I accept that the investments to which the promotions will relate may expose me to a significant risk of losing all of the money or other property invested.</strong></p>
						<p>I am aware that it is open to me to seek advice from an authorised person who specialises in advising on non-readily realisable securities.</strong></p>
					</div>
					<div class="panel-footer">
						<div class="checkbox">
					    <label>
					      <input type="checkbox" id="checkbox-3" onClick="set_button_active(3);"> I confirm that the above declaration is true and complete
					    </label>
					  </div>
						<a href="{!! route('member-investor-set-type', ['type' => 'RESTRICTED' ]) !!}" class="btn btn-primary btn-block disabled" id="button-3">Accept & Continue</a>
					</div>
				</div>
			</div>

		</div>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


@endsection

@section('footer-scripts')

<script type="text/javascript">
function set_button_active(button){
	
	$('#checkbox-1').prop('checked', false);
	$('#checkbox-2').prop('checked', false);
	$('#checkbox-3').prop('checked', false);
	
	$('#button-1').addClass('disabled');
	$('#button-2').addClass('disabled');
	$('#button-3').addClass('disabled');

	if(button==1){
		$('#button-1').removeClass('disabled');
		$('#checkbox-1').prop('checked', true);
	}
	
	if(button==2){
		$('#button-2').removeClass('disabled');
		$('#checkbox-2').prop('checked', true);
	}
	
	if(button==3){
		$('#button-3').removeClass('disabled');
		$('#checkbox-3').prop('checked', true);
	}
}
</script>

@endsection
