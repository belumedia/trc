@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>Questions for {{ $start_up->full_business_name }}</h1>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-4 col-lg-2">
      <div class="thumbnail">
			@if(!empty($start_up->logo_file))
				<img src="{!! asset('img/company_photo_holder.png'); !!}" class="content-company-profile-img" style="background-image: url({!! asset('photo_uploads/resized_'.$start_up->logo_file.'.jpg'); !!});"> 
			@else
				<img src="{!! asset('img/no_photo_startup.jpg'); !!}" class="content-company-profile-img">
			@endif
      </div>
   </div>
   <div class="col-sm-8 col-lg-6 content-sub-page content-sub-page-top-pad">
      <h2>Questions & Answers</h2>
      	
      	<h3>{{ $question->question }}</h3>
	  	<br>
		{!! Form::model($question, ['route' => ['member-startup-questions-save-answer']]) !!}
		<div class="form-group">
		{!! Form::textarea('answer', null, ['class' => 'form-control', 'rows' => '8', 'placeholder' => 'Please enter your answer in this box.']); !!}
		</div>
		<button class="btn btn-primary">Save Answer</button>
		{!! Form::hidden('question_id', $question->id); !!}
		{!! Form::close(); !!}
      
      

      <hr>
      <a href="{{ route('member-your-startups') }}" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
   </div>
</div>

@endsection
