@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<?php $active = "team"; ?>
		@include('partials.start_up_navigator')
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">
		
		
		@if(Session::has('message_error'))
			<br>
			<div class="alert alert-danger" role="alert">{!! Session::get('message_error'); !!}</div>
		@endif

		
		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif
		
		<h2>Executive</h2>
		<p>Please use this section to upload any documentation that supports your application.  Please note that this information will be shared with potential investors.</p>
		
		
		{!! Form::open(['route' => 'member-update-startup-team-member-save', 'files' => true]) !!}
		<div class="panel panel-default">
		  <div class="panel-body">
		    <div class="form-group">
	            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']); !!}
	        </div>
	        <div class="form-group">
	            {!! Form::text('position', null, ['class' => 'form-control', 'placeholder' => 'Position']); !!}
	        </div>
	        <div class="form-group">
	            {!! Form::text('age', null, ['class' => 'form-control', 'placeholder' => 'Age in Years']); !!}
	        </div>
	        <div class="form-group">
	            {!! Form::textarea('personal_bio', null, ['class' => 'form-control', 'placeholder' => 'Personal Biography', 'rows' => '3']); !!}
	        </div>
	        <div class="form-group">
	            {!! Form::textarea('involvement', null, ['class' => 'form-control', 'placeholder' => 'Involvement in the business', 'rows' => '3']); !!}
	        </div>
	        <button class="btn btn-primary btn-sm">Add Team Member</button>
		  </div>
		</div>
		{!! Form::hidden('start_up_id', $start_up->id); !!}
		</form>

		<h5>Current Executive Team Members</h5>
		<table class="table table-striped">
			<thead>
			<tr>
				<th class="col-sm-1"></th>
				<th>Name</th>
				<th>Positon</th>
				<th class="col-sm-1">Edit</th>
				<th class="col-sm-1">Photo</th>
				<th class="col-sm-1">Remove</th>
			</tr>
			</thead>
			<tbody>
				@if(count($team_members)>0)
				@foreach($team_members as $team_member)
				<tr>
					<td>
						@if($team_member->photo_url)
                    	<img src="{!! asset('img/team_photo_holder.png'); !!}" alt="{{ $team_member->name }}" class="content-company-team-img"  style="background-image: url({!! asset('team_photo_uploads/resized_'.$team_member->photo_url.'.jpg'); !!}); width:100%;"><br><br>
						@endif
					</td>
					<td><strong>{{ $team_member->name }}</strong><br>{{ $team_member->description }}</th>
					<td><strong>{{ $team_member->position }}</strong><br>{{ $team_member->description }}</th>
					<th class="col-sm-1"><a class="btn btn-xs btn-default" href="{{ route('member-update-startup-team-edit', ['id' => $team_member->id]) }}">Edit</a></th>
					<th class="col-sm-1"><a class="btn btn-xs btn-default" href="{{ route('member-update-startup-team-photo', ['id' => $team_member->id, 'start_up_id' => $start_up->id]) }}">Add Photo</a></th>
					<th class="col-sm-1"><a class="btn btn-xs btn-danger" href="{{ route('member-update-startup-team-remove', ['id' => $team_member->id, 'start_up_id' => $start_up->id]) }}">Remove</a></th>
				</tr>
				@endforeach
				@else
				<tr>
					<td colspan="4" class="text-center"><h5>No Executive Team Currently..</h5></td>
				</tr>
				@endif
			</tbody>
		</table>
				        
			<hr>
	        <a href="{{ route('member-your-startups') }}" class="btn btn-default pull-right">Exit</a>
	        <a href="{{ route('member-edit-startup-seis', ['id' => $start_up->id]) }}" class="btn btn-primary">Continue</a>

		 </form>
		
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
