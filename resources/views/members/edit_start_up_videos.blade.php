@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<?php $active = "videos"; ?>
		@include('partials.start_up_navigator')
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">
		
		
		@if(Session::has('message_error'))
			<br>
			<div class="alert alert-danger" role="alert">{!! Session::get('message_error'); !!}</div>
		@endif

		
		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif
		
		<h2>Videos</h2>
		<p>Please use this section to add videos to support your application. </p>
		
		
		{!! Form::open(['route' => 'member-update-startup-video-save']) !!}
		<div class="panel panel-default">
		  <div class="panel-body">
		    <div class="form-group">
	            {!! Form::text('video_url', null, ['class' => 'form-control', 'placeholder' => 'Video share URL from YouTube']); !!}
	        </div>
	        <button class="btn btn-primary btn-sm">Add Video</button>
		  </div>
		</div>
		{!! Form::hidden('start_up_id', $start_up->id); !!}
		</form>

		<h5>Current Videos</h5>
		<table class="table table-striped">
			<thead>
			<tr>
				<th>URL</th>
				<th class="col-sm-1">View</th>
				<th class="col-sm-1">Remove</th>
			</tr>
			</thead>
			<tbody>
				@if(count($videos)>0)
				@foreach($videos as $video)
				<tr>
					<td>{{ $video->video_url }}</th>
					<th class="col-sm-1"><a class="btn btn-xs btn-default" href="{{ $video->video_url }}" target="_blank">View</a></th>
					<th class="col-sm-1"><a class="btn btn-xs btn-danger" href="{{ route('member-update-startup-video-remove', ['id' => $video->id, 'start_up_id' => $start_up->id]) }}">Remove</a></th>
				</tr>
				@endforeach
				@else
				<tr>
					<td colspan="3" class="text-center"><h5>No Videos Currently..</h5></td>
				</tr>
				@endif
			</tbody>
		</table>
				        
			<hr>
	        <a href="{{ route('member-your-startups') }}" class="btn btn-default pull-right">Exit</a>
	        <a href="{{ route('member-edit-startup-seis', ['id' => $start_up->id]) }}" class="btn btn-primary">Continue</a>

		 </form>
		
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
