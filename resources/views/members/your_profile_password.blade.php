@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
		@if(Session::has('error_message'))
			<br>
			<div class="alert alert-danger" role="alert">{!! Session::get('error_message'); !!}</div>
		@endif

	   
      <h1>Change Contact Details</h1>
      <p>Please use the form below to make changes to your contact details.</p>
   </div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
			
		{!! Form::open(['route' => 'member-your-proflie-password-update', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
		
		<h4>Your Old Password</h4>
		
		<div class='form-group'>
			<label for='first_name' class='col-sm-2 control-label'>Old Password</label>
			<div class="col-sm-10">
			{!! Form::password('password', ['class' => 'form-control']) !!}
			</div>
		</div>

		<h4>Your New Password</h4>

		<div class='form-group'>
			<label for='last_name' class='col-sm-2 control-label'>New Password</label>
			<div class="col-sm-10">
			{!! Form::password('new_password', ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class='form-group'>
			<label for='telephone' class='col-sm-2 control-label'>Re-Type New Password</label>
			<div class="col-sm-10">
				{!! Form::password('retype_new_password', ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class='form-group'>
			<label for='postcode' class='col-sm-2 control-label'></label>
			<div class="col-sm-10">
				<input type='submit' value='Change Password' class='btn btn-primary'>
			</div>
		</div>
		
		{!! Form::close() !!}


	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">


	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection
