@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>Your Investments</h1>
      <a href="{{ route('member-browse-start-ups') }}" class="btn btn-primary btn-sm pull-right"><span class="glyphicon glyphicon-search"></span> Browse Startups</a>

   </div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
         
         
         @if(count($investments_incomplete))
         <h2>To Complete</h2>
         <p>These are investments you have started, but not completed.</p>
         <table class="table table-striped">
            <thead>
               <tr>
                  <th class="col-sm-2">Date</th>
                  <th>Company</th>
                  <th class="col-sm-2">Amount</th>            
                  <th class="col-sm-3"></th>
                  <th class="col-sm-1"></th>
               </tr>
            </thead>
            <tbody>
               @foreach($investments_incomplete as $investment)
               <?php $investment->get_investment_status(); ?>
               <tr>
                  <td>{{ $investment->updated_at->toFormattedDateString() }}</td>
                  <td>{{ $investment->startups->full_business_name }}</td>
                  <td>&pound;{{ $investment->amount }}</td>
                  @if(Auth::user()->is_sipp_acc)
                  <td class="text-right" colspan="2">Investment Confirmed</td>
                  @else
                  <td>@if($investment->can_make_payment) <a href="{!! route('member-company-payment', ['start_up_id' => $investment->start_up_id, 'investment_id' => $investment->id]) !!}" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-credit-card"></span> Add Credit Card</a> &nbsp;@endif</td>
                     
                  <td class="text-right">
                     @if($investment->can_cancel)
                     {!! Form::open(['route' => ['investment-destroy', $investment->id], 'method' => 'delete', 'onsubmit' => 'return confirm("Are you sure you want to cancel this investment?");'] ) !!}
                     <button type="submit" class="btn btn-danger btn-xs">Cancel Investment</button>
                     {!! Form::close() !!}
                     @endif
                  </td>
                  @endif
               </tr>
               @endforeach
            </tbody>
         </table>
         <br>
         @endif

         @if(count($investments_pending))
         <h2>Pending Investments</h2>
         <p>These are investments you have made in start-ups that are still trying to reach there funding goal.</p>
         
         <table class="table table-striped">
            <thead>
               <tr>
                  <th class="col-sm-2">Date</th>
                  <th>Company</th>
                  <th class="col-sm-2">Amount</th>            
                  <th class=""></th>
               </tr>
            </thead>
            <tbody>
                @foreach($investments_pending as $investment)
               <?php $investment->get_investment_status(); ?>
               <tr>
                  <td>{{ $investment->updated_at->toFormattedDateString() }}</td>
                  <td>{{ $investment->startups->full_business_name }}</td>
                  <td>&pound;{{ $investment->amount }}</td>     
                  <td class="text-right">
                     <a href="{!! route('member-company-invest', ['id' => $investment->start_up_id]) !!}" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-retweet"></span> Invest Again</a>
            
              
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
         <br>
         @endif
         
         @if(count($investments_complete))
         <h2>Completed</h2>
         <p>These are investments you have made in start-ups that have now been used to fund the company.</p>
         <table class="table table-striped">
            <thead>
               <tr>
                  <th class="col-sm-2">Date</th>
                  <th>Company</th>
                  <th class="col-sm-2">Amount</th>  
               </tr>
            </thead>
            <tbody>
               @foreach($investments_complete as $investment)
               <?php $investment->get_investment_status(); ?>
               <tr>
                  <td>{{ $investment->updated_at->toFormattedDateString() }}</td>
                  <td>{{ $investment->startups->full_business_name }}</td>
                  <td>&pound;{{ $investment->amount }}</td>  
               </tr>
               @endforeach
            </tbody>
         </table>
         <br>
         @endif
         
         
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection
