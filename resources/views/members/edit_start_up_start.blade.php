@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<?php $active = ""; ?>
		@include('partials.start_up_navigator')
		
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">
		
		<h2>Getting Started</h2>
		<hr>
		<p>Adding your business is simple and easy, please complete all sections below.  You don't have to complete all sections in one go and you can come back make changes at any time.  Once you have completed all sections you can click the submit button below and your application will be sent to the board for approval.</p>
		
		@if($start_up->is_edit_complete())
		
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><strong>Congratulations</strong> - Startup Ready For Review</h3>
		  	</div>
		  	<div class="panel-body">
		    	<p>It's looks like you've entered all the information required in order for you to submit your startup for board review.</p>
		    	<p>Please <strong>review the Investee Standard Terms and Conditions</strong> below.</p>
		  	</div>
		  	<div class="panel-body">
				<div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="{!! route('legal-investee-company-terms') !!}"></iframe>
            </div>
		  	</div>
		  	<div class="panel-footer text-right">
			  	<span class="pull-left">By clicking on the "Submit for Review" button, I accept the Investee Standard Terms and Conditions</span>
			  	<div class="clearfix"></div>
		  	</div>
		  	<div class="panel-footer text-right">
			  	<a href="{{ route('member-update-startup-set-for-review', ['id' => $start_up->id]) }}" class="btn btn-sm btn-primary">Submit For Review</a>
			</div>
		</div>
		
		@endif
				
			<table class="table">
				<thead>
					<tr>
						<th>Section</th>
						<th class="col-sm-2">Status</th>
						<th class="col-sm-2">Edit</th>
					</tr>
				</thead>
				<tbody>
					
					<tr>
						<td><strong>Company Details</strong><br><p>Please enter details about your company in this section.</p></td>
						<td>@if($start_up->is_edit_compnay_complete())<span class="label label-success">Complete</span>@else<span class="label label-default">In-Complete</span>@endif</td>
						<td><a href="{{ route('member-edit-startup-company', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Edit Section</a></td>
					</tr>
					
					<tr>
						<td><strong>Business Plan</strong><br><p>Use this section to enter details about the direction of your business.</p></td>
						<td>@if($start_up->is_edit_business_complete())<span class="label label-success">Complete</span>@else<span class="label label-default">In-Complete</span>@endif</td>
						<td><a href="{{ route('member-edit-startup-business', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Edit Section</a></td>
					</tr>
					
					<tr>
						<td><strong>Market Research</strong><br><p>Enter details about your target market and your marketing strategy.</p></td>
						<td>@if($start_up->is_edit_market_complete())<span class="label label-success">Complete</span>@else<span class="label label-default">In-Complete</span>@endif</td>
						<td><a href="{{ route('member-edit-startup-market', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Edit Section</a></td>
					</tr>
					
					<tr>
						<td><strong>Executive Team</strong><br><p>Add your executive team members into this section.</p></td>
						<td>@if($start_up->is_edit_executive_complete())<span class="label label-success">Complete</span>@else<span class="label label-default">In-Complete</span>@endif</td>
						<td><a href="{{ route('member-edit-startup-team', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Edit Section</a></td>
					</tr>
					
					<tr>
						<td><strong>Document Upload</strong><br><p>Upload any documents that support your business application.</p></td>
						<td><!-- @if($start_up->is_edit_documents_complete())<span class="label label-success">Complete</span>@else<span class="label label-default">In-Complete</span>@endif--></td>
						<td><a href="{{ route('member-edit-startup-documents', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Edit Section</a></td>
					</tr>
					
					<tr>
						<td><strong>SEIS + EIS</strong><br><p>Complete this section to work out whether your business would provide tax relief to our investors.</p></td>
						<td>@if($start_up->is_edit_seis_complete())<span class="label label-success">Complete</span>@else<span class="label label-default">In-Complete</span>@endif</td>
						<td><a href="{{ route('member-edit-startup-seis', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Edit Section</a></td>
					</tr>
					
					<tr>
						<td><strong>Payments</strong><br><p>Ensure your account is connected to Stripe to take payments once your funding has been completed.</p></td>
						<td>@if($user->stripe_connected)<span class="label label-success">Complete</span>@else<span class="label label-default">In-Complete</span>@endif</td>
						<td><a href="{{ route('member-edit-startup-bank', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Edit Section</a></td>
					</tr>
					
				</tbody>
			</table>
			
			
				        	        
			<hr>
	        <a href="{{ route('member-your-startups') }}" class="btn btn-default pull-right">Exit</a>
	
		
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
