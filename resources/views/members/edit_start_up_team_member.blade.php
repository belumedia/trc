@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>{{ $start_up->full_business_name }}</h1>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-4 col-lg-2">
      <?php $active = "team"; ?>
      @include('partials.start_up_navigator')

   </div>
   <div class="col-sm-8 col-lg-6 content-sub-page">

      <h2>Edit Team Member</h2>
      <hr>
      {!! Form::model($team_member, array('route' => array('member-update-startup-team-member-update', $team_member->id))) !!}
         <div class="form-group">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']); !!}
         </div>
         <div class="form-group">
            {!! Form::text('position', null, ['class' => 'form-control', 'placeholder' => 'Position']); !!}
         </div>
         <div class="form-group">
            {!! Form::text('age', null, ['class' => 'form-control', 'placeholder' => 'Age in Years']); !!}
         </div>
         <div class="form-group">
            {!! Form::textarea('personal_bio', null, ['class' => 'form-control', 'placeholder' => 'Personal Biography', 'rows' => '3']); !!}
         </div>
         <div class="form-group">
            {!! Form::textarea('involvement', null, ['class' => 'form-control', 'placeholder' => 'Involvement in the business', 'rows' => '3']); !!}
         </div>
         {!! Form::hidden('id', $team_member->id); !!}
         {!! Form::hidden('start_up_id', $team_member->start_up_id); !!}

         <hr>
         <a href="{{ route('member-edit-startup-team', ['id' => $team_member->start_up_id]) }}" class="btn btn-default pull-right">Exit</a>
         <button class="btn btn-primary" >Update Team Member</button>

      </form>

   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
