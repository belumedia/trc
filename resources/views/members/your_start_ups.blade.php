@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>Your Start-Ups</h1>
		<a href="{{ route('member-add-startup') }}" class="btn btn-primary btn-sm pull-right"><span class="glyphicon glyphicon-plus"></span> Add New Startup</a>
	</div>
		
	<div class="col-sm-1 col-lg-2"></div>
</div>




<div class="row content-section content-section-spacer-base">
	<br><br>
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		
		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif
		
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Startup Name</th>
					<th class="col-sm-2">Status</th>
					<th class="col-sm-2"></th>
				</tr>
			</thead>
			<tbody>
				@if(count($start_ups)>0)
				@foreach($start_ups as $start_up)
					{!! $start_up->get_sart_up_status() !!}
					<tr>
						<td>{{ $start_up->full_business_name }}</td>
						<td><span class="label {{ $start_up->status_class }}">{{ $start_up->status_text }}</span></td>
						<td class="text-right">
						{!! Form::open(['route' => ['start-up-destroy', $start_up->id], 'method' => 'delete', 'onsubmit' => 'return confirm("Are you sure you want to delete this startup?");'] ) !!}
						@if($start_up->can_edit) <a href="{{ route('member-edit-startup-start', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Edit Startup</a> @endif
                     	@if(!$start_up->can_edit) <a href="{{ route('member-startup-questions-list', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Questions</a> @endif
						@if($start_up->can_edit) <button type="submit" class="btn btn-danger btn-xs">Delete</button>@endif
					 	{!! Form::close() !!}
						</td>
					</tr>
				@endforeach
				@else
					<tr>
						<td colspan="4" class="text-center"><h5>You don't currently have any start-ups.</h5></td>
					</tr>				
				@endif
			</tbody>
		</table>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection
