@extends('layouts.members')

@section('content')



<div class="row content-section content-section-title">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">

		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif
      <h1>@if($start_up->invested_total_integer<1000) <span class="label label-success pull-right">Just Launched</span> @endif {{ $start_up->full_business_name }}</h1>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-3 col-lg-2">
      <div class="thumbnail">
			@if(!empty($start_up->logo_file))
				<img src="{!! asset('img/company_photo_holder.png'); !!}" class="content-company-profile-img" style="background-image: url({!! asset('photo_uploads/resized_'.$start_up->logo_file.'.jpg'); !!});">
			@else
				<img src="{!! asset('img/no_photo_startup.jpg'); !!}" class="content-company-profile-img">
			@endif
      </div>
   </div>
   <div class="col-sm-7 col-lg-6 content-sub-page">

      <h2>Company Details</h2>
      <hr>

      <h3>About the Business</h3>
      <p>{{ $start_up->business_summuary }}</p>

      @if($start_up->funding_expire_days!=0)
		<div class="row content-company-profile-financial-row">
			<div class="col-md-3">
				<lead>{{ $start_up->display_investment_target() }}</lead><br>
				<small>TARGET</small>
			</div>
			<div class="col-md-2">
				<lead>{{ $start_up->funding_expire_days }}</lead><br>
				<small>DAYS LEFT</small>
			</div>
			<div class="col-md-3">
				<lead>{{ $start_up->invested_total_string }}</lead><br>
				<small>INVESTED</small>
			</div>
			<div class="col-md-2">
				<lead>{{ $start_up->invested_percentage }}</lead><br>
				<small>FUNDED</small>
			</div>
			<div class="col-md-2">
            	<lead>{{ $start_up->equity_offered }}%</lead><br>
				<small>EQUITY</small>
         	</div>
		</div>

	  @else

      <div class="row content-company-profile-financial-row">
         <div class="col-md-3">
            <lead>{{ $start_up->display_investment_target() }}</lead><br>
            <small>TARGET</small>
         </div>
         <div class="col-md-3">
            <lead>{{ $start_up->invested_total_string }}</lead><br>
            <small>INVESTED</small>
         </div>
         <div class="col-md-3">
            <lead>{{ $start_up->invested_percentage }}</lead><br>
            <small>FUNDED</small>
         </div>
         <div class="col-md-3">
            <lead>{{ $start_up->equity_offered }}%</lead><br>
            <small>EQUITY</small>
         </div>
      </div>

	  @endif

      <hr>
	  @if($start_up->is_investor_followed(Auth::user()->id))
	      <a href="{!! route('member-company-un-follow', ['id' => $start_up->id]) !!}" class="btn btn-warning pull-right">Un-Follow</a>
	  @else
	      <a href="{!! route('member-company-follow', ['id' => $start_up->id]) !!}" class="btn btn-default pull-right">Follow</a>
	  @endif
      <a href="{!! route('member-company-invest', ['id' => $start_up->id]) !!}" class="btn btn-primary">Invest</a>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>
<div class="row content-section"><br></div>

<div class="row content-section content-section-spacer-base">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-3 col-lg-2">

   </div>
   <div class="col-sm-7 col-lg-6 content-sub-page content-sub-page-top-pad">
      <div role="tabpanel">

         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Overview</a></li>
            <li role="presentation"><a href="#team" aria-controls="team" role="tab" data-toggle="tab">Executive Team</a></li>
            <li role="presentation"><a href="#documents" aria-controls="documents" role="tab" data-toggle="tab">Documents</a></li>
            @if(count($videos)>0)<li role="presentation"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Videos</a></li>@endif
         </ul>

         <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="overview">
               <div class="row">
                  <div class="col-sm-6">
                     <h3>Product or Service Details</h3>
                     <p>{{ $start_up->product_details }}</p>
                  </div>
                  <div class="col-sm-6">
                     <h3>Differentiation & Market Impact</h3>
                     <p>{{ $start_up->market_impact }}</p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <h3>Achievements</h3>
                     <p>{{ $start_up->acheivements }}</p>
                  </div>
                  <div class="col-sm-6">
                     <h3>Monetisation Strategy</h3>
                     <p>{{ $start_up->monetisation_strategy }}</p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <h3>Use of funds</h3>
                     <p>{{ $start_up->use_of_funds }}</p>
                  </div>
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="team">
               <br><br>
               <div class="row">
                  @foreach($team_members as $team_member)
                  <div class="col-sm-4">

                     @if($team_member->photo_url)
                    	<img src="{!! asset('img/team_photo_holder.png'); !!}" alt="{{ $team_member->name }}" class="img-thumbnail img-responsive content-company-team-img"  style="background-image: url({!! asset('team_photo_uploads/resized_'.$team_member->photo_url.'.jpg'); !!});"><br><br>
                     @else
                     	<img src="{!! asset('img/no_photo.jpg'); !!}" alt="{{ $team_member->name }}" class="img-thumbnail img-responsive"><br><br>
                     @endif
                     <div class="panel panel-default">
                        <div class="panel-heading"><strong>{{ $team_member->name }}</strong></div>
                        <ul class="list-group">
                           <li class="list-group-item"><strong>Position</strong> {{ $team_member->position }}</li>
                           <li class="list-group-item"><strong>Age</strong> {{ $team_member->age }} years</li>
                           <li class="list-group-item"><strong>Personal Bio</strong><br>{{ $team_member->personal_bio }}</li>
                           <li class="list-group-item"><strong>Involvement</strong><br>{{ $team_member->involvement }}</li>
                        </ul>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="documents">

               <table class="table table-striped">
                  <thead>
                     <tr>
                        <th>Document</th>
                        <th class="col-sm-1">Type</th>
                        <th class="col-sm-1">Size</th>
                        <th class="col-sm-2">Download</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($documents as $document)
                     <tr>
                        <td><strong>{{ $document->title }}</strong><br><small>{{ $document->description }}</small></td>
                        <td>{{ $document->file_type }}</td>
                        <td>{{ round($document->file_size/1000) }}KB</td>
                        <td><a href="{!! asset('uploads/'.$document->file_location); !!}" class="btn btn-default btn-sm" target="_blank"><span class="glyphicon glyphicon-arrow-down"></span> Download Document</a></td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
            @if(count($videos)>0)
            <div role="tabpanel" class="tab-pane" id="videos">
               @foreach($videos as $video)
               <br>
               <div class="embed-responsive embed-responsive-16by9">
                  <iframe  class="embed-responsive-item" src="{{ $video->get_embed_url() }}" frameborder="0" allowfullscreen></iframe>
               </div>
               <br>
               @endforeach
            </div>
            @endif
         </div>

      </div>
   </div>
</div>


<div class="row content-section content-section-spacer-base">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-3 col-lg-2">

   </div>
   <div class="col-sm-7 col-lg-6 content-sub-page content-sub-page-top-pad">
      <h2>Questions & Answers</h2>
      @if(count($questions))
         @foreach($questions as $question)

            <blockquote>
            <h3>{{ $question->question }}</h3>
            <footer>Asked on {{ $question->created_at->toFormattedDateString() }} by {{ $question->getPerson() }}</footer><br>
            <p>{!! $question->format_answer() !!}</p>
          </blockquote>

         @endforeach
      @else
      <h5>No questions asked yet, why not be the first.</h5>
      @endif
      <hr>
      <div class="well well-sm">
         {!! Form::model($start_up, ['route' => ['member-company-ask-question']]) !!}
         <div class="form-group">
            {!! Form::textarea('question', null, ['class' => 'form-control', 'rows' => '4', 'placeholder' => 'Please enter a question in this box.']); !!}
         </div>
         <button class="btn btn-primary" onclick="tempDisableBeforeUnload = true;">Ask Question</button>
         {!! Form::hidden('start_up_id', $start_up->id); !!}
         {!! Form::close(); !!}
      </div>
   </div>
</div>

@endsection
