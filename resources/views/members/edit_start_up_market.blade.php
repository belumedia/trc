@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<?php $active = "market"; ?>
		@include('partials.start_up_navigator')
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">
		
		<h2>Market Research</h2>
		<hr>
			{!! Form::model($start_up, array('route' => array('member-update-startup-market-research', $start_up->id))) !!}
			 
	        <div class="form-group">
	            <label for="market_analysis">Market Analysis</label> 
	            <span id="helpBlock" class="help-block">What is your target market? How will your product(s) or service(s) fit into this market?</span>
	            {!! Form::textarea('market_analysis', null, ['class' => 'form-control', 'rows' => '4']); !!}
				
	        </div>
	        <div class="form-group">
	            <label for="marketing_strategy">Marketing Strategy</label>
	            <span id="helpBlock" class="help-block">How will your business succeed against current or potential future competitors?</span>
	            {!! Form::textarea('marketing_strategy', null, ['class' => 'form-control', 'rows' => '4']); !!}
	        </div>
	        <div class="form-group">
	            <label for="competition_strategy">Competition Strategy</label>
	            <span id="helpBloc" class="help-block">Describe what you have achieved so far with your business.</span>
	            {!! Form::textarea('competition_strategy', null, ['class' => 'form-control', 'rows' => '4']); !!}
	        </div>

			<div class="form-group">
	            <label for="relevant_market_experience">Relevant market experience and insights</label>
	            <span id="helpBloc" class="help-block">What research have you done into your target market? What have you learned from this that will benefit your business?</span>
	            {!! Form::textarea('relevant_market_experience', null, ['class' => 'form-control', 'rows' => '4']); !!}
	        </div>
	        

	        
			<hr>
	        <a href="{{ route('member-your-startups') }}" class="btn btn-default pull-right">Exit</a>
	        <button class="btn btn-primary">Save &amp; Continue</button>

		 </form>
		
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
