@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<?php $active = "documents"; ?>
		@include('partials.start_up_navigator')
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">
		
		
		@if(Session::has('message_error'))
			<br>
			<div class="alert alert-danger" role="alert">{!! Session::get('message_error'); !!}</div>
		@endif

		
		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif
		
		<h2>Documents</h2>
		<p>Please use this section to upload any documentation that supports your application.  Please note that this information will be shared with potential investors.</p>
		
		
		{!! Form::open(['route' => 'member-update-startup-document-save', 'files' => true]) !!}
		<div class="panel panel-default">
		  <div class="panel-body">
		    <div class="form-group">
	            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Document Title']); !!}
	        </div>
	        <div class="form-group">
	            {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Document Description', 'rows' => '3']); !!}
	        </div>
	        <div class="form-group">
		        {!! Form::file('document_file'); !!}
	        </div>
	        <hr>
	        <button class="btn btn-primary btn-sm">Upload Document</button>
		  </div>
		</div>
		{!! Form::hidden('start_up_id', $start_up->id); !!}
		</form>

		<h5>Uploaded Documents</h5>
		<table class="table table-striped">
			<thead>
			<tr>
				<th>Document Name / Description</th>
				<th>View</th>
				<th>Remove</th>
			</tr>
			</thead>
			<tbody>
				@if(count($documents)>0)
				@foreach($documents as $document)
				<tr>
					<td><strong>{{ $document->title }}</strong><br>{{ $document->description }}</th>
					<th class="col-sm-1"><a class="btn btn-xs btn-default" href="{!! asset('uploads/'.$document->file_location); !!}" target="_blank">View</a></th>
					<th class="col-sm-1"><a class="btn btn-xs btn-danger" href="{{ route('member-update-startup-document-remove', ['id' => $document->id, 'start_up_id' => $start_up->id]) }}">Remove</a></th>
				</tr>
				@endforeach
				@else
				<tr>
					<td colspan="3" class="text-center"><h5>No Documents Currently..</h5></td>
				</tr>
				@endif
			</tbody>
		</table>
				        
			<hr>
	        <a href="{{ route('member-your-startups') }}" class="btn btn-default pull-right">Exit</a>
	        <a href="{{ route('member-edit-startup-seis', ['id' => $start_up->id]) }}" class="btn btn-primary">Continue</a>

		 </form>
		
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
