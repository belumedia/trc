@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<?php $active = "seis"; ?>
		@include('partials.start_up_navigator')
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">
		
		
		
		@if($start_up->is_edit_seis_complete())
			@if($start_up->seis_eis_tax_relief)
				<br>
				<div class="panel panel-success">
				<div class="panel-heading"><h3 class="panel-title">SEIS + EIS Accepted</h3></div>
				<div class="panel-body">
					You have specified that you would like to provide SEIS or EIS available to potential investors
				</div>
				<div class="panel-body">
				<a href="{{ route('member-edit-startup-bank', ['id' => $start_up->id]) }}" class="btn btn-primary btn-sm">Next Section</a>
				</div>
				</div>
			
			@else
				<br>
				<div class="panel panel-warning">
				<div class="panel-heading"><h3 class="panel-title">SEIS + EIS Declined</h3></div>
				<div class="panel-body">
					You have specified that you would not like to provide SEIS or EIS available to potential investors
				</div>
				<div class="panel-body">
				<a href="{{ route('member-edit-startup-bank', ['id' => $start_up->id]) }}" class="btn btn-primary btn-sm">Next Section</a>
				</div>
				</div>
			@endif
		@endif	
			
		
		
		
		<h2>SEIS + EIS</h2>
		<p>The UK government provides generous tax reliefs to investors who invest in certain startups. The two that may be relevant to you are the Seed Enterprise Investment Scheme (SEIS) and the Enterprise Investment Scheme (EIS). If you are eligible for one of these reliefs, and you agree to let your investors take advantage of the relief, we will indicate as much on your startup listing, and we will only complete an investment after you have recieved advance assurance of your eligibility from HMRC.</p>
		<hr>
			{!! Form::model($start_up, array('route' => array('member-update-startup-seis-eis'))) !!}
			 
	        <div class="form-group">
	            <label for="seis_eis_tax_relief">If eligible, would you like to make SEIS or EIS tax relief available to potential investors?</label> 
	            <div class="radio">
		            <label class="radio-inline">
					  	{!! Form::radio('seis_eis_tax_relief', '1', null); !!}Yes
					</label>
		            <label class="radio-inline">
					  	{!! Form::radio('seis_eis_tax_relief', '0', null); !!} No
					</label>
	            </div>
	        </div>
			
			<hr>
			
	        <div class="form-group">
	            <label for="eis_shares_compliance">Has your business previously issued shares where an EIS Compliance Statement was made available to investors who subscribed for these shares or has previously received investment from a Venture Capital Trust?</label> 
	            <div class="radio">
		            <label class="radio-inline">
					  	{!! Form::radio('eis_shares_compliance', '1', null); !!}Yes
					</label>
		            <label class="radio-inline">
					  	{!! Form::radio('eis_shares_compliance', '0', null); !!} No
					</label>
	            </div>
	        </div>

			<hr>

			<div class="form-group">
	            <label for="controlled_by_another_company">Is your business controlled by another company?</label> 
	            <div class="radio">
		            <label class="radio-inline">
					  	{!! Form::radio('controlled_by_another_company', '1', null); !!}Yes
					</label>
		            <label class="radio-inline">
					  	{!! Form::radio('controlled_by_another_company', '0', null); !!} No
					</label>
	            </div>
	        </div>
	        
	        <hr>

	        <div class="form-group">
	            <label for="own_shares_in_other_company">Does your business own shares in any other companies?</label> 
	            <div class="radio">
		            <label class="radio-inline">
					  	{!! Form::radio('own_shares_in_other_company', '1', null); !!}Yes
					</label>
		            <label class="radio-inline">
					  	{!! Form::radio('own_shares_in_other_company', '0', null); !!} No
					</label>
	            </div>
	        </div>
	        
	        <hr>

	        <div class="form-group">
	            <label for="partner_in_other_partnership">Is your business a partner in a partnership?</label> 
	            <div class="radio">
		            <label class="radio-inline">
					  	{!! Form::radio('partner_in_other_partnership', '1', null); !!}Yes
					</label>
		            <label class="radio-inline">
					  	{!! Form::radio('partner_in_other_partnership', '0', null); !!} No
					</label>
	            </div>
	        </div>
	        
	        <hr>
	        
	        <div class="form-group">
	            <label for="gross_assets">What is your business' gross assets?</label> 
	            <div class="radio">
		            <label class="radio-inline">
					  	{!! Form::radio('gross_assets', '0', null); !!} <&pound;200k
					</label>
		            <label class="radio-inline">
					  	{!! Form::radio('gross_assets', '1', null); !!} &pound;200k-&pound;15m 
					</label>
					<label class="radio-inline">
					  	{!! Form::radio('gross_assets', '2', null); !!} &pound;15m+
					</label>
	            </div>
	        </div>
	        
	        <hr>
	        
	        <div class="form-group">
	            <label for="total_exmployees">Will your business be engaged in any of the following activities? </label>
	            <ul>
			        <li>Dealing in land, in commodities or futures in shares, securities or other financial instruments</li>
			        <li>Dealing in goods, other than in an ordinary trade of retail or wholesale distribution</li>
			        <li>Financial activities such as banking, insurance, money-lending, debt-factoring, hire-purchase
			            financing or any other financial activities (but excluding the provision of services, such as advice
			            on financial matters)
			        </li>
			        <li>Leasing or letting assets on hire (but excluding certain ship-chartering activities)</li>
			        <li>Receiving royalties or licence fees (but excluding the exploitation of an intangible asset which the
			            company itself has created)
			        </li>
			        <li>Providing legal or accountancy services</li>
			        <li>Property development</li>
			        <li>Farming or market gardening</li>
			        <li>Holding, managing or occupying woodlands, any other forestry activities or timber production</li>
			        <li>Shipbuilding</li>
			        <li>Coal production</li>
			        <li>Steel production</li>
			        <li>Operating or managing hotels or comparable establishments or managing property used as an hotel or
			            comparable establishment
			        </li>
			        <li>Operating or managing nursing homes or residential care homes, or managing property used as a
			            nursing home or residential care home
			        </li>
			        <li>Providing services to another person where that person's trade consists, to a substantial extent, of
			            one of the above activities, and the person controlling that trade also controls the company
			            providing the services
			        </li>
			        <li>The receipt of Feed-in-Tariffs (FITs) or similar government subsidies
			    </ul>
				            
	            
	            <div class="radio">
		            <label class="radio-inline">
					  	{!! Form::radio('business_engagement_confirmation', '1', null); !!} Yes 
					</label>
		            <label class="radio-inline">
					  	{!! Form::radio('business_engagement_confirmation', '0', null); !!} No 
					</label>
	            </div>
	        </div>
	        
			<hr>
	        <a href="{{ route('member-your-startups') }}" class="btn btn-default pull-right">Exit</a>
	        <button class="btn btn-primary">Save &amp; Continue</button>

		 </form>
		
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
