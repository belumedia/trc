@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8"><h1>Browse Startups</h1></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">

		@foreach($start_ups as $start_up)
		<div class="row">
			<div class="col-sm-12">
				@include('partials.invest_display')
			</div>
		</div>
		@endforeach


	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection
