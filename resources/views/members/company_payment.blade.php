@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>{{ $start_up->full_business_name }}</h1>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-3 col-lg-2">
      <div class="thumbnail">
			@if(!empty($start_up->logo_file))
				<img src="{!! asset('img/company_photo_holder.png'); !!}" class="content-company-profile-img" style="background-image: url({!! asset('photo_uploads/resized_'.$start_up->logo_file.'.jpg'); !!});"> 
			@else
				<img src="{!! asset('img/no_photo_startup.jpg'); !!}" class="content-company-profile-img">
			@endif
      </div>
   </div>
   <div class="col-sm-7 col-lg-6 content-full-page">

      @if(Session::has('message'))
      <br>
      <div class="alert alert-danger" role="alert">{!! Session::get('message'); !!}</div>
      @endif

      

      <h2>Make Payment</h2>
      <hr>

      <p>Payment for your investment won't be taken at this time, we will only put a hold on the amount until the investment goes through.<br>
         If the investment fails to complete the hold on the amount will be cancelled.</p>
      <table class="table">
         <thead>
            <tr>
               <th>Item</th>
               <th>Price</th>
            </tr>
         </thead>
         <tbody>
            <tr>
               <td>Investment in {{ $start_up->full_business_name }}</td>
               <td>&pound; {{ $investment->amount }}</td>
            </tr>
            <tr>
               <th class="text-right">Total Amount Payable</th>
               <td>&pound; {{ $investment->amount }}</td>
            </tr>
         </tbody>
      </table>	



      <div class="panel panel-default">
         <div class="panel-body">
                 
                 @if(Auth::user()->is_sipp_acc)

			  	<strong>Investment Accepted</strong><br>
			  	Your request to invest in this business has been registered and will be confirmed by your SIPP provider.
			  	<hr>
			  	<a href="{{ route('member-your-investments') }}" class="btn btn-primary">View Investments</a>
				                 
                 @else 
            

                 {!! Form::open(['route' => 'member-company-transaction-capture-customer', 'id' => 'payment-form', 'class' => 'form-horizontal']) !!}
                  
                 <div class="alert alert-danger payment-errors"></div>
                 
                   <div class="form-group">
                     <label class="col-sm-3 control-label" for="card-number">Card Number</label>
                     <div class="col-sm-9">
                       <input type="text" class="form-control" placeholder="Debit/Credit Card Number" data-stripe="number">
                     </div>
                   </div>
                   <div class="form-group">
                     <label class="col-sm-3 control-label" for="expiry-month">Expiration Date</label>
                     <div class="col-sm-9">
                       <div class="row">
                         <div class="col-xs-3">
                           <select class="form-control col-sm-2" data-stripe="exp-month">
                             <option value="01">Jan (01)</option>
                             <option value="02">Feb (02)</option>
                             <option value="03">Mar (03)</option>
                             <option value="04">Apr (04)</option>
                             <option value="05">May (05)</option>
                             <option value="06">June (06)</option>
                             <option value="07">July (07)</option>
                             <option value="08">Aug (08)</option>
                             <option value="09">Sep (09)</option>
                             <option value="10">Oct (10)</option>
                             <option value="11">Nov (11)</option>
                             <option value="12">Dec (12)</option>
                           </select>
                         </div>
                         <div class="col-xs-3">
                           <select class="form-control" data-stripe="exp-year">
                             @for($i=date('Y'); $i<date('Y')+20; $i++)
                              <option value="{{ $i }}">{{ $i }}</option>
                             @endfor
                           </select>
                         </div>
                       </div>
                     </div>
                   </div>
                   <div class="form-group">
                     <label class="col-sm-3 control-label" for="cvv">Card CVV</label>
                     <div class="col-sm-3">
                       <input type="text" class="form-control" placeholder="Security Code" data-stripe="cvc">
                     </div>
                   </div>
                   <div class="form-group">
                     <div class="col-sm-offset-3 col-sm-9">
                       <button type="submit" class="btn btn-primary">Submit Investment</button>
                     </div>
                   </div>
         
              {!! Form::close() !!}
          
		    @endif
            
            
     
            
         </div>
      </div>
     
   


   </div>

</div>
@endsection

@section('footer-scripts')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
Stripe.setPublishableKey('{{ config('stripe.stripe.public') }}');
$('#payment-form').find('.payment-errors').hide();

jQuery(function ($) {
   $('#payment-form').submit(function (event) {
      var $form = $(this);
      $form.find('button').prop('disabled', true);
      Stripe.card.createToken($form, stripeResponseHandler);
      return false;
   });
});

function stripeResponseHandler(status, response) {
   var $form = $('#payment-form');

   if (response.error) {
      // Show the errors on the form
      $form.find('.payment-errors').text(response.error.message);
      $('#payment-form').find('.payment-errors').show('fast');
      
      $form.find('button').prop('disabled', false);
   } else {
      // response contains id and card, which contains additional card details
      var token = response.id;
      // Insert the token into the form so it gets submitted to the server
      $form.append($('<input type="hidden" name="stripeToken" />').val(token));
      // and submit
      $form.get(0).submit();
   }

}
;

</script>
@endsection