@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h1>Welcome {{ Auth::user()->first_name }}</h1></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@if(count($followed_start_ups)>0)

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h2>Startups you're following</h2></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">

		@foreach($followed_start_ups as $start_up)
		<div class="row">
			<div class="col-sm-12">
				@include('partials.invest_display')
			</div>
		</div>
		@endforeach


	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endif

@if(count($top_picks)>0)

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h2>Our Top Investment Picks</h2></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">

		@foreach($top_picks as $start_up)
		<div class="row">
			<div class="col-sm-12">
				@include('partials.invest_display')
			</div>
		</div>
		@endforeach


	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endif

@if(Session::get('email_marketing_show_tracking'))
<!-- <iframe src="http://nmctrk.co.uk/p.ashx?o=59&t=TRANSACTION_ID" height="1" width="1" frameborder="0"></iframe> -->
<iframe src="http://nmctrk.co.uk/p.ashx?o=59&e=45&t=TRANSACTION_ID" height="1" width="1" frameborder="0"></iframe>
@endif

@endsection
