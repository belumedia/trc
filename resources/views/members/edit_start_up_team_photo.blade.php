@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<?php $active = "team"; ?>
		@include('partials.start_up_navigator')
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">
		
		
		@if(Session::has('error_message'))
			<br>
			<div class="alert alert-danger" role="alert">{!! Session::get('error_message'); !!}</div>
		@endif

		
		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif
		
		<h2>Photo For {{ $team_member->name }}</h2>
		<p>Please use this page to upload a photo, make sure that your image is bigger than 400px x 250px.</p>
		<p>Also make sure that the file size of your image is no bigger than 2mb.</p>
	
		
		{!! Form::open(['route' => 'member-update-startup-team-photo-upload', 'files' => true]) !!}
		<div class="panel panel-default">
		  <div class="panel-body">
		    <div class="form-group">
	            {!! Form::file('photo_file', null, ['class' => 'form-control']); !!}
	        </div>
	        <button class="btn btn-primary btn-sm">Upload Photo</button>
		  </div>
		</div>
		</form>

		<h5>Current Photo</h5>

		@if(!empty($team_member->photo_url))
			<img src="{{ asset('team_photo_uploads/resized_'.$team_member->photo_url.'.jpg') }}" border="0">
		@else
			<h3>No photo currently...</h3>
			<p>Use the form above to upload a new photo.</p>
		@endif
					        
		<hr>
        <a href="{{ route('member-edit-startup-team', ['id' => $start_up->id]) }}" class="btn btn-primary">Back</a>

		 </form>
		
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
