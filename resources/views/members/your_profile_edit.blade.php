@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>Change Contact Details</h1>
      <p>Please use the form below to make changes to your contact details.</p>
   </div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
			
				{!! Form::model($user, ['route' => 'member-your-proflie-update', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
				
				<h4>Your Name</h4>
				
				<div class='form-group'>
					<label for='first_name' class='col-sm-2 control-label'>First Name</label>
					<div class="col-sm-10">
					{!! Form::text('first_name', null, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class='form-group'>
					<label for='last_name' class='col-sm-2 control-label'>Last Name</label>
					<div class="col-sm-10">
					{!! Form::text('last_name', null, ['class' => 'form-control']) !!}
					</div>
				</div>
				
				<h4>Contact Details</h4>

				<div class='form-group'>
					<label for='telephone' class='col-sm-2 control-label'>Telephone</label>
					<div class="col-sm-10">
						{!! Form::text('telephone', null, ['class' => 'form-control']) !!}
					</div>
				</div>
				
				<!--
				<div class='form-group'>
					<label for='email' class='col-sm-2 control-label'>Email</label>
					<div class="col-sm-10">
						{!! Form::text('email', null, ['class' => 'form-control']) !!}
						<span id="helpBlock" class="help-block"><strong>Remember:</strong> Your email address is used to sign-in to The Right Crowd.</span>
					</div>
				</div>
				-->
				
				<h4>Your Address</h4>

				<div class='form-group'>
					<label for='street' class='col-sm-2 control-label'>Street</label>
					<div class="col-sm-10">
						{!! Form::text('street', null, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class='form-group'>
					<label for='street_2' class='col-sm-2 control-label'></label>
					<div class="col-sm-10">
						{!! Form::text('street_2', null, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class='form-group'>
					<label for='city' class='col-sm-2 control-label'>City</label>
					<div class="col-sm-10">
						{!! Form::text('city', null, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class='form-group'>
					<label for='county' class='col-sm-2 control-label'>County</label>
					<div class="col-sm-10">
						{!! Form::text('county', null, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class='form-group'>
					<label for='postcode' class='col-sm-2 control-label'>Postcode</label>
					<div class="col-sm-10">
						{!! Form::text('postcode', null, ['class' => 'form-control']) !!}
					</div>
				</div>
				
				<hr>
			
				<div class='form-group'>
					<label for='postcode' class='col-sm-2 control-label'></label>
					<div class="col-sm-10">
						<input type='submit' value='Update Contact Details' class='btn btn-primary'>
					</div>
				</div>
				
				{!! Form::close() !!}


	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">


	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection
