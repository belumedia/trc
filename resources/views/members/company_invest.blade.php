@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>{{ $start_up->full_business_name }}</h1>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-3 col-lg-2">
      <div class="thumbnail">
			@if(!empty($start_up->logo_file))
				<img src="{!! asset('img/company_photo_holder.png'); !!}" class="content-company-profile-img" style="background-image: url({!! asset('photo_uploads/resized_'.$start_up->logo_file.'.jpg'); !!});">
			@else
				<img src="{!! asset('img/no_photo_startup.jpg'); !!}" class="content-company-profile-img">
			@endif
      </div>
   </div>
   <div class="col-sm-7 col-lg-6 content-full-page">

      @if(Session::has('message'))
			<br>
			<div class="alert alert-danger" role="alert">{!! Session::get('message'); !!}</div>
		@endif

      {!! Form::open(['route' => 'member-company-post-investment']) !!}

      <h2>Investment application in {{ $start_up->full_business_name }}</h2>
      <hr>

      <h3>How much would you like to invest?</h3>

      <p>Please enter the amount you would like to invest in this business.</p>

      <div class="form-group">
         <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-default" onclick="document.getElementById('investment_amount').value = '10';">£10</button>
            <button type="button" class="btn btn-default" onclick="document.getElementById('investment_amount').value = '50';">£50</button>
            <button type="button" class="btn btn-default" onclick="document.getElementById('investment_amount').value = '100';">£100</button>
            <button type="button" class="btn btn-default" onclick="document.getElementById('investment_amount').value = '250';">£250</button>
            <button type="button" class="btn btn-default" onclick="document.getElementById('investment_amount').value = '500';">£500</button>
            <button type="button" class="btn btn-default" onclick="document.getElementById('investment_amount').value = '1000';">£1000</button>
            <button type="button" class="btn btn-default" onclick="document.getElementById('investment_amount').value = '5000';">£5000</button>
         </div>
      </div>
      <div class="form-group">
         {!! Form::text('investment_amount', null, ['class' => 'form-control', 'placeholder' => 'or enter other amount here', 'id' => 'investment_amount']) !!}
      </div>
      <hr>

      <h3>Investment Terms</h3>
      <p>Please make sure that you have read our summary of the risks of making an investment on The Right Crowd, The Right Crowd's Terms and Conditions for Investors and the Offer Rules.</p>


      <div role="tabpanel">

         <!-- Nav tabs -->
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#riskterms" aria-controls="riskterms" role="tab" data-toggle="tab">Risks of Investing in Shares</a></li>
            <li role="presentation"><a href="#investorterms" aria-controls="investorterms" role="tab" data-toggle="tab">Investor Terms &amp; Conditions</a></li>
            <li role="presentation"><a href="#shareoffer" aria-controls="shareoffer" role="tab" data-toggle="tab">Offer Rules</a></li>

         </ul>

         <!-- Tab panes -->
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="riskterms">
               <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" scrolling="yes" src="{!! route('legal-investment-risk') !!}"></iframe>
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="investorterms">
               <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" scrolling="yes" src="{!! route('legal-investor-full-terms') !!}"></iframe>
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="shareoffer">
               <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" scrolling="yes" src="{!! route('legal-share-offer-terms') !!}"></iframe>
               </div>
            </div>
         </div>

      </div>
      <br>
      <br>

      I confirm that I have read, understood and accepted:
      <ul>
	      <label><input type="checkbox" name="understand_risks" value="check"> The risks of making an investment on The Right Crowd.</label>
	      <label><input type="checkbox" name="accept_terms" value="check"> The Right Crowd's Terms and Conditions for Investors and the Offer Rules</label>
      </ul>

      <p>I confirm that by clicking "invest" button below, I am making an application for shares in the Investee Company named at the top of this page for the amount which I have set out above, which if accepted will form a legally binding agreement between myself and the Investee Company.</p>


      <hr>
      <button class="btn btn-primary">Invest</button>
      <br><br>
      <!-- {!! Form::hidden('understand_risks', '1') !!}
      {!! Form::hidden('accept_terms', '1') !!} -->
      {!! Form::hidden('start_up_id', $start_up->id) !!}
      {!! Form::close() !!}
   </div>
   <br><br>
</div>


@endsection
