@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
	   @if(Session::has('message'))
			<br>
			<div class="alert alert-info" role="alert">{!! Session::get('message'); !!}</div>
		@endif
	   
      <h1>Your Profile</h1>
   </div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<h2>Contact Information</h2>
				<h3>Email Address</h3>
            <p>{{ $user->email }}</p>
            
				<h3>Telephone</h3>
            <p>{{ $user->telephone }}</p>
            
				<h3>Address</h3>
				<p>{{ $user->street }}<br>
					@if($user->street_2) {{ $user->street_2 }}<br> @endif
					@if($user->city) {{ $user->city }}<br> @endif
               {{ $user->county }}<br>
               {{ $user->postcode }}<br>
            </p>
			</div>
			<div class="col-sm-4">
			<h2>Investment Settings</h2>
			<h3>Type of Investor</h3>
			<?php $user->get_user_investment_status(); ?>
				
			<p>You are set as a <strong>{{ $user->investor_type_text }}</strong></p>
            @if($user->investor_terms_specific) <p><span class="label label-success"><span class="glyphicon glyphicon-ok"></span>&nbsp;</span>&nbsp; <strong>Agreed</strong>, to the investor terms.</p> @endif
            @if($user->investor_terms_general) <p><span class="label label-success"><span class="glyphicon glyphicon-ok"></span>&nbsp;</span>&nbsp; <strong>Agreed</strong>, to general investor terms.</p> @endif
            
            <hr>
            <h2>Startup Settings</h2>
            <h3>Stripe Connected</h3>
			@if($user->stripe_connected)
               <p><span class="label label-success"><span class="glyphicon glyphicon-ok"></span>&nbsp;</span>&nbsp; <strong>Yes</strong>, your stripe account is connected.</p>
            @else
               <p><span class="label label-danger"><span class="glyphicon glyphicon-check"></span>&nbsp;</span>&nbsp; <strong>No</strong>, your stripe account is not connected.</p>     
            @endif
      
            
			</div>
			<div class="col-sm-4">
				<h2>Account Options</h2>
            <h3></h3>
				<a href="{{ route('member-your-proflie-edit') }}" class="btn btn-primary btn-block">Change Contact Details</a><br>
				<a href="{{ route('member-your-proflie-password-change') }}" class="btn btn-default btn-block">Change Password</a><br>
            @if($user->stripe_connected)
               <hr>
               <a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id={{ Config::get('stripe.stripe.client_id') }}&scope=read_write&state=1234&redirect_uri={{ route('member-transaction-connect') }}" class="btn btn-default btn-block">Reconnect to Stripe</a><br>
            @else
               <a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id={{ Config::get('stripe.stripe.client_id') }}&scope=read_write&state=1234&redirect_uri={{ route('member-transaction-connect') }}" class="btn btn-default btn-block">Connect to Stripe</a><br>
            @endif
			</div>
		</div>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">


	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection
