@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<?php $active = "company"; ?>
		@include('partials.start_up_navigator')

	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">

		<h2>Company Details</h2>
		<hr>
			{!! Form::model($start_up, array('route' => array('member-update-startup-company', $start_up->id))) !!}

			<h4>About the Business</h4>
	        <div class="form-group">
	            <label for="full_business_name">Full Business Name</label>
	            {!! Form::text('full_business_name', null, ['class' => 'form-control']); !!}
				<span id="helpBlock" class="help-block">Full Business Name including limited company status</span>
	        </div>
	        <div class="form-group">
	            <label for="brand_name">Brand Name</label>
	            {!! Form::text('brand_name', null, ['class' => 'form-control']); !!}
	            <span id="helpBlock" class="help-block">In your business trades under a brand name, please enter it above.</span>
	        </div>
	        <div class="form-group">
	            <label for="business_summuary">Business Summary</label>
	            {!! Form::textarea('business_summuary', null, ['class' => 'form-control']); !!}
	            <span id="helpBlock" class="help-block">Maximum 200 words or less.</span>
	        </div>
	        <div class="form-group">
	            <label for="company_number">Company Registration Number</label>
	            {!! Form::text('company_number', null, ['class' => 'form-control']); !!}
	        </div>
	        <div class="form-group">
	            <label for="incorporation_date">Date of Incorporation</label>


	            <div class='row'>
	               <div class='col-sm-3'>
	                  {!! Form::select('incorporation_date_day', FormHelper::options_date_day(), $incorporation_date_day, ['class' => 'form-control']) !!}
	               </div>
	               <div class='col-sm-5'>
	                  {!! Form::select('incorporation_date_month', FormHelper::options_date_month(), $incorporation_date_month, ['class' => 'form-control']) !!}
	               </div>
	               <div class='col-sm-4'>
	                  {!! Form::select('incorporation_date_year', FormHelper::options_date_year(), $incorporation_date_year, ['class' => 'form-control']) !!}
	               </div>

            </div>

	        </div>
	        <div class="form-group">
	            <label for="business_address">Business Address</label>
	            {!! Form::textarea('business_address', null, ['class' => 'form-control', 'rows' => '4']); !!}
	        </div>
	        <div class="form-group">
	            <label for="business_postcode">Business Postcode</label>
	            {!! Form::text('business_postcode', null, ['class' => 'form-control']); !!}
	        </div>

	        <h4>Investment Requirements</h4>

	        <!-- <div class="form-group">
	            <label for="investment_total">Minimum Investment</label>
	            {!! Form::text('investment_min', null, ['class' => 'form-control', 'placeholder' => '80,000']); !!}
	            <span id="helpBlock" class="help-block">This is the minimum investment you will accept</span>
	        </div> -->

	        <div class="form-group">
	            <label for="investment_total">Investment Target</label>
	            {!! Form::text('investment_total', null, ['class' => 'form-control', 'placeholder' => '100,000']); !!}
	            <span id="helpBlock" class="help-block">Investment sought in &pound; (GBP) and displayed as full sum</span>
	        </div>
	        <div class="form-group">
	            <label for="equity_offered">Equity offered</label>
	            {!! Form::text('equity_offered', null, ['class' => 'form-control', 'placeholder' => '25%']); !!}
	            <span id="helpBlock" class="help-block">Equity offered as a percentage of total share</span>
	        </div>
	        <div class="form-group">
	            <label for="investment_total">Achieve investment in</label>
	            {!! Form::select('investment_days', [
		            '30' => '30 Days',
		            '60' => '60 Days',
		            '90' => '90 Days',
	            ], null, ['class' => 'form-control']); !!}
	            <span id="helpBlock" class="help-block">The amount of time in days you would like to achieve your funding goal.</span>
	        </div>
	        <div class="form-group">
	            <label for="investment_summuary">Investment Summary</label>
	            {!! Form::textarea('investment_summuary', null, ['class' => 'form-control']); !!}
	        </div>

	        <h4>Business Sectors</h4>
	        <div class="form-group">
	            <label for="sector_id">Primary Sector</label>
	            {!! Form::select('sector_1_id', $sectors, null, ['class' => 'form-control']); !!}
	            <span id="helpBlock" class="help-block">Your main business sector</span>
	        </div>
	        <div class="form-group">
	            <label for="sector_2_id">Secondary Sector</label>
	            {!! Form::select('sector_2_id', $sectors, null, ['class' => 'form-control']); !!}
	            <span id="helpBlock" class="help-block">Your second most important business sector</span>
	        </div>
	        <div class="form-group">
	            <label for="sector_3_id">Tertiary Sector</label>
	            {!! Form::select('sector_3_id', $sectors, null, ['class' => 'form-control']); !!}
	            <span id="helpBlock" class="help-block">Your third most important business sector</span>
	        </div>

			<hr>
	        <a href="{{ route('member-your-startups') }}" class="btn btn-default pull-right">Exit</a>
	        <button class="btn btn-primary" onclick="tempDisableBeforeUnload = true;">Save &amp; Continue</button>

		 </form>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>



@endsection
