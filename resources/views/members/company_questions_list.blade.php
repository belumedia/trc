@extends('layouts.members')

@section('content')

<div class="row content-section content-section-title">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>Questions for {{ $start_up->full_business_name }}</h1>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-4 col-lg-2">
      <div class="thumbnail">
			@if(!empty($start_up->logo_file))
				<img src="{!! asset('img/company_photo_holder.png'); !!}" class="content-company-profile-img" style="background-image: url({!! asset('photo_uploads/resized_'.$start_up->logo_file.'.jpg'); !!});"> 
			@else
				<img src="{!! asset('img/no_photo_startup.jpg'); !!}" class="content-company-profile-img">
			@endif
      </div>
   </div>
   <div class="col-sm-8 col-lg-6 content-sub-page content-sub-page-top-pad">
      <h2>Questions & Answers</h2>
      @if(count($questions))
         @foreach($questions as $question)

            <blockquote>
            <h3>{{ $question->question }}</h3>
            <footer>Asked on {{ $question->created_at->toFormattedDateString() }} by {{ $question->getPerson() }}</footer><br>
            <p>{!! $question->format_answer() !!}</p>
          	<a href="{{ route('member-startup-questions-answer', ['id' => $question->id]) }}" class="btn btn-primary btn-sm">Answer Question</a>
          	</blockquote>
		  	
         @endforeach     
      @else
      <h5>No questions asked yet, will let you know when you get asked something.</h5>
      @endif
      <hr>
      <a href="{{ route('member-your-startups') }}" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
   </div>
</div>

@endsection
