<h1>The Right Crowd Limited</h1>

<h1>Website Terms of Use</h1>

<p>PLEASE READ THESE TERMS OF USE CAREFULLY BEFORE USING THIS SITE.</p>
<p>BY USING THIS SITE YOU CONFIRM THAT YOU ACCEPT THESE TERMS OF USE AND THAT YOU AGREE TO COMPLY WITH THEM.  IF YOU DO NOT AGREE TO THESE TERMS OF USE, STOP USING THIS SITE IMMEDIATELY. </p>

<h2>1. These Terms</h2>
<p>These terms of use (together with the documents referred to in them) tell you the terms of use on which you may make use of our website accessed via http://www.therightcrowd.com (“our site”), whether as a guest or a registered user. Use of our site includes accessing, browsing, or registering to use our site.</p>
<p>Please read these terms of use carefully before you start to use our site, as these will apply to your use of our site. </p>
<p>We recommend that you print a copy of these terms for future reference.</p>
<p>By using our site, you confirm that you accept these terms of use and that you agree to comply with them.  If you do not agree to these terms of use, you must not use our site.</p>

<h2>2. Our Privacy Policy</h2>
<p>These terms of use refer to our privacy policy (which includes our cookies policy), which also applies to your use of our site.</p>

<h2>3. Information About Us</h2>
<p>3.1 The website accessed via http://www.therightcrowd.com is a site operated by The Right Crowd Limited ("we", “us”).  We are registered in England and Wales under company number 09398506 and have our registered office at 39 Steeple Close, Poole, Dorset BH17 9BJ.</p>
<p>3.2 The Right Crowd Limited is an appointed representative of Equity For Growth Ltd which is authorised and regulated by the Financial Conduct Authority with registration number 475953. </p>

<h2>4. Changes To These Terms</h2>
<p>We may revise these terms of use at any time by amending this page.  Please check this page from time to time to take notice of any changes we made, as they are binding on you.</p>

<h2>5. Changes To Our Site</h2>
<p>We may update our site from time to time, and may change the content at any time. However, please note that any of the content on our site may be out of date at any given time, and we are under no obligation to update it.  We do not guarantee that our site, or any content on it, will be free from errors or omissions.</p>

<h2>6. Accessing Our Site</h2>
<p>6.1 Our site is made available free of charge. </p>
<p>6.2 We do not guarantee that our site, or any content on it, will always be available or be uninterrupted. Access to our site is permitted on a temporary basis. We may suspend, withdraw, discontinue or change all or any part of our site without notice. We will not be liable to you if for any reason our site is unavailable at any time or for any period.</p>
<p>6.3 You are responsible for making all arrangements necessary for you to have access to our site. </p>
<p>6.4 You are also responsible for ensuring that all persons who access our site through your internet connection are aware of these terms of use and other applicable terms and conditions, and that they comply with them. </p>

<h2>7. Your Account And Password</h2>
<p>7.1 If you choose, or you are provided with, a user identification code, password or any other piece of information as part of our security procedures, you must treat such information as confidential. You must not disclose it to any third party. </p>
<p>7.2 We have the right to disable any user identification code or password, whether chosen by you or allocated by us, at any time, if in our reasonable opinion you have failed to comply with any of the provisions of these terms of use.</p>
<p>7.3 If you know or suspect that anyone other than you knows your user identification code or password, you must promptly notify us at admin@therightcrowd.com.</p>

<h2>8. Intellectual property rights</h2>
<p>8.1 We are the owner or the licensee of all intellectual property rights in our site, and in the material published on it.  Those works are protected by copyright laws and treaties around the world.  All such rights are reserved. </p>
<p>8.2 You may print off one copy, and may download extracts, of any page(s) from our site for your personal use and you may draw the attention of others within your organisation to content posted on our site. </p>
<p>8.3 You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text. </p>
<p>8.4 Our status (and that of any identified contributors) as the authors of content on our site must always be acknowledged. </p>
<p>8.5 You must not use any part of the content on our site for commercial purposes without obtaining a licence to do so from us or our licensors.</p>
<p>8.6 If you print off, copy or download any part of our site in breach of these terms of use, your right to use our site will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</p>

<h2>9. No Reliance On Information</h2>
<p>9.1 The content on our site is provided for general information only. It is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on our site.</p>
<p>9.1 Although we make reasonable efforts to update the information on our site, we make no representations, warranties or guarantees, whether express or implied, that the content on our site is accurate, complete or up-to-date. </p>

<h2>10. Limitation of our liability</h2>
<p>10.1 Nothing in these terms of use excludes or limits our liability for death or personal injury arising from our negligence, or our fraud or fraudulent misrepresentation, or any other liability that cannot be excluded or limited by statute or common law in England.</p>
<p>10.2 To the extent permitted by law, we exclude all conditions, warranties, representations or other terms which may apply to our site or any content on it, whether express or implied. </p>
<p>10.3 We will not be liable to any user for any direct or indirect loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with your use of, or inability to use, our site; or your use of or reliance on any content displayed on our site. </p>
<p>10.4 We will not be liable for any loss or damage caused by a virus, distributed denial-of-service attack, or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of our site or to your downloading of any content on it, or on any website linked to it.</p>
<p>10.5 We assume no responsibility for the content of websites linked on our site. Such links should not be interpreted as endorsement by us of those linked websites. We will not be liable for any loss or damage that may arise from your use of them.</p>

<h2>11. Acceptable use of our site</h2>
<p>11.1 You may use our site only for lawful purposes.  You may not use our site:
	<ol type="A">
		<li>in any way that breaches any applicable local, national or international law or regulation; </li>
		<li>in any way that is unlawful or fraudulent, or has any unlawful or fraudulent purpose or effect; </li>
		<li>for the purpose of harming or attempting to harm minors in any way; </li>
		<li>to transmit, or procure the sending of, any unsolicited or unauthorised advertising or promotional material or any other form of similar solicitation (spam); and</li>
		<li>to knowingly transmit any data, send or upload any material that contains viruses, Trojan horses, worms, time-bombs, keystroke loggers, spyware, adware or any other harmful programs or similar computer code designed to adversely affect the operation of any computer software or hardware.</li>
	</ol>
</p>

<p>11.2 You also agree:
	<ol type="A">
		<li>not to reproduce, duplicate, copy or re-sell any part of our site in contravention of these terms of use; </li>
		<li>not to access without authority, interfere with, damage or disrupt:</li>
		<li>any part of our site;</li>
		<li>any equipment or network on which our site is stored; </li>
		<li>any software used in the provision of our site; or </li>
		<li>any equipment or network or software owned or used by any third party.</li>
	</ol>
</p>

<p>11.3 Any content you upload to our site will be considered non-confidential and non-proprietary (unless otherwise agreed with us), and we have the right to use, copy, distribute and disclose to third parties any such content for any purpose. </p>
<p>11.4 We also have the right to disclose your identity to any third party who is claiming that any content posted or uploaded by you to our site constitutes a violation of their intellectual property rights, or of their right to privacy.</p>
<p>11.5 We will not be responsible, or liable to any third party, for the content or accuracy of any content posted by you or any other user of our site.</p>
<p>11.6 We have the right to remove any posting you make on our site.</p>
<p>11.7 The views expressed by other users on our site do not represent our views or values.</p>

<h2>12 Viruses</h2>
<p>12.1 We do not guarantee that our site will be secure or free from bugs or viruses.</p>
<p>12.2 You are responsible for configuring your information technology, computer programmes and platform in order to access our site. You should use your own virus protection software.</p>
<p>12.3 You must not misuse our site by knowingly introducing viruses, trojans, worms, logic bombs or other material which is malicious or technologically harmful. You must not attempt to gain unauthorised access to our site, the server on which our site is stored or any server, computer or database connected to our site. You must not attack our site via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our site will cease immediately.</p>

<h2>13 Linking To Our Site</h2>
<p>13.1 You may link to our home page, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it.  You must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists. </p>
<p>13.2 You must not establish a link to our site in any website that is not owned by you. </p>
<p>13.3 Our site must not be framed on any other site, nor may you create a link to any part of our site other than the home page. </p>
<p>13.4 We reserve the right to withdraw linking permission without notice. </p>
<p>13.5 If you wish to make any use of content on our site other than that set out above, please contact admin@therightcrowd.com.</p>

<h2>14 Your Failure to Comply With These Terms</h2>
<p>14.1 Failure to comply with these terms of use constitutes a breach and may result in our taking all or any of the following actions:
	<ol type="A">
		<li>immediate, temporary or permanent withdrawal of your right to use our site;</li>
		<li>immediate, temporary or permanent removal of any posting or material uploaded by you to our site;</li>
		<li>issue of a warning to you;</li>
		<li>legal proceedings against you for reimbursement of all costs on an indemnity basis (including, but not limited to, reasonable administrative and legal costs) resulting from the breach; </li>
		<li>further legal action against you; and/or</li>
		<li>disclosure of such information to law enforcement authorities as we reasonably feel is necessary.</li>
	</ol>
<p>14.2 We exclude liability for actions taken in response to breaches of these terms.  The responses described in this policy are not limited, and we may take any other action we reasonably deem appropriate.</p>

<h2>15 Applicable law</h2>
<p>Please note that these terms of use, the subject matter and their formation, are governed by the laws of England. You and we both agree that the courts of England and Wales will have exclusive jurisdiction. </p>

<h2>16 Contact us</h2>
<p>To contact us, please email admin@therightcrowd.com.</p>
