<h1>SUBSCRIPTION LETTER</h1>
<h2>THE RIGHT CROWD LIMITED ("COMPANY")</h2>
<h2>APPLICATION TO APPLY FOR DEFERRED SHARES IN THE COMPANY</h2>

<ol>
	<li>I agree and accept that as a condition of becoming an investor member of The Right Crowd crowdfunding platform ("Platform"), I am required to become a shareholder of the Company.</li>
	<li>I hereby apply for the allotment to me of one (1) deferred share of &pound;0.01 each in the capital of the Company ("Share") for cash at a price of &pound;0.01 per share subject to the Company's articles of association.</li>
	<li>I acknowledge that I have received and read a copy of the Company's articles of association, a copy of which can be found http://www.therightcrowd.com/terms</li>
	<li>The total subscription price for the Share is &pound;0.01 ("Subscription Price")</li>
	<li>I agree and accept that subject to the board of directors approving this application, I will be allotted the Share as unpaid and I will be legally obliged to pay the Subscription Price to the Company.</li>
	<li>I request and authorise that upon my first Successful Investment (as defined below), you instruct your third party payment provider to deduct the Subscription Price (and transfer the same to the Company in satisfaction of the Subscription Price) from my Total Bid Amount before such monies will be used to apply for shares in relevant investee company.</li>
	<li>"<strong>Successful Investment</strong>" means where:
		<ol type="A">
			<li>I have bid to invest in an investee company on the Platform;</li>
			<li>the investee company has received sufficient bids to meet its minimum investment target; and</li>
			<li>I have received a notification from the investee company that it has had a successful fundraise and that my bid was accepted.</li>
		</ol>
	</li>
	<li>Total Bid Amount means the total amount of monies which I have agreed to invest in an investee company in connection with a Successful Investment and which I have transferred to the Company’s third party payment provider pursuant to the instructions on the Platform.</li>
	<li>I agree and acknowledge that:
		<ol type="A">
			<li>if the Subscription Price is not deducted from a Successful Investment I will remain liable to pay the Subscription Price to the Company; and</li>
			<li>in the event that the Company calls on me to pay the Subscription Price, I will do so as soon as practicably possible.</li>
		</ol>
	</li>
	<li>Conditionally upon allotment to me of the Share, I request and authorise you to enter my name in the Company's register of members as holder of the Shares allotted to me and to send to me a share certificate pursuant to this application.</li>
	<li>By ticking the "[Accept]" box below, I confirm that I have read, understood and accept the terms of this application and that I understand that on acceptance of my application by the Company this subscription letter shall form a legally binding contract between the Company and myself . </li>
</ol>
		