@extends('layouts.legal')

@section('content')

<h3 class="text-center">The Right Crowd Limited </h3>
<h3 class="text-muted text-center">Standard Terms and Conditions (Investor)</h3>
<br><br>
<p>
   These terms and conditions (the <strong>"Terms"</strong>) set out the basis on which you will be
   permitted to use the site accessed via <a href="http://www.therightcrowd.com/">http://www.therightcrowd.com/</a> (the <strong>"Site"</strong>) to
   access information about potential investment opportunities and invest funds in the
   companies registered on the Site (<strong>"Investee Companies"</strong>) in exchange for an equity
   stake in such Investee Companies.
</p>

<p>
   The Site is operated by The Right Crowd Limited of 39 Steeple Close, Poole, Dorset
   BH17 9BJ (<strong>"The Right Crowd"</strong>, <strong>"us"</strong> or <strong>"we"</strong>).
</p>

<p>
   The Right Crowd Limited is an appointed representative of Equity For Growth Ltd which is
   authorised and regulated by the Financial Conduct Authority with registration number
   475953.
</p>

<p>
   References in these Terms to “you” and the “Investor Member” are references to
   you as the individual who has ticked the checkbox indicating your acceptance of these Terms and clicked the acceptance button on the Site.
</p>

<p>
   <strong>
      By accepting these Terms (by clicking the acceptance button on the Site) you
      agree to bound by these Terms and all documents referred to herein. If you do
      not agree to these Terms and such documents referred to herein, you must not
      accept these Terms and you will not be permitted to invest any monies in
      Investee Companies through the Site.
   </strong>
</p>

<ol>
   <li><strong>INVESTOR MEMBERS</strong></li>

   <ol>
      <li>
         By ticking the checkbox indicating your acceptance of these Terms and clicking the acceptance button on the Site, you confirm that you wish to be an “Investor Member” on the Site. By accepting these Terms you warrant that you have accepted
         and continue to comply with the Member Terms and Conditions and our
         Privacy Policy which can be found at <a href="http://www.therightcrowd.com/?index.php?page=terms" target="_blank">http://www.therightcrowd.com/terms</a>
         <br/> </li>

      <li>
         As an Investor Member of the Site you are permitted to view the information
         and documentation uploaded to the Site in relation to the Investee Companies
         <strong>("Due Diligence Materials")</strong>. Subject to clause 1.3 below, you can view the
         Due Diligence Materials of any of the Investee Companies whilst such
         materials remain available on the Site.
      </li>

      <li>
         The Right Crowd does not verify and is not liable in respect of the accuracy of
         any of the Due Diligence Materials posted on the Site.
      </li>

      <li>
         You may be required to accept the terms of a non-disclosure agreement with
         each relevant Investee Company prior to obtaining access to any of the Due
         Diligence Materials available in relation to a particular Investee Company. The
         non-disclosure agreement will be a legally binding agreement between you
         and the Investee Company. The provisions of such non-disclosure
         agreement are in addition to the confidentiality provisions set out in the
         Member Terms and Conditions found at <a href="http://beta.therightcrowd.com/?index.php?page=terms" target="_blank">http://beta.therightcrowd.com/terms</a>.
      </li>

      <li>
         Investor Members are required to complete a Certificate of High Net Worth, a Sophisticated Investor Certificate (Certified of Self Certified) or a Restricted Investor Certificate completed no earlier than 12 months before submitting a Bid (as defined below) and at intervals of no greater than 12 months thereafter whilst ever you remain an Investor Member.
      </li>

      <li>
         We may contact you from time to time for clarification and confirmation of your
         investor categorisation by email, post, text or phone.
      </li>

      <li>
         Each Investor Member agrees that:
         <ul>
	         <li>effective from its acceptance of these Termsit will subscribe for one deferred share of £0.01 in the share capital of The Right Crowd and that it will hold such share in accordance with The Right Crowd’s articles of association, a copy of which can be viewed <a href="http://beta.therightcrowd.com/files/legal-terms/The Right Crowd Articles of Association.pdf" target="_blank">here</a></li>
	         <li>it waives the right to receive a share certificate in respect of such deferred share.</li>
         </ul>
	  </li>
   </ol>

   <li><strong>INVESTMENT PROCESS AND EQUITY SUBSCRIPTION</strong></li>
   <ol>
      <li>
         Each Investee Company registered on the Site will be available to accept
         online offers for investment (<strong>"Bids"</strong>, each a <strong>"Bid"</strong>) by Investor Members.
      </li>

      <li>
         Bids will only be accepted during a “Bid Period”. Full details of the Bid
         Periods are set out in The Right Crowd rules of the offer process at
         (the <strong>"Rules"</strong>). Investee Companies are under no obligation to accept a
         Bid.
      </li>

      <li>
         Full details of the minimum and maximum amounts which you can offer to
         invest pursuant to a Bid are set out on the Site in respect of the Investee
         Company.
      </li>

      <li>
         By accepting these Terms, you confirm that you accept and will be bound by
         the Rules in relation to all Bids that you make on the Site.
      </li>

      <li>
         A Bid is made on the Site by confirming the amount you wish to invest in the Company by clicking “invest” on the relevant investment application page.  By clicking “invest” on the relevant investment application page you accept and acknowledge that the investment application page, the Bid and these Terms shall form an application/subscription for shares in the relevant Investee Company (“Application”).  The Application shall constitute an offer by you to purchase the shares in the Investee Company at the subscription price indicated on the relevant investment application page.Once the Application has been submitted on the Site, the Investee Company may, at its absolute discretion, choose whether or not to accept your Bid.  You will be notified by The Right Crowd if your Bid has been accepted.  If you do not receive notice of acceptance of your Bid, your Bid has not been accepted.
      </li>

      <li>
         A valid Bid is only made once you have made payment of the subscription amount set out in the Application (“Investment Funds”) to our third party payment provider in accordance with the instructions on the Site.  Our third party payment provider will hold the subscription amount on your behalf until such time as the Investee Company decides whether or not your Bid has been accepted.
      </li>

      <li>
         If your Bid is accepted by the Investee Company, of which you will receive
         notification by email:
      </li>
      <ol>
         <li>the Application shall form a legally binding agreement between you and the Investee Company in connection with your investment in the Investee Company (“Subscription Agreement”); and
         </li>

         <li>
            you hereby authorise the immediate release of the Investment Funds
            from our third party payment provider to the Investee Company
         </li>
      </ol>

      <li>
         The Right Crowd shall not be a party to the Subscription Agreement.
      </li>

      <li>
         If Investment Funds are not received by Investee Company once the Bid has
         been accepted the Bid will be deemed to be a <strong>"Default Bid"</strong> and we reserve
         the right to:
      </li>

      <ol>
         <li>suspend your membership of the Site until further notice;</li>
         <li>terminate your membership of the Site with immediate effect; and/or</li>
         <li>permanently ban you from applying to be a Member of the Site.</li>
      </ol>

      <li>
         In the event of a Default Bid, you shall be in breach of the Subscription
         Agreement pursuant to which the Investee Company shall be entitled to
         pursue you for the monies owed to it.
      </li>
   </ol>

   <li><strong>Subscription terms</strong></li>
   <ol>
	   <li>You acknowledge that if your Bid is accepted but the Investee Company receives applications for more shares than there are available to allot, the Investee Company may allocate you with a smaller number of shares than you applied for in the Application. </li>
	   <li>The investment being made available on the Site is only being made available to persons ordinarily resident in the United Kingdom, save in circumstances referred to in paragraph 3.15(e). Where an Application appears to be made by a person not so resident, the directors of the relevant Investee Company (“Directors”) may request that you prove that you are entitled to apply under the laws of the country in which you are resident and if they are not satisfied, the Application may be rejected by the Directors.</li>
	   <li>The right is reserved by the relevant Investee Company to reject any Application.  The right is also reserved by the relevant Investee Company, at its absolute discretion, to treat as valid any Application which does not fully comply with the conditions set out in the Application, these Terms and any other relevant terms set out on the Site. </li>
	   <li>The subscription list for the receipt of Application will be closed at 5pm on the Offer End Date (as set out on relevant Investee Company investment page on the Site), unless the aggregate amount of shares applied for in Applications received by the relevant Investee Company exceeds the Maximum Cash Amount (as set out on relevant Investee Company investment page on the Site) before such time at which point the subscription list will be closed.  The Directors reserve the right to re-open the subscription list in the event that there is a default on payment of the subscription price pursuant to any Application.  </li>
	   <li>The Directors, exercising their sole discretion, may extend the Offer End Date.</li>
	   <li>Where Applications have been received and the Maximum Cash Amount is exceeded, the relevant Investee Company shall seek to issue all shares in accordance with its articles of association and then, if applicable, on a pro-rata basis (proportionate to the number of shares applied for) to all other applicants.  This is subject always to the discretion of the Directors who may, by exercising their discretion, issue the shares in such proportions as they think fit (subject to the relevant Investee Company’s articles of association). </li>
	   <li>You shall be entitled to revoke your Application at any time until 24 hours after the expiry of the Offer End Date. </li>
	   <li>Once you have submitted your Application, you shall not be permitted to amend the terms of the Application, including the number of shares that you have applied for.  If you wish to increase or decrease the number of shares that you have applied for, you must withdraw your application and re-submit a new application which includes the amended number of shares.  For the avoidance of any doubt, any re-submitted application shall be treated by the relevant Investee Company as a new application and shall rank below any other applications received from other Investor Members up to the date of the submission of the amended application by you.  </li>
	   <li>You authorise the relevant Investee Company to enter your name in the relevant Investee Company’s register of members as holder of the shares allotted to you pursuant to the Application.</li>
	   <li>The allotment of the shares in the relevant Investee Company is strictly conditional on the relevant Investee Company receiving the full subscription price from you.  </li>
	   <li>No person gaining access to the Site in any territory other than the United 	Kingdom may treat the investment as constituting an offer or invitation by an Investee Company or its Directors to purchase or subscribe for shares.</li>
	   <li>Neither the Application nor anything on the Site constitutes an offer to sell, or the solicitation of an offer to buy or to subscribe for, the shares in any jurisdiction outside of the United Kingdom. </li>
	   <li>The shares of the various Investee Companies have not been and will not be registered under the United States Securities Act 	1933 as amended (“US Securities Act”) or the relevant Canadian, Japanese, Australian or Irish securities legislation and therefore such shares may not be offered, sold, transferred or delivered directly or indirectly into the United States or in Canada, Japan, 	Australia or the Republic of Ireland or their respective territories and possessions. The 	United States means the United States of America, each of its states, its territories and possessions and the District of Columbia.</li>
	   <li>The Directors may, in their absolute discretion, require verification of your identity, to the extent that you have not already provided the same. Pending the provision to the Directors of evidence of identity, definitive certificates in respect of the shares you have allocated may be retained or withheld at the Directors’ absolute discretion. If, within a reasonable time after a request for verification of identity, the Directors have not received evidence satisfactory to the Directors, the Directors may, at their absolute discretion, terminate the Application in which event the monies payable on acceptance of Application will be returned to you.</li>
	   <li>In consideration of the Directors agreeing that they will consider and process applications for shares in accordance with the procedures referred to in these Terms, you:
	   		<ol type="A">
		   		<li>warrant that you are not relying on any information or representation made by the relevant Investee Company, its Directors, officers, agents, employees or advisers in relation to your Application;</li>
		   		<li>warrant that you are either resident or ordinarily resident in the United Kingdom and will notify the Director(s) immediately in writing if you cease to be so resident, or in circumstances where you are not so resident you warrant to the relevant Investee Company on the basis set out in paragraph 3.15(e) below;</li>
		   		<li>agree that the Application and/or the Subscription Agreement shall be construed in accordance with and governed by the laws of England and Wales;</li>
		   		<li>warrant that no action has been taken to permit the distribution of the details of the relevant Investee Company’s fundraising in any jurisdiction outside the United Kingdom; </li>
		   		<li>warrant that by submitting the Application, you have complied with all relevant laws and obtained all requisite government or other consents which may be required in connection with your Application and is permitted pursuant to the laws of the jurisdiction in which you are located (if outside the UK) to complete the Application and hold shares in the relevant Investee Company; and that you have complied with all requisite formalities and that you have not taken any action or omitted to take any action which will or may result in the relevant Investee Company or any of such Investee Company’s respective partners, Directors, officers, agents, employees or advisers acting in breach of the legal or regulatory requirements of any territory in connection with the Application;</li>
		   		<li>confirm that you have read, understood and agreed to the terms and conditions of the Application and these Terms, and have taken all appropriate professional advice which you consider necessary before submitting the Application and that you are aware of the special risks involved in participating in an investment of this nature and the applicant understands that your Application is made upon the terms of the Application and these Terms;</li>
		   		<li>acknowledge that, in relation to your Application the advisers of the relevant Investee Company are acting for that Investee Company and are not acting for or on your behalf and that, accordingly they will not be responsible for advising the applicant on any transactions described herein or for ensuring that such transaction is suitable for you; </li>
		   		<li>warrant that you are not engaged in money laundering as defined in the Money Laundering Regulations 2003 and confirm that you are aware of your obligations under the Criminal Justice Act 1993, the Anti-Terrorism Crime and Security Act 2001 and the Proceeds of Crime Act 2002 which relate to money laundering; and</li>
		   		<li>warrant that you are either (i) a self-certified investor; (ii) a high net worth investor; or (iii) a restricted investor; and have delivered to us a signed copy of the relevant certified or self-certified sophisticated investor certificate or high net worth investor certificate or restricted investor certificate.</li>
	   		</ol>
	   </li>
	   <li>The Application is subject to all terms and conditions set out in the Rules set out on 	the Site which you remain bound by in respect of the Application. </li>
	   <li>The Application (and if the Application is accepted, the Subscription Agreement) is personal to the parties hereto and neither party shall, without the 	prior written consent of the other party, assign, transfer, mortgage, charge, subcontract, 	declare a trust over or deal in any other manner with any of its/his rights and obligations under the Application or the Subscription Agreement.</li>
	   <li>The Application and any documents referred to in it are made for the benefit of the 	parties to them and their successors, and are not intended to benefit, or be enforceable, by anyone else. </li>
   </ol>


   <li><strong>DISCLAIMER AND LIMITATION OF LIABILITY </strong></li>

   <ol>
      <li>
         The Right Crowd is engaged by and provides services to the Investee
         Company only. Nothing on the Site or in any written or oral correspondence
         between you and The Right Crowd shall be deemed to be advice of any
         nature given by The Right Crowd to you.
      </li>

      <li>
         Any advisers engaged by the Investee Company are engaged solely for the
         benefit of the Investee Company and do not intend to provide any legal,
         financial, tax or any other advice to you.
      </li>

      <li><strong>
            The Right Crowd strongly recommends that you seek independent legal,
            tax and financial advice before making any Bid on the Site.
         </strong>
      </li>

      <li>
         The Right Crowd shall not be liable for any loss suffered by you as a result of The Right Crowd not receiving any Subscription Application or Bid submitted by you or as a result of any errors made by you on the Subscription Application or the Bid.
      </li>
   </ol>


   <li><strong>GENERAL</strong></li>
   <ol>
      <li>
         All notifications sent by The Right Crowd to you, shall be sent by email to the
         email address which you provide to us from time to time. It is your
         responsibility to ensure that we hold correct and accurate contact details for
         you. We shall not be responsible for any distribution of confidential
         information in the event that you provide us with incorrect contact details and
         you shall be liable to pay The Right Crowd on demand and hold The Right
         Crowd harmless in respect of all costs, charges or losses sustained or
         incurred by The Right Crowd that arise directly or indirectly from you providing
         incorrect contact details to The Right Crowd.
      </li>

      <li>
         If you wish to contact The Right Crowd in connection with these Terms,
         please email admin@therightcrowd.com.
      </li>

      <li>
         If any provision or part-provision of these Terms is or becomes invalid, illegal
         or unenforceable, it shall be deemed modified to the minimum extent
         necessary to make it valid, legal and enforceable. If such modification is not
         possible, the relevant provision or part-provision shall be deemed deleted.
         Any modification to or deletion of a provision or part-provision under this
         clause shall not affect the validity and enforceability of the rest of these
         Terms.
      </li>

      <li>
         If any provision or part-provision of these Terms is invalid, illegal or
         unenforceable, the parties shall negotiate in good faith to amend such
         provision so that, as amended, it is legal, valid and enforceable, and, to the
         greatest extent possible, achieves the intended commercial result of the
         original provision.
      </li>

      <li>
         The Right Crowd may amend these Terms from time to time. If The Right
         Crowd makes any material amendment to these Terms, it will inform you by
         email notification.
      </li>
   </ol>

   @endsection
