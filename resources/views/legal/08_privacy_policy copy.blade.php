<h1>The Right Crowd Limited Privacy Policy</h1>

<p>The Right Crowd Limited ("we") are committed to protecting and respecting your privacy.</p>

<p>This policy (together with our terms of use http://www.therightcrowd.com/terms and any other documents referred to herein) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us.  Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. </p>

<p>By using our site accessed via http://www.therightcrowd.com ("our site") and/or ticking the pop-up box on our site, you confirm that you have read and understood the contents of this privacy policy and you accept the terms set out herein. </p>

<p>For the purpose of the Data Protection Act 1998 (the "Act"), the data controller is The Right Crowd Limited (registration number: 09398506) of 39 Steeple Close, Poole, Dorset BH17 9BJ.</p>

<h2>1. Information we may collect from you </h2>
<p>We may collect and process the following data about you:</p>
<ul>
	<li><strong>Information you give us.</strong> You may give us information about you by filling in forms on our site or by corresponding with us by phone, e-mail or otherwise. This includes information you provide when you register to use our site, subscribe to our service, and when you report a problem with our site. The information you give us may include your name, address, e-mail address and phone number, financial and credit card information or any other personal information.</li>
	<li><strong>Information we collect about you.</strong> With regard to each of your visits to our site we may automatically collect the following information:
		<ul>
			<li>technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform; and</li>
			<li>information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from our site (including date and time); pages you viewed or searched for; page response times; download errors; length of visits to certain pages; page interaction information (such as scrolling, clicks, and mouse-overs); and methods used to browse away from the page and any phone number used to call us. </li>
		</ul>
	</li>
	<li><strong>Information we receive from other sources.</strong> We may receive information about you from third parties that we work closely with (including, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers, search information providers, identity/credit reference agencies) and may receive information about you from them.</li>
</ul>

<h2>2. Cookies</h2>
<p>Our site uses cookies to distinguish you from other users of our site. A cookie is a small file of letters and numbers that we put on your computer, if you agree. These cookies allow us to distinguish you from other users of our site, which helps us to provide you with a good experience when you browse our site and also allows us to improve our site. </p>
<p>The cookies we use are "analytical" cookies. They allow us to recognise and count the number of visitors and to see how visitors move around the site when they are using it. This helps us to improve the way our website works, for example by ensuring that users are finding what they are looking for easily.</p>

<h2>3. Uses made of your information </h2>
<p>We use information held about you in the following ways:</p>
<ul>
	<li>Information you give to us. We will use this information:
		<ol type="A">
			<li>to carry out our obligations arising from any contracts entered into between you and us and to provide you with the information and services that you request from us;</li>
			<li>to provide you with information about other services we offer that are similar to those that you have already purchased or enquired about;</li>
			<li>to provide you, or permit selected third parties to provide you, with information about goods or services we feel may interest you;</li>
			<li>to notify you about changes to our service; and</li>
			<li>to ensure that content from our site is presented in the most effective manner for you and for your computer. </li>
		</ol>
	</li>
	<li>Information we collect about you. We will use this information:
		<ol type="A">
			<li>to administer our site and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;</li>
			<li>to improve our site to ensure that content is presented in the most effective manner for you and for your computer; </li>
			<li>to allow you to participate in interactive features of our service, when you choose to do so;</li>
			<li>as part of our efforts to keep our site safe and secure;</li>
			<li>to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you; and</li>
			<li>to make suggestions and recommendations to you and other users of our site about goods or services that may interest you or them.</li>
		</ol>
	</li>
	<li>Information we receive from other sources. We may combine this information with information you give to us and information we collect about you. We may use this information and the combined information for the purposes set out above (depending on the types of information we receive).</li>
</ul>

<h2>4. Disclosure of your information</h2>
<p>We may share your information with selected third parties including other members, professional service firms, identity and credit reference agencies, suppliers and sub-contractors for the performance of any contract we enter into with them or you.  </p>
<p>We may also share your information with advertisers and advertising networks that require the data to select and serve relevant adverts to you and others.  We would not disclose information about identifiable individuals to our advertisers, but we may provide them with aggregate information about our users. We may also use such aggregate information to help advertisers reach the kind of audience they want to target.  We may make use of the personal data we have collected from you to enable us to comply with our advertisers' wishes by displaying their advertisement to that target audience. </p>
<p>We will also share your information with analytics and search engine providers that assist us in the improvement and optimisation of our site. </p>
<p>We may disclose your personal information to third parties in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets.</p>
<p>We will disclose your personal information if we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our terms of use and/or other agreements; or to protect our rights, property, or safety or those of our members, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</p>

<h2>5. Where we store your personal data</h2>
<p>The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area ("EEA"). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff maybe engaged in, among other things, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</p>
<p>Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</p>
<p>Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.</p>

<h2>6. Your rights</h2>
<p>You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data.  You can also exercise the right at any time by contacting us at admin@therightcrowd.com.</p>
<p>Our site may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates.  If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies.  Please check these policies before you submit any personal data to these websites.</p>

<h2>7. Access to information</h2>
<p>The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of £10 to meet our costs in providing you with details of the information we hold about you.</p>

<h2>8. Changes to our privacy policy</h2>
<p>Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail. Please check back frequently to see any updates or changes to our privacy policy.</p>

<h2>9. Contact</h2>
<p>Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to admin@therightcrowd.com.</p>