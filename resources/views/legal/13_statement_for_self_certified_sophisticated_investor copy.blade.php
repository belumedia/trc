<h1>Statement for Self-Certified Sophisticated Investor</h1>
<p>I declare that I am a self-certified sophisticated investor for the purposes of the Financial Services and Markets Act (Financial Promotion) Order 2001. </p>
<p>I understand that this means: </p>
<ol type="A">
	<li>I can receive financial promotions that may not have been approved by a person authorised by the Financial Services Authority;</li>
	<li>the content of such financial promotions may not conform to rules issued by the Financial Services Authority;</li>
	<li><strong>by signing this statement I may lose significant rights;</strong></li>
	<li>I may have no right to complain to either of the following:
		<ol type="I">
			<li>the Financial Services Authority; or </li>
			<li>the Financial Ombudsman Scheme; </li>
		</ol>
	</li>
	<li>I may have no right to seek compensation from the Financial Services Compensation Scheme. </li>
</ol>
<p>I am a self-certified sophisticated investor because <strong>at least one of the following applies:</strong></p>
<ol type="A">
	<li>I am a member of a network or syndicate of business angels and have been so for at least the last six months prior to the date below; </li>
	<li>I have made more than one investment in an unlisted company in the two years prior to the date below; </li>
	<li>I am working, or have worked in the two years prior to the date below, in a professional capacity in the private equity sector, or in the provision of finance for small and medium enterprises;</li>
	<li>I am currently, or have been in the two years prior to the date below, a director of a company with an annual turnover of at least £1 million. </li>
</ol>
<p><strong>I accept that I can lose my property and other assets from making investment decisions based on financial promotions. </strong></p>
<p>I am aware that it is open to me to seek advice from someone who specialises in advising on investments.</p>
