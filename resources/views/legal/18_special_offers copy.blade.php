<h1>The Right Crowd Limited ("The Right Crowd")</h1>
<h2>The Right Crowd crowdfunding platform ("Platform")</h2>
<h2>Special Offers</h2>
<h2>Terms and Conditions</h2>
<ol>
	<li>First 10,000 REGISTERED Investors Promotion (February 2015)
		<ol>
			<li>This offer applies to the first 10,000 users to register as an investor member on the Platform.</li>
			<li>Each of the first 10,000 investors to register on the Platform shall receive a 1% increase on their first investment on the Platform.</li>
			<li>Illustration:
				<ol type="A">
					<li>You have made a bid to invest &pound;1,000 into company X.  </li>
					<li>Provided that the fundraise for company X is successful, you will receive an additional &pound;10 worth of shares in the company.</li>	
				</ol>
			</li>
			<li>Promotion conditions:
				<ol type="A">
					<li>the investor must make his first investment within 12 months of registering as an investor member on The Right Crowd crowdfunding platform; and</li>
					<li>the investor has not breached any relevant terms and conditions, which include but are but not limited to:
						<ol type="I">
							<li>our Standard Terms and Conditions (Investor);</li>
							<li>our Website Terms of Use;</li>
							<li>our Offer Rules; and</li>
							<li>our standard Terms and Conditions (Members).</li>
						</ol>
					</li>
				</ol>
			</li>
			<li>For the avoidance of doubt, this promotion made be claimed in conjunction with the "First 2000 &pound;500 Investors Promotion (February 2015)" (see paragraph 2 for further details).</li>
			<li>No alternative or substituted reward will be given in conjunction with this promotion.</li>
			<li>The Right Crowd reserves the right to amend, withdraw or extend any or all elements of this promotion at any time by giving notice by email to investors.</li>
			<li>The Right Crowd is a private limited company incorporated in England and Wales under company number 09398506 and whose registered address is at 39 Steeple Close, Poole, Dorset BH17 9BJ.</li>
		</ol>
	</li>
	<li>FIRST 2000 &pound;500 Investors Promotion (February 2015)
		<ol>
			<li>This offer applies to the first 2,000 investors to make a successful bid of &pound;500 or more on The Right Crowd crowdfunding platform.</li>
			<li>For the purpose of this paragraph, a "successful bid" means that:
				<ol type="A">
					<li>the investor has made a bid to invest monies into an investee company;</li>
					<li>the investee company has accepted that bid; </li>
					<li>the investee company has received sufficient bids to meet its minimum investment amount (as set out on the relevant investee company’s fundraising page on the Platform); and</li>
					<li>the investee company has notified the investor that its fundraise on the Platform has been successful.</li>
				</ol>
			</li>
			<li>Each of the first 2,000 investors to make a successful bid of &pound;500 or more on The Right Crowd crowdfunding platform shall receive a 3% increase on their first investment.</li>
			<li>Illustration:
				<ol type="A">
					<li>the investor must make his first investment within 12 months of registering as an investor member on The Right Crowd crowdfunding platform; and</li>
					<li>the investor has not breached any relevant terms and conditions, which include but are but not limited to:
						<ol type="I">
							<li>our Standard Terms and Conditions (Investor);</li>
							<li>our Website Terms of Use;</li>
							<li>our Offer Rules; and</li>
							<li>our standard Terms and Conditions (Members).</li>
						</ol>
					</li>
				</ol>
			</li>
			<li>For the avoidance of doubt, this promotion made be claimed in conjunction with the "First 10,000 Registered Investors Promotion (February 2015)" (see paragraph 1 for further details).</li>
			<li>No alternative or substituted reward will be given in conjunction with this promotion.</li>
			<li>The Right Crowd reserves the right to amend, withdraw or extend any or all elements of this promotion at any time by giving notice by email to investors.</li>
			<li>The Right Crowd is a private limited company incorporated in England and Wales under company number 09398506 and whose registered address is at 39 Steeple Close, Poole, Dorset BH17 9BJ.</li>
			
		</ol>
	</li>
</ol>