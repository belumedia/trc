<h1>Statement for Certified High Net Worth Individual</h1>
<p>I declare that I am a certified high net worth individual for the purposes of the Financial Services and Markets Act 2000 (Financial Promotion) Order 2001.</p>
<p>I understand that this means: </p>
<ol type="A">
	<li>I can receive financial promotions that may not have been approved by a person authorised by the Financial Services Authority;</li>
	<li>the content of such financial promotions may not conform to rules issued by the Financial Services Authority;</li>
	<li><strong>by signing this statement I may lose significant rights;</strong></li>
	<li>I may have no right to complain to either of the following:
		<ol type="I">
			<li>the Financial Services Authority; or </li>
			<li>the Financial Ombudsman Scheme; </li>
		</ol>
	</li>
	<li>I may have no right to seek compensation from the Financial Services Compensation Scheme. </li>
</ol>
<p>I am a certified high net worth individual because <strong>at least one of the following applies:</strong></p>
<ol type="A">
	<li>I had, during the financial year immediately preceding the date below, an annual income to the value of £100,000 or more;</li>
	<li>I held, throughout the financial year immediately preceding the date below, net assets to the value of £250,000 or more. Net assets for these purposes do not include:
		<ol type="I">
			<li>the property which is my primary residence or any loan secured on that residence;</li>
			<li>any rights of mine under a qualifying contract of insurance within the meaning of the Financial Services and Markets Act 2000 (Regulated Activities) Order 2001; or</li>
			<li>any benefits (in the form of pensions or otherwise) which are payable on the termination of my service or on my death or retirement and to which I am (or my dependants are), or may be, entitled.</li>
		</ol>
	</li>
	
</ol>
<p><strong>I accept that I can lose my property and other assets from making investment decisions based on financial promotions. </strong></p>
<p>I am aware that it is open to me to seek advice from someone who specialises in advising on investments.</p>
