<h1>RESTRICTED INVESTOR STATEMENT</h1>
<p>I make this statement so that I can receive promotional communications relating to non-readily realisable securities as a restricted investor. I declare that I qualify as a restricted investor because:</p>
<ol type="A">
	<li>in the twelve months preceding the date below, I have not invested more than 10% of my net assets in non-readily realisable securities; and</li>
	<li>I undertake that in the twelve months following the date below, I will not invest more than 10% of my net assets in non-readily realisable securities.</li>
</ol>
<p>Net assets for these purposes do not include:</p>
<ol type="A">
	<li>the property which is my primary residence or any money raised through a loan secured on that property;</li>
	<li>any rights of mine under a qualifying contract of insurance; or</li>
	<li>any benefits (in the form of pensions or otherwise) which are payable on the termination of my service or on my death or retirement and to which I am (or my dependents are), or may be entitled.</li>
</ol>
<p><strong>I accept that the investments to which the promotions will relate may expose me to a significant risk of losing all of the money or other property invested. I am aware that it is open to me to seek advice from an authorised person who specialises in advising on non-readily realisable securities.</strong></p>

	
	
