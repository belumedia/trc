@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>Investments</h1>
	</div>	
	<div class="col-sm-1 col-lg-2"></div>
</div>




<div class="row content-section content-section-spacer-base">
	<br><br>
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		
		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif
		
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Date</th>
					<th>Investor</th>
					<th>Business Name</th>
					<th>Status</th>
					<th>Amount</th>				
					<th></th>
				</tr>
			</thead>	
			<tbody>
				
				@if(count($investments)>0)
				@foreach($investments as $investment)
					<?php //$investment->get_investment_status(); ?>
					<tr>
						<td>{{ $investment->updated_at->toFormattedDateString() }}</td>
						<td>@if($investment->investor)
							{{ $investment->investor->first_name }} {{ $investment->investor->last_name }}
							@else
								<span class="text-muted">Account Removed</span>
							@endif
						</td>
						<td>{{ $investment->startups->full_business_name }}</td>
						<td>{{ $investment->admin_status }}</td>
						<td>{{ $investment->display_formatted_amount() }}</td>
						<td></td>
						<td class="text-right"><a href="{{ route('admin.startups_view', ['id' => $investment->startups->id]) }}" class="btn btn-xs btn-default">View Startup</a></td>
					</tr>
				@endforeach
				@else
					<tr>
						<td colspan="5" class="text-center"><h5>You don't currently have any investments.</h5></td>
					</tr>				
				@endif
				

			</tbody>
		</table>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection