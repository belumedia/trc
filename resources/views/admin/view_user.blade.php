@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		
		<h1>{{ $user->first_name }} {{ $user->last_name }} <span class="label label-default pull-right">
			<?php $user->get_user_investment_status(); ?>
			{{ $user->investor_type_text }}
		</span></h1>
		<p><strong>Tel:</strong> {{ $user->telephone }} <strong>Email:</strong> {{ $user->email }} </p>
		<p>{{ $user->street }}, {{ $user->street_2 }}, {{ $user->city }}, {{ $user->county }}, {{ $user->postcode }}</p>
	</div>	
	<div class="col-sm-1 col-lg-2"></div>
</div>




<div class="row content-section content-section-spacer-base">
	<br><br>
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-6 col-lg-5">
		
		<h2>Start-Ups</h2>
		
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Startup Name</th>
					<th class="col-sm-2">Status</th>
					<th class="col-sm-1">View</th>
					<th class="col-sm-1">Edit</th>
				</tr>
			</thead>
			<tbody>
				@if($user->count_start_ups()>0)
				@foreach($user->get_start_ups() as $start_up)
					{!! $start_up->get_sart_up_status() !!}
					<tr>
						<td>{{ $start_up->full_business_name }}</td>
						<td><span class="label {{ $start_up->status_class }}">{{ $start_up->status_text }}</span></td>
						<td><a href="{{ route('admin.startups_view', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">View Startup</a></td>
						<td><a href="{{ route('member-edit-startup-start', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Edit Startup</a></td>

					</tr>
				@endforeach
				@else
					<tr>
						<td colspan="4" class="text-center"><h5>User doesn't have any startups.</h5></td>
					</tr>				
				@endif
			</tbody>
		</table>

		<hr>
		
		<h2>Investments</h2>
		
			<table class="table table-striped">
			<thead>
				<tr>
					<th>Invested In</th>
					<th class="col-sm-3">Status</th>
					<th class="col-sm-3">Amount</th>
				</tr>
			</thead>
			<tbody>
				@if($user->count_investments()>0)
				@foreach($user->get_investments() as $investment)
					{!! $investment->get_investment_status() !!}
					<tr>
						<td>{{ $investment->startups->full_business_name }}</td>
						<td>{{ $investment->status }}</td>
						<td>{{ $investment->display_formatted_amount() }}</td>
					</tr>
				@endforeach
				@else
					<tr>
						<td colspan="4" class="text-center"><h5>User doesn't have any investments.</h5></td>
					</tr>				
				@endif
			</tbody>
		</table>

	</div>
		<div class="col-sm-4 col-lg-3">
		
		<div class="panel panel-default">
		  <div class="panel-body">
		  	 {!! Form::model($user, ['route' => ['admin.user_update_access', $user->id], 'class' => '']) !!}
			  <div class="checkbox">
			  	<label>
				{!! Form::checkbox('allow_admin', '1', null); !!} <strong>Admin Access</strong><br><small>This needs to be on to set access rights below.</small>
			  	</label><br>
			  </div>
			  <div class="row">
				  <div class="col-sm-offset-1">
					  <div class="checkbox">
					  	<label>
						{!! Form::checkbox('allow_admin_review', '1', null); !!} <strong>Review Startups</strong>
					  	</label><br>
					  </div>
					  <div class="checkbox">
					  	<label>
						{!! Form::checkbox('allow_admin_options', '1', null); !!} <strong>Edit website options</strong>
					  	</label><br>
					  </div>
					  <div class="checkbox">
					  	<label>
						{!! Form::checkbox('allow_admin_users', '1', null); !!} <strong>Manage Users</strong>
					  	</label><br>
					  </div>
					  <div class="checkbox">
					  	<label>
						{!! Form::checkbox('allow_admin_investors', '1', null); !!} <strong>View Investments</strong>
					  	</label><br>
					  </div>
				  </div>
			  </div>
			  <hr>

			  <div class="checkbox">
			  	<label>
				{!! Form::checkbox('is_sipp_provider', '1', null); !!} <strong>SIPP Provider</strong><br><small>This account needs to act as SIPP provider.</small>
			  	</label><br>
			  </div>

			  {!! Form::hidden('user_id', $user->id) !!}
			  
			  <hr>
			  <button class="btn btn-primary btn-block">Update Access Rights</button>
			</form>
		  </div>
		</div>
		
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection