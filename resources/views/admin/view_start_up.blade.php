@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
		<p>{{ $start_up->business_address }}, {{ $start_up->business_postcode }}</p>
	</div>	
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-6 col-lg-5">
		<div class="panel panel-default">
		  <div class="panel-body">
			  
			  <h3>Business Summary</h3>
			  <p>{{ $start_up->business_summuary }}</p>
			  
			  <h3>Investment Summary</h3>
			  <p>{{ $start_up->investment_summuary }}</p>
			  <hr>
			  <a href="{{ route('member-company-view', ['id' => $start_up->id]) }}" class="btn btn-default">Preview Company</a>
			  
		  </div>
		  
		</div>
		
      @if(count($investments)>0)
      <div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Investments</h3>
			</div>
         <table class="table">
            <thead>
               <tr>
                  <th>Investor</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th></th>
               </tr>
            </thead>
            <tbody>
               @foreach($investments as $investment)
               <?php $investment->get_investment_status(); ?>
               <tr>
                  <td>
	                @if($investment->investor)
						{{ $investment->investor->first_name }} {{ $investment->investor->last_name }}
					@else
						<span class="text-muted">Account Removed</span>
					@endif
				  </td>
                  <td>{{ $investment->display_formatted_amount() }}</td>
                  <td>{{ $investment->admin_status }}</td>
                  <td class="text-right">
                     @if($investment->admin_can_transfer) <a href="{{ route('member-transaction-transfer', ['id' => $investment->id]) }}" class="btn btn-xs btn-success">Transfer</a> @endif
                     @if($investment->admin_can_revesrse) <a href="{{ route('member-transaction-reverse', ['id' => $investment->id]) }}" class="btn btn-xs btn-default">Reverse</a> @endif
                     
                     @if($investment->admin_can_cancel) <a href="" class="btn btn-xs btn-danger">Cancel</a> @endif
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
         <!--
         <div class="panel-footer">
            <a href="" class="btn btn-success btn-sm">Transfer All Funds</a>
         </div>
         -->
      </div>
      @endif
      
      @if(count($reviews)>0)
      <div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Reviews</h3>
			</div>

         <table class="table">
            @foreach($reviews as $review)
            <thead>
               <tr>
                  <th>{{ $review->reviewer->first_name }} {{ $review->reviewer->last_name }}</th>
                  <th class="text-right"><span class="label label-default">{{ $review->rating }} / 5</span></th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td colspan="2">{{ $review->review }}</td>
               </tr>
            </tbody>
             @endforeach
         </table>
         <!--
         <div class="panel-footer">
            <a href="" class="btn btn-success btn-sm">Transfer All Funds</a>
         </div>
         -->
      </div>
      @endif
      
      
       <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Notes</h3>
		</div>

         <table class="table">
            @foreach($notes as $note)
            <thead>
               <tr>
                  <th>{{ $note->author->first_name }} {{ $note->author->last_name }}</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td colspan="2">{{ $note->notes }}</td>
               </tr>
            </tbody>
             @endforeach
         </table>
         <div class="panel-footer">
         	{!! Form::open(['route' => 'admin.startups_save_notes']) !!}
         	{!! Form::textarea('notes', null, ['class' => 'form-control', 'rows' => '5', 'placeholder' => 'Add in notes here']) !!}
         	{!! Form::hidden('id', $start_up->id) !!}
         	<br>
         	<input type="submit" value="Add Notes" class="btn btn-primary">
         	{!! Form::close() !!}
         </div>
      </div>
      
				
	</div>
	<div class="col-sm-4 col-lg-3">
		
		<div class="panel panel-default">
		  <div class="panel-body">
			  {!! Form::model($start_up, ['route' => ['admin.startups_update_status', $start_up->id], 'class' => '']) !!}
			  <div class="form-group">
				  <label>Startup Status</label>
				  {!! Form::select('status_select', $start_up_statuses, null, ['class' => 'form-control']); !!}
			  </div>
			  <div class="form-group">
				<label>Funding Expire Date</label>
			  	<div class='row'>
	               <div class='col-sm-3'>
	                  {!! Form::select('investment_expire_day', FormHelper::options_date_day(), $investment_expire_day, ['class' => 'form-control']) !!}
	               </div>
	               <div class='col-sm-5'>
	                  {!! Form::select('investment_expire_month', FormHelper::options_date_month(), $investment_expire_month, ['class' => 'form-control']) !!}
	               </div>
	               <div class='col-sm-4'>
	                  {!! Form::select('investment_expire_year', FormHelper::options_date_year_funding_expire(), $investment_expire_year, ['class' => 'form-control']) !!}
	               </div>
	            </div>
			  </div>
			  <div class="checkbox">
			  	<label>
				{!! Form::checkbox('startup_no_expire', '1', $startup_no_expire); !!} <strong>Startup doesn't expire</strong>
			  	</label><br>
			  	<small>Click this checkbox if this start-up can seek funding indefinitely.</small>
			  </div>
			  
			  
			  <button class="btn btn-primary btn-block">Update Startup</button>
			</form>
		  </div>
		</div>
		
		<div class="panel panel-default admin-breakdown">
		  <div class="panel-body">
		    <strong>TARGET</strong><br>
		    <h2>{{ $start_up->display_investment_target() }}</h2>
		    <strong>STAKE</strong><br>
		    <h2>{{ $start_up->equity_offered }}%</h2>
		    
		    <strong>DAYS</strong><br>
		    <h2>{{ $start_up->investment_days }} days
		    
			   @if($start_up->funding_expire_days!=0)
			   	<br><small>
			   	{{ $start_up->funding_expire_days }} days remaining
			   	</small>
			   @endif
		    </h2>
			<strong>INVESTED</strong><br>
			<h2>{{ $start_up->invested_total_string }} <span class="text-muted">({{ $start_up->invested_percentage }})</span></h2>
			<strong>TRANSFERED</strong><br>
		    <h2>{{ $start_up->transfered_total_string }}</h2>
		    
		    <strong>Review Score</strong>
		    <h2>{{ $start_up->review_avg_score }} / 5 over {{ $start_up->review_count }} review(s)</h2>
		    
		  </div>
		</div>
		
		<div class="panel panel-default">
		  <div class="panel-body">
		  	<a href="{{ route('admin.startups_send_success', ['id' => $start_up->id]) }}" class="btn btn-success btn-block">Send Funding Success Email</a>
		  	<hr>
		  	<a href="{{ route('admin.startups_send_failed', ['id' => $start_up->id]) }}" class="btn btn-danger btn-block">Send Funding Failed Email</a>
		  </div>
		</div>
		
		
		
		<div class="panel panel-default">
		  <div class="panel-body">
			  {!! Form::model($start_up, ['route' => ['admin.startups_update_display', $start_up->id], 'class' => '']) !!}

			  <div class="checkbox">
			  	<label>
				{!! Form::checkbox('top_pick', '1'); !!} <strong>Show as Top Pick</strong>
			  	</label><br>
			  	<small>This start-up will be displayed on the members home page as a top pick.</small>
			  </div>
			  <hr>
		
			   <div class="checkbox">
			  	<label>
				{!! Form::checkbox('is_featured', '1'); !!} <strong>Show as <strong>Featured</strong> start-up.</strong>
			  	</label><br>
			  	<small>This start-up will be displayed on the front of the web site to potential investors.</small>
			  </div>
			  <br>
			  <button class="btn btn-primary btn-block">Update Visibility</button>
			</form>
		  </div>
		</div>
		
		<div class="panel panel-default">
		  <div class="panel-body">
		  	<a href="{{ route('member-edit-startup-start', ['id' => $start_up->id]) }}" class="btn btn-primary btn-block">Edit Startup</a>
		  </div>
		</div>
		
	</div>

	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection