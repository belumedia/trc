@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>Users</h1>
	</div>	
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<br><br>
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		
		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif
		
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Date Added</th>
					<th class="col-sm-2">Full Name</th>
					<th>Email</th>
					<th class="col-sm-2">Investor Status</th>
					<th class="col-sm-1">Investments</th>
					<th class="col-sm-1">Startups</th>
					<th class="col-sm-1">View</th>
				</tr>
			</thead>
			<tbody>
				@if(count($users)>0)
				@foreach($users as $user)
					<tr>
						<td>{{ $user->created_date_object()->format('jS M y') }}
						<td>{{ $user->first_name }} {{ $user->last_name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->get_investment_status() }}</td>
						<td>{{ $user->count_investments() }}</td>
						<td>{{ $user->count_start_ups() }}</td>
						<td><a href="{{ route('admin.users_view', ['id' => $user->id]) }}" class="btn btn-xs btn-default">View User</a></td>
					</tr>
				@endforeach
				@else
					<tr>
						<td colspan="4" class="text-center"><h5>You don't currently have any users.</h5></td>
					</tr>				
				@endif
			</tbody>
		</table>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection