@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>Review Startups</h1>
	</div>	
	<div class="col-sm-1 col-lg-2"></div>
</div>




<div class="row content-section content-section-spacer-base">
	<br><br>
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		
		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif
		
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Business Name</th>
					<th class="col-sm-2">Status</th>				
					<th class="col-sm-1">View</th>
				</tr>
			</thead>
			<tbody>
				@if(count($start_ups)>0)
				@foreach($start_ups as $start_up)
					{!! $start_up->get_sart_up_status() !!}
					<tr>
						<td>{{ $start_up->full_business_name }}</td>
						<td><span class="label {{ $start_up->status_class }}">{{ $start_up->status_text }}</span></td>
						<td><a href="{{ route('admin.review-add', ['id' => $start_up->id]) }}" class="btn btn-xs btn-default">Review Startup</a></td>
					</tr>
				@endforeach
				@else
					<tr>
						<td colspan="4" class="text-center"><h5>There are no startups requiring review.</h5></td>
					</tr>				
				@endif
			</tbody>
		</table>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection