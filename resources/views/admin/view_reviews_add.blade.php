@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>{{ $start_up->full_business_name }}</h1>
		<p>{{ $start_up->business_address }}, {{ $start_up->business_postcode }}</p>
	</div>	
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-6 col-lg-5">
		<div class="panel panel-default">
		  <div class="panel-body">
			  
			  <h3>Business Summary</h3>
			  <p>{{ $start_up->business_summuary }}</p>
			  
			  <h3>Investment Summary</h3>
			  <p>{{ $start_up->investment_summuary }}</p>
			  
			  <hr>
			  
			  <div role="tabpanel">

         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Overview</a></li>
            <li role="presentation"><a href="#team" aria-controls="team" role="tab" data-toggle="tab">Executive Team</a></li>
            <li role="presentation"><a href="#documents" aria-controls="documents" role="tab" data-toggle="tab">Documents</a></li>
            @if(count($videos)>0)<li role="presentation"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Videos</a></li>@endif
         </ul>

         <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="overview">
               <div class="row">
                  <div class="col-sm-6">
                     <h3>Product or Service Details</h3>
                     <p>{{ $start_up->product_details }}</p>
                  </div>
                  <div class="col-sm-6">
                     <h3>Differentiation & Market Impact</h3>
                     <p>{{ $start_up->market_impact }}</p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <h3>Achievements</h3>
                     <p>{{ $start_up->acheivements }}</p>
                  </div>
                  <div class="col-sm-6">
                     <h3>Monetisation Strategy</h3>
                     <p>{{ $start_up->monetisation_strategy }}</p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <h3>Use of funds</h3>
                     <p>{{ $start_up->use_of_funds }}</p>
                  </div>
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="team">
               <br><br>
               <div class="row">
                  @foreach($team_members as $team_member)
                  <div class="col-sm-4">
                     
                     @if($team_member->photo_url)
                    	<img src="{!! asset('img/team_photo_holder.png'); !!}" alt="{{ $team_member->name }}" class="img-thumbnail img-responsive content-company-team-img"  style="background-image: url({!! asset('team_photo_uploads/resized_'.$team_member->photo_url.'.jpg'); !!});"><br><br>
                     @else                    
                     	<img src="{!! asset('img/no_photo.jpg'); !!}" alt="{{ $team_member->name }}" class="img-thumbnail img-responsive"><br><br>
                     @endif
                     <div class="panel panel-default">
                        <div class="panel-heading"><strong>{{ $team_member->name }}</strong></div>
                        <ul class="list-group">
                           <li class="list-group-item"><strong>Position</strong> {{ $team_member->position }}</li>
                           <li class="list-group-item"><strong>Age</strong> {{ $team_member->age }} years</li>
                           <li class="list-group-item"><strong>Personal Bio</strong><br>{{ $team_member->personal_bio }}</li>
                           <li class="list-group-item"><strong>Involvement</strong><br>{{ $team_member->involvement }}</li>
                        </ul>
                     </div>
                  </div>				    	
                  @endforeach
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="documents">

               <table class="table table-striped">
                  <thead>
                     <tr>
                        <th>Document</th>
                        <th class="col-sm-1">Type</th>
                        <th class="col-sm-1">Size</th>
                        <th class="col-sm-2">Download</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($documents as $document)
                     <tr>
                        <td><strong>{{ $document->title }}</strong><br><small>{{ $document->description }}</small></td>
                        <td>PDF</td>
                        <td>0KB</td>
                        <td><a href="{!! asset('uploads/'.$document->file_location); !!}" class="btn btn-default btn-sm" target="_blank"><span class="glyphicon glyphicon-arrow-down"></span> Download Document</a></td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
            @if(count($videos)>0)
            <div role="tabpanel" class="tab-pane" id="videos">
               @foreach($videos as $video)
               <br>
               <div class="embed-responsive embed-responsive-16by9">
                  <iframe  class="embed-responsive-item" src="{{ $video->get_embed_url() }}" frameborder="0" allowfullscreen></iframe>
               </div>
               <br>
               @endforeach
            </div>
            @endif
         </div>

      </div>
			  
		  </div>
		</div>
		
		
		
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Review</h3>
			</div>
			<div class="panel-body">
			  {!! Form::open(['route' => ['admin.review-save', $start_up->id], 'class' => '']) !!}
			  
			  <div class="form-group">
				  <label for="review">Review Score</label>
				  {!! Form::select('review_score', ['1' => '1 Star', '2' => '2 Star', '3' => '3 Star', '4' => '4 Star', '5' => '5 Star'], null, ['class' => 'form-control']); !!}
			  </div>
			  
			  <div class="form-group">
				  <label for="review">Review</label>
				  {!! Form::textarea('review', null, ['class' => 'form-control']); !!}
			  </div>
			  
			  <hr>
			  	<strong>Please feel free to add comments, but ensure no advice is given.</strong>
			  <hr>
			  
			  <button class="btn btn-primary">Add Review</button>
			</form>
		  </div>
		</div>
		
     				
	</div>
	<div class="col-sm-4 col-lg-3">
				
		<div class="panel panel-default admin-breakdown">
		  <div class="panel-body">
		    <strong>TARGET</strong><br>
		    <h2>{{ $start_up->display_investment_target() }}</h2>
		    <strong>STAKE</strong><br>
		    <h2>{{ $start_up->equity_offered }}%</h2>
		  </div>
		</div>
		

	</div>

	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection


