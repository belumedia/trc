@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>Member Terms and Conditions</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">

	<p>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY AND MAKE SURE YOU UNDERSTAND THEM BEFORE JOINING THE WEBSITE. </p>

		<h2>1. INTRODUCTION </h2>
		<p>1.1 The Right Crowd Limited (“The Right Crowd”, “we”, “us”) operates the website accessed via http://www.therightcrowd.com (the “site”).</p>
		<p>1.2 This page (together with the documents expressly referred to on it) tells you (“you”) information about us and the legal terms and conditions (the “Terms”) on which we operate the area of our site through which our members (each a “Member”) can seek or offer investment (the “Full Site”).  You cannot access the Full Site, unless you are a Member. </p>
		<p>1.3 To be able to access the Full Site you must be registered as a Member on our site.  To become a Member you must open an account (“Membership Account”).  </p>
		<p>1.4 To open a Membership Account you must follow the instructions on our site, complete and submit to us the online application form and accept these Terms, by clicking the "I Accept" button on the Site.  </p>
		<p>1.5 By accepting these Terms you confirm that you have read and understood these Terms (including the documents expressly referred to herein, including our <a href="http://www.therightcrowd.com/terms-conditions" target="_blank">terms of use</a> and our <a href="http://www.therightcrowd.com/privacy-policy" target="_blank">privacy policy</a>) and you agree to be legally bound by them in relation to your use of the site, the Full Site and your receipt of the services that we provide on the Full Site.  If you refuse to accept these Terms, you will not be able to access the Full Site. </p>
		<p>1.6 Before accepting these Terms and every time we amend these Terms (please see paragraph 10 below) you should ensure that you understand the Terms which apply at that time. Each time you use the Full Site, the Terms which are in force at that time will apply to the contract between you and us.</p>
		<p>1.7 You should print a copy of these Terms, or save them to your computer, for future reference.</p>

		<h2>2. ABOUT US</h2>
		<p>2.1 We are a limited company registered in England and Wales under number 09398506 and our registered office is at 39 Steeple Close, Poole, Dorset BH17 9BJ. </p>
		<p>2.2 The Right Crowd Limited is an appointed representative of Equity For Growth Ltd which is authorised and regulated by the Financial Conduct Authority with registration number 475953.</p>
		<p>2.3 If you want to contact us, please use the contact details set out in paragraph 11 of these Terms.</p>

		<h2>3. YOUR PERSONAL INFORMATION</h2>
		<p>3.1 We will only use your personal information in accordance with our <a href="http://www.therightcrowd.com/privacy-policy" target="_blank">Privacy Policy</a>, which can be found at http://www.therightcrowd.com/privacy-policy.  Please take some time to read our Privacy Policy as it includes important terms which apply to you. </p>
		<p>3.2 You acknowledge and agree that, at any time, we may use any reasonable means that we consider necessary to verify your identity and creditworthiness with any third party providers and to permit us to comply with all applicable laws in relation to the site. </p>

		<h2>4. YOUR MEMBERSHIP ACCOUNT </h2>
		<p>4.1 Before you can use access the Full Site, you need to open a Membership Account, in accordance with the application process set out below.  </p>
		<p>4.2 To apply for a Membership Account, you must complete the online application form (“Application”) in accordance with the instructions set out on the site.  We may refuse any Application for any reason (without providing the reason to you). </p>
		<p>4.3 Your username will be the email address you use in the Application. You must also select a password, known only to you, and which you must keep strictly confidential. </p>
		<p>4.4 Prior to The Right Crowd accepting your Application, we may undertake identification and credit checks about you.  We will not accept any Application until we have received satisfactory results from our third party verification providers. We reserve the right to undertake additional identification and credit checks about you at any time whilst you remain a Member. </p>
		<p>4.5 Your Membership Account is non-transferable. The Membership Account may only be used by the person who created it and not by anyone else. We reserve the right to terminate your Membership Account if we have reason to believe that your Membership Account details are being used by anyone other than you and/or you have not kept your password confidential. </p>
		<p>4.6 You shall be liable for all activities that are undertaken using your Membership Account, together with the associated password, and you shall compensate us for all and any losses, damages, costs and expenses we may suffer as a result of any failure by you to keep your username and password strictly confidential.</p>
		<p>4.7 In consideration of our accepting your Application to open a Membership  Account, you represent and warrant as follows:
			<ol type="A">
				<li>you are 18 years of age or over; </li>
				<li>you are located in the United Kingdom;</li>
				<li>you are resident for tax purposes in the United Kingdom;</li>
				<li>you have a United Kingdom bank account;</li>
				<li>you are of sound mind and capable of taking responsibility for your own actions and you can enter into a legally binding contract with us; </li>
				<li>the details submitted in your Application are true, accurate and not misleading;</li>
				<li>you will inform us immediately if any of the information you provided in your Application changes; and</li>
				<li>you are not an undischarged bankrupt and you are not in a voluntary arrangement with your creditors. </li>
			</ol>
		</p>
		<p>4.8 It is your responsibility to keep your contact details up to date on your Membership Account.  We may, from time to time, send you important information using the details that you have provided to us.  If those details are incorrect or not up-to-date you may not receive important information relating to your Membership Account, the Full Site or these Terms. </p>
		<p>4.9 We may close your Membership Account at any time.  We will use our reasonable endeavours to give advance notice to you of such closure; however, this may not always be possible.</p>
		<p>4.10 If you wish to close your Membership Account please send your request to us by email to admin@therightcrowd.com. On receipt of an email request to close your Membership Account, we will close the Membership Account as soon as we can.</p>
		<p>4.11 You cannot open or hold more than one Membership Account on the site at any time. </p>
		<p>4.12 Once you have registered a Membership Account with us, which has been accepted by us, you will be a Member.  </p>
		<p>4.13 Through the Full Site, The Right Crowd provides a platform through which you can seek or offer investment to other Members.</p>
		<p>4.14 By registering as a Member you agree and acknowledge that, The Right Crowd, in providing you access to its platform is not:
			<ol type="A">
				<li>dealing in investments as principal or as agent;</li>
				<li>arranging deals in investments;</li>
				<li>managing investments;</li>
				<li>safeguarding and administering investments;</li>
				<li>advising on investments; or</li>
				<li>conducting any other regulated activity specified in Financial Services and Markets Act 2000 (Regulated Activities) Order 2001 (SI 2001/544), other than as would be permitted pursuant to its permissions as a corporate finance firm regulated by the Financial Conduct Authority.</li>
			</ol>
		</p>

		<h2>5. OUR INTELLECTUAL PROPERTY RIGHTS</h2>
		<p>5.1 We own, or are the licensor of, all intellectual property rights in and to the site and the Full Site, including but not limited to:
			<ol type="A">
				<li>all copyright and related rights in and to the site and the Full Site; </li>
				<li>all trade mark rights (whether registered or unregistered) in “The Right Crowd”, “The Right Crowd” ; and</li>
				<li>the domain name http://www.therightcrowd.com.</li>
			</ol>
		</p>
		<p>5.2 We hereby grant to you a revocable, royalty-free, non-exclusive licence to access the site and the Full Site provided that you comply with these Terms. Your use of our site and/or the Full Site confers no rights whatsoever to the content and related intellectual property rights contained on our site or the Full Site.</p>

		<h2>6. CONFIDENTIAL INFORMATION</h2>
		<p>6.1 You acknowledge and agree that:
			<ol type="A">
				<li>when you access the Full Site, you will be party to information relating to the business, finance, affairs, customers, clients or suppliers of various investee companies and other confidential information from time to time (“Confidential Information”);</li>
				<li>you will not at any time disclose to any person any Confidential Information except as permitted by paragraph 6.3; and</li>
				<li>you may be required to enter into a separate confidentiality agreement in respect of a particular investee company.</li>
			</ol>
		</p>
		<p>6.2 The Information is not Confidential Information if the information is, or subsequently becomes, public knowledge other than as a direct or indirect result of the information being disclosed in of these Terms.</p>
		<p>6.3 You may disclose the Confidential Information if required to do so by law, a court of competent jurisdiction or any governmental or regulatory authority.</p>
		<p>6.4 be required to agree to strict provisions relating to the disclosure of confidential information.  </p>

		<h2>7. OUR LIABILITY</h2>
		<p>7.1 Nothing in these Terms or any other terms and conditions posted on the site limits or excludes our liability for:
			<ol type="A">
				<li>death or personal injury caused by our negligence;</li>
				<li>fraud or fraudulent misrepresentation; and/or</li>
				<li>any other liability which cannot be excluded or limited by applicable law. </li>
			</ol>
		</p>
		<p>7.2 We make no warranties or representations (express or implied) about the site, in particular, we do not represent or warrant that the information that you find on the Full Site is true and accurate.  Although we ask all of our Members to ensure that all information that they post on the Full Site is true, accurate and not misleading, we do not verify this information or confirm its accuracy. </p>
		<p>7.3 No part of the Full Site is intended to constitute advice.  We strongly recommend that you obtain independent advice in connection with all transactions which you undertake on the Full Site.</p>
		<p>7.4 We do not make any guarantees that by registering as a Member on the site, that you will achieve any specific results, for example, a successful investment.  We act as a platform through which investments can be advertised, offered and concluded and advice and business services can be advertised; we do not endorse any of the advertisements on the Full Site.  You must always undertake your own research prior to entering into any contract with any other Member.</p>
		<p>7.5 We give no warranties that our site is and will remain free of defects or faults.  If a defect or fault affects any online process which you are attempting to complete on the site, we will seek to correct such defect or fault as soon as possible. </p>
		<p>7.6 We will not be liable or responsible for any failure to perform, or delay in performance of, any of our obligations under these Terms that is caused by an Event Outside Our Control.  “An Event Outside Our Control” means any act or event beyond our reasonable control, including without limitation any act of God, strikes, lock-outs or other industrial action by third parties, civil commotion, riot, invasion, terrorist attack or threat of terrorist attack, war (whether declared or not) or threat or preparation for war, fire, explosion, storm, flood, earthquake, subsidence, epidemic or other natural disaster, or obstruction or failure of public or private telecommunications networks. </p>
		<p>7.7 If an Event Outside Our Control takes place that affects the performance of our obligations to you:
			<ol type="A">
				<li>we will contact you as soon as reasonably possible to notify you; and</li>
				<li>our obligations will be suspended and the time for performance of our obligations will be extended for the duration of the Event Outside Our Control. </li>
			</ol>
		</p>

		<h2>8. NOTICES AND INFORMATION FROM US</h2>
		<p>8.1 You hereby agree to receiving all information from us electronically, by email, using the email address that is registered to your Membership Account.  </p>
		<p>8.2 Any notice given by you to us, or by us to you, will be deemed received and properly served immediately when posted on our website or 24 hours after an e-mail is sent.  In proving the service of any notice, it will be sufficient to prove, in the case of an e-mail, that such e-mail was sent to the specified e-mail address of the addressee. The provisions of this paragraph shall not apply to the service of any proceedings or other documents in any legal action.</p>

		<h2>9. VARYING THESE TERMS</h2>
		<p>9.1 We may amend these Terms from time to time. We will inform you of any material changes to these Terms by emailing you at the email address we have for you on our records.</p>
		<p>9.2 If we amend these Terms at any time whilst you are a Member, you will be asked to accept the amended Terms on the next time that you log-in to the Full Site.  You will not be able to log-in to the Full Site until you have accepted the amended Terms.  If you do not wish to accept the amended Terms, you should contact us at admin@therightcrowd.com and we will close your Membership Account. </p>

		<h2>10. CONTACT US</h2>
		<p>If you have any queries in relation to the Full Site or these Terms, please contact The Right Crowd by email at admin@therightcrowd.com or write to us at The Right Crowd Limited, 39 Steeple Close, Poole, Dorset BH17 9BJ.  We shall deal with your query as quickly as possible. </p>

		<h2>11. OTHER IMPORTANT TERMS</h2>
		<p>11.1 We may transfer our rights and obligations under these Terms to another organisation, but this will not affect your rights or our obligations under these Terms. </p>
		<p>11.2 You may not assign these Terms to any third party.  </p>
		<p>11.3 This contract, created by you accepting these Terms, is between you and us. No other person shall have any rights to enforce any of its terms, whether under the Contracts (Rights of Third Parties Act) 1999 or otherwise. </p>
		<p>11.4 Each of the paragraphs of these Terms operates separately. If any court or relevant authority decides that any of them are unlawful or unenforceable, the remaining paragraphs will remain in full force and effect.</p>
		<p>11.5 If we fail to insist that you perform any of your obligations under these Terms, or if we do not enforce our rights against you, or if we delay in doing so, that will not mean that we have waived our rights against you and will not mean that you do not have to comply with those obligations. If we do waive a default by you, we will only do so in writing, and that will not mean that we will automatically waive any later default by you.</p>
		<p>11.6 These Terms are governed by the laws of England and Wales. This means that any dispute or claim arising out of or in connection with these Terms will be governed by the laws of England and Wales. You and we both agree to that the courts of England and Wales will have exclusive jurisdiction.</p>



	</div>
	<div class="col-sm-1"></div>
</div>

@stop
