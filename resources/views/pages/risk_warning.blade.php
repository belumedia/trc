@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>RISKS OF INVESTING IN SHARES</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">


<p>To help you understand the risks involved when investing in shares on The Right Crowd website (http://www.therightcrowd.com) (“The Right Crowd Platform”), please read the following risk summary. Please invest aware and diversify your investments. </p>

<h2>The need for diversification when you invest</h2>
<p>Diversification involves spreading your money across multiple investments to reduce risk. However, it will not lessen all types of risk. Diversification is an essential part of investing. Investors should only invest a proportion of their available investment funds via The Right Crowd Platform and should balance this with safer, more liquid investments.  </p>

<h2>Risks when investing in equity</h2>
<p>Investing in shares (also known as equity) on The Right Crowd Platform does not involve a regular return on your investment unlike loans which offer interest paid regularly. Please bear in mind the following particular risks for equity investments:</p>

<h2>Loss of investment</h2>
<p>The majority of start-up businesses fail or do not scale as planned and therefore investing in these businesses may involve significant risk. It is likely that you may lose all, or part, of your investment. You should only invest an amount that you are willing to lose and should build a diversified portfolio to spread risk and increase the chance of an overall return on your investment capital. If a business you invest in fails, neither the company – nor The Right Crowd Platform – will pay you back your investment. </p>

<h2>Lack of liquidity</h2>
<p>Liquidity is the ease with which you can sell your shares after you have purchased them. Buying shares in businesses pitching through The Right Crowd Platform cannot be sold easily and they are unlikely to be listed on a secondary trading market, such as AIM or the London Stock Exchange. Even successful companies rarely list shares on such an exchange. </p>

<h2>Rarity of dividends</h2>
<p>Dividends are payments made by a business to its shareholders from the company’s profits.  Most of the companies pitching for equity on The Right Crowd Platform website are start-ups or early stage companies, and these companies will rarely pay dividends to their investors. This means that you are unlikely to see a return on your investment until you are able to sell your shares. Profits are typically re-invested into the business to fuel growth and build shareholder value. Businesses have no obligation to pay shareholder dividends.</p>

<h2>Dilution</h2>
<p>Any investment in shares made through The Right Crowd Platform may be subject to dilution in the future. Dilution occurs when a company issues more shares. Dilution affects every existing shareholder who does not buy any of the new shares being issued. As a result an existing shareholder's proportionate shareholding of the company is reduced, or ‘diluted’, this has an effect on a number of things, including voting, dividends and value.</p>


	</div>
	<div class="col-sm-1"></div>
</div>

@stop