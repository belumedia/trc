@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>Standard Terms and Conditions (Investee Companies)</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">

	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">

<p>These are the standard terms and conditions (the "Terms") under which The Right Crowd Limited of 39 Steeple Close, Poole, Dorset BH17 9BJ (Company number: 09398506) ("The Right Crowd", "us" or "we") shall provide the Services to the Company ("you") in connection with the Project.</p>
<p>The Right Crowd Limited is an appointed representative of Equity For Growth Ltd which is authorised and regulated by the Financial Conduct Authority with registration number 475953.</p>
<p>References in these Terms to the Engagement Letter shall mean the engagement letter from The Right Crowd.  Where there is any conflict between the provisions of these Terms and the Engagement Letter, the Engagement Letter will prevail.</p>
<p>These Terms form part of and are deemed incorporated into the Engagement Letter.<br>
	Words and phrases defined in the Engagement Letter shall have the same meaning in these Terms.</p>


<h2>1. Interpretation</h2>
<p>1.1 In these Terms, the following definitions apply:</p>
<p>Contract: the contract, which shall comprise the Engagement Letter and these Terms, between The Right Crowd and the Company for the supply of Services in accordance with these Terms; and</p>
<p>Engagement Letter: the engagement letter which sets out the scope of the Services to be provided by The Right Crowd to the Company.</p>
<p>Investor Member or Investor: all members of the Platform classified as “Investor Members”. </p>
<p>1.2 In these Terms, the following rules apply:
	<ol type="A">
		<li>a person includes a natural person, corporate or unincorporated body (whether or not having separate legal personality);</li>
		<li>a reference to a party includes its successors or permitted assigns;</li>
		<li>a reference to a statute or statutory provision is a reference to such statute or statutory provision as amended or re-enacted. A reference to a statute or statutory provision includes any subordinate legislation made under that statute or statutory provision, as amended or re-enacted; and</li>
		<li>a reference to writing or written includes faxes and e-mails.</li>
	</ol>
</p>

<h2>2. Basis of contract</h2>
<p>2.1 By accepting the terms of the Engagement Letter (by clicking the acceptance button on the Site (“Acceptance”)) you agree that the provision of the Services shall be governed by the Contract which shall apply to the exclusion of all other terms which may be implied or proposed or otherwise presented by you. </p>
<p>2.2 The Contract shall come into existence on the date of on which we notify you that you are approved as an investee company on Platform. </p>
<p>2.3 The Contract constitutes the entire agreement between the parties. You acknowledge that you have not relied on any statement, promise, representation, assurance or warranty made or given by or on behalf of The Right Crowd which is not set out in the Contract. </p>

<h2>3 Supply of Services & Investment Funds</h2>
<p>3.1 The Right Crowd shall supply the Services to the Company in accordance with the provisions of the Engagement Letter in all material respects. </p>
<p>3.2 The Right Crowd shall use reasonable skill and care in the provision of the Services.  </p>
<p>3.3 The Right Crowd shall use all reasonable endeavours to meet any performance dates specified in the Engagement Letter (if any), but any such dates shall be estimates only and time shall not be of the essence for performance of the Services.</p>
<p>3.4 The Right Crowd shall have the right to make any changes to the Services which are necessary to comply with any applicable law or safety requirement, or which do not materially affect the nature or quality of the Services, and The Right Crowd shall notify you in any such event.</p>
<p>3.5 In supplying the Services, The Right Crowd shall not provide commercial, legal, accountancy or tax advice.  Any information given to the Company does not and is not intended to constitute commercial, legal, accountancy or tax advice in respect of any aspect of the Project. </p>
<p>3.6 As part of the Services, The Right Crowd may from time to time offer you ancillary services (“Ancillary Services”) to be provided by third party service providers (“Third Party Service Providers”).  You acknowledge and accept that The Right Crowd may receive an introduction fee (or some other similar payment) where the Company enters into a contract with the third party provider for such Ancillary Services. </p>
<p>3.7 Our third party payment provider shall accept and hold on behalf of the Company all investment funds received in from Investor Members. Such third party shall hold such funds until the Project has completed at which time we shall instruct our third party payment provider to either:
	<ol type="A">
		<li>return funds to Investors if the Minimum Target Amount (as set out in the Engagement Letter) has not been achieved; or</li>
		<li>release (but not pay) the investment funds to you if the Minimum Target Amount has been achieved.</li>
	</ol>
</p>
<p>3.8 In the event that investment funds are released to you pursuant to clause 3.7(b) above, you hereby irrevocable authorise us to instruct our third party provider to:
	<ol type="A">
		<li>transfer an amount equal to the Fee (see clause 5 below) to us; and</li>
		<li>transfer an amount equal to any fees payable by you to Equity For Growth Ltd; </li>
		<li>transfer an amount equal to any fees payable by you to any Third Party Service Providers to such Third Party Service Providers; and</li>
		<li>transfer the remainder of the investment funds to the Company’s bank account in accordance with instructions and bank account details provided by you.</li>
	</ol>
</p>

<h2>4 Your obligations </h2>
<p>4.1 You shall:
	<ol type="A">
		<li>provide The Right Crowd with timely instructions, information and materials necessary for The Right Crowd to perform the Services and ensure that all such instructions, information and materials that it provides to The Right Crowd are complete and accurate;</li>
		<li>co-operate with The Right Crowd in all matters relating to the Services;</li>
		<li>provide The Right Crowd, its employees, agents, consultants and subcontractors, with access to your premises, office accommodation and other facilities as reasonably required by The Right Crowd;</li>
		<li>provide The Right Crowd with such information and materials as The Right Crowd may reasonably require in order to supply the Services, and ensure that such information is accurate in all material respects; and</li>
		<li>obtain and maintain all necessary permissions and consents which may be required before the date on which the Services are to start.</li>
	</ol>
</p>

<p>4.2 If The Right Crowd's performance of any of its obligations under the Contract is prevented or delayed by any act or omission by you or failure by you to perform any relevant obligation (“Default”):
	<ol type="A">
		<li>The Right Crowd shall without limiting its other rights or remedies have the right to suspend performance of the Services until you remedy the Default, and to rely on the Default to relieve it from the performance of any of its obligations to the extent the Default prevents or delays The Right Crowd's performance of any of its obligations;</li>
		<li>The Right Crowd shall not be liable for any costs or losses sustained or incurred by you arising directly or indirectly from The Right Crowd's failure or delay to perform any of its obligations as set out in this clause 4.2; and </li>
		<li>you shall reimburse The Right Crowd on written demand for any costs or losses sustained or incurred by The Right Crowd arising directly or indirectly from the Default.</li>
	</ol>
</p>

<h2>5 Fees</h2>
<p>5.1 The Fee shall be calculated in accordance with the terms set out in the Engagement Letter. </p>
<p>5.2 The Fee shall only become due and payable in the event that the Company achieves a Successful Investment (as defined below). </p>
<p>5.3 A “Successful Investment” is one or more offers for investment which are accepted by the Company on terms and conditions agreed with the Company which result in the Company receiving the Minimum Target Amount (as defined in the schedule to the Engagement Letter). </p>
<p>5.4 The Right Crowd shall invoice you for the Fee on completion of the Project and shall be entitled to retain the Fee (and any other monies owed by you to The Right Crowd) from the investment monies received by or on behalf of the Company pursuant to a Successful Investment. </p>
<p>5.5 All amounts payable by you under the Contract are exclusive of amounts in respect of value added tax chargeable for the time being (“VAT”). Where any taxable supply for VAT purposes is made under the Contract by The Right Crowd to you, you shall, on receipt of a valid VAT invoice from The Right Crowd, pay to The Right Crowd such additional amounts in respect of VAT as are chargeable on the supply of the Services at the same time as payment is due for the supply of the Services. </p>
<p>5.6 If you fail to make any payment due to The Right Crowd under the Contract by the due date for payment, then you shall pay interest on the overdue amount at the rate of 4% per cent per annum above Barclays Bank Plc's base rate from time to time. Such interest shall accrue on a daily basis from the due date until actual payment of the overdue amount, whether before or after judgment. You shall pay the interest together with the overdue amount.</p>
<p>5.7 You shall pay all amounts due under the Contract in full without any set-off, counterclaim, deduction or withholding (except for any deduction or withholding required by law). The Right Crowd may at any time, without limiting its other rights or remedies, set off any amount owing to it by you against any amount payable by The Right Crowd to you.</p>

<h2>6 Intellectual property rights </h2>
<p>6.1 All intellectual property rights in or arising out of or in connection with the Services and/or the Project shall be owned by The Right Crowd.</p>
<p>6.2 All materials, equipment, documents and other property of The Right Crowd used in connection with the Project are, and shall remain, the exclusive property of The Right Crowd.</p>

<h2>7 Limitation of liability</h2>
<p>7.1 Nothing in these Terms shall limit or exclude The Right Crowd's liability for:
	<ol type="A">
		<li>death or personal injury caused by its negligence, or the negligence of its employees, agents or subcontractors;</li>
		<li>fraud or fraudulent misrepresentation; or</li>
		<li>any other liability that cannot be excluded or limited by English law.</li>
	</ol>
</p>
<p>7.2 Subject to clause 7.1:
	<ol type="A">
		<li>The Right Crowd shall under no circumstances whatever be liable to you, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, for any special, indirect or consequential losses or any pure economic loss, costs, expenses or damage, nor shall The Right Crowd be responsible for any loss of profit, income, production, loss of business, depletion of goodwill, loss of contract, loss of data or any accruals arising in any circumstances whatsoever, whether in contract, tort, under statute or otherwise and howsoever caused; and</li>
		<li>The Right Crowd's total liability to you in respect of all other losses arising under or in connection with the Contract, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, shall in no circumstances exceed £100,000.</li>
	</ol>
</p>
<p>7.3 All implied warranties and conditions are excluded to the maximum extent permitted by law. </p>
<p>7.4 This clause 7 shall survive termination of the Contract.</p>

<h2>8 Force majeure</h2>
<p>8.1 For the purposes of the Contract, Force Majeure Event means an event beyond the reasonable control of The Right Crowd including but not limited to strikes, lock-outs or other industrial disputes (whether involving the workforce of The Right Crowd or any other party), failure of a utility service or transport network, act of God, war, riot, civil commotion, malicious damage, compliance with any law or governmental order, rule, regulation or direction, accident, breakdown of plant or machinery, fire, flood, storm or default of suppliers or subcontractors.</p>
<p>8.2 The Right Crowd shall not be liable to you as a result of any delay or failure to perform its obligations under the Contract as a result of a Force Majeure Event.</p>
<p>8.3 If the Force Majeure Event prevents The Right Crowd from providing any of the Services for more than 30 days, The Right Crowd shall, without limiting its other rights or remedies, have the right to terminate the Contract immediately by giving written notice to you.</p>


<h2>9 Severance</h2>
<p>9.1 If any provision or part-provision of the Contract is or becomes invalid, illegal or unenforceable, it shall be deemed modified to the minimum extent necessary to make it valid, legal and enforceable. If such modification is not possible, the relevant provision or part-provision shall be deemed deleted. Any modification to or deletion of a provision or part-provision under this clause shall not affect the validity and enforceability of the rest of the Contract.</p>
<p>9.2 If any provision or part-provision of the Contract is invalid, illegal or unenforceable, the parties shall negotiate in good faith to amend such provision so that, as amended, it is legal, valid and enforceable, and, to the greatest extent possible, achieves the intended commercial result of the original provision.</p>

<h2>10 Waiver</h2>
<p>A waiver of any right under the Contract or law is only effective if it is in writing and shall not be deemed to be a waiver of any subsequent breach or default. No failure or delay by a party in exercising any right or remedy provided under the Contract or by law shall constitute a waiver of that or any other right or remedy, nor shall it prevent or restrict its further exercise of that or any other right or remedy. No single or partial exercise of such right or remedy shall prevent or restrict the further exercise of that or any other right or remedy. </p>

<h2>11 No partnership or agency</h2>
<p>Nothing in the Contract is intended to, or shall be deemed to, establish any partnership or joint venture between the parties, nor constitute either party the agent of the other for any purpose. Neither party shall have authority to act as agent for, or to bind, the other party in any way.</p>

<h2>12 Third parties </h2>
<p>The Services are provided to and for the benefit of you only.  No other person may use or rely upon the Services nor derive any rights or benefits from them.  The provisions of the Contracts (Rights of Third Parties) Act 1999 are to that extent excluded. </p>

<h2>13 Variation</h2>
<p>Except as set out in these Terms, no variation of the Contract, including the introduction of any additional terms and conditions, shall be effective unless it is agreed in writing and signed by The Right Crowd.</p>

<h2>14 Governing law AND jurisdiction</h2>
<p>14.1 The Contract, and any dispute or claim arising out of or in connection with it or its subject matter or formation (including non-contractual disputes or claims), shall be governed by, and construed in accordance with the law of England and Wales.</p>
<p>14.2 Each party irrevocably agrees that the courts of England and Wales shall have exclusive jurisdiction to settle any dispute or claim arising out of or in connection with the Contract or its subject matter or formation (including non-contractual disputes or claims).</p>



	</div>
	<div class="col-sm-1"></div>
</div>

@stop
