@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1></h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">

	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">
		<h2>We're Sorry the page you've just tried to access has caused an error.</h2>
		<p>Our engineers have been notified and are now working to resolve this issue.</p>

		<hr>
		<h2>What Next...</h2>
		<h3>1. Click your browsers back button and try again.</h3>
		<h3>2. If you keep seeing this error please email us at support@therightcrowd.com.</h3>

	</div>
	<div class="col-sm-1"></div>
</div>

@stop
