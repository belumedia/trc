@extends('layouts.companies')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>Companies</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">
		<p>Banks not providing funds? Fed up with managers loosing your money? Its time to take control! Become your own manager with The Right Crowd.</p>
		<p>Read the Investee company documents, listen to their stories and ideas and ask your own probing questions before investing and taking control of your own future.</p>
		<hr>
		<h2>Get investment capital for your business</h2>
		<br>
		<div class="row">
			<div class="col-sm-4">
				<img src="img/lg_icon_register.png" class="img-responsive">
				<h3>1. Register</h3>
				<p><a href="{{ url('auth/register') }}">Register</a> with us and add your company listing.</p>
			</div>
			<div class="col-sm-4">
				<img src="img/lg_icon_list.png" class="img-responsive">
				<h3>2. List Your Company</h3>
				<p>Complete the Executive summary page along with attaching financials and/or presentations as you wish and note how much you are looking to raise and on what terms.</p>
			</div>
			<div class="col-sm-4">
				<img src="img/lg_icon_review.png" class="img-responsive">
				<h3>3. Review</h3>
				<p>The Right Crowd Executive Board will then review your proposal and vote on the viability of your company. Majority rules in favor and you are ready to go.</p>
			</div>
		</div>
		<br><br>
		
		<div class="row">
			<div class="col-sm-4">
				<img src="img/lg_icon_shop.png" class="img-responsive">
				<h3>4. Open for Investment</h3>
				<p>Once your company is live, a member of The Right Crowd will contact you to discuss any ideas that may help your company win the financial commitments of the investors.</p>
			</div>
			<div class="col-sm-4">
				<img src="img/lg_icon_qanda.png" class="img-responsive">
				<h3>5. Investor Q&amp;A</h3>
				<p>Investors will be sent details of your company and will then start to review your business plan and strategy. At this point be ready for some questions. Investor/Investee interaction is very important.</p>
			</div>
			<div class="col-sm-4">
				<img src="img/lg_icon_speech.png" class="img-responsive">
				<h3>6. Stay Proactive</h3>
				<p>Once Investors start to view your profile and place commitments for investment, you need to keep updating any news and developments to show how proactive you are as a business.</p>
			</div>
		</div>
		<br><br>
		
		<div class="row">
			<div class="col-sm-4">
				<img src="img/lg_icon_money.png" class="img-responsive">
				<h3>7. Funding Goal Reached</h3>
				<p>Upon completion of your fund raising, the investment capital will be held for 15 days, so any investors can act upon their cooling off period. They will then be deposited direct to your company account less The Right Crowd Fees.</p>
			</div>
			<div class="col-sm-4">
				<img src="img/lg_icon_complete.png" class="img-responsive">
				<h3>Congratulations</h3>
				<p>Congratulations, your funding is in place.  Now's the time to implement your business plan and look to the future growth of your company. From here The Right Crowd partner Equity For Growth will be on hand to advise you as and when you feel it is needed.</p>
			</div>
		</div>
		<br><br>
		
		<div class="content-join">
			<a href="{{ route('join-company') }}">Get Started</a>
		</div>
	</div>
	<div class="col-sm-1"></div>
</div>

@stop