@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h1>Opportunities</h1><p>Take a look at some of the businesses on The Right Crowd.</p></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
	     @foreach($start_ups as $start_up)
			<div class="row">
				<div class="col-sm-12">
				@include('partials.opportunity_display')
				</div>
			</div>
	     @endforeach
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>



@stop