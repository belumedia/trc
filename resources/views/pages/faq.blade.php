@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>FAQ's</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">

	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">

		<h3>How does crowd funding with The Right Crowd work?</h3>
	<p>There are 4 steps:</p>
	<ol>
		<li><strong>Create your profile, and your project</strong> - Each project will be reviewed carefully by our Executive board and must be approved before fundraising can begin.</li>
		<li><strong>Market your project</strong> - Take advantage of our social media tools to share your project with friends, family, colleagues, customers, the media and potential investors.</li>
		<li><strong>Gather investors</strong> - Interested investors will be able to invest in projects in amounts as small as &pound;10, or as large as they wish, via our online payment system.</li>
		<li><strong>Collect funds</strong>- Once fundraising is completed for your project, all funds will be collected and sent to your designated account.</li>
	</ol>

	<h3>How do I create an account?</h3>
	<p>To create an account, <a href="{{ url('/auth/register') }}">click here</a>. You must enter the following information to set up your The Right Crowd account:</p>
	<ul>
		<li>Your name,</li>
		<li>User information</li>
   	</ul>
   	<p>Simply follow the on screen instructions as you go.</p>

   	<h3>How long does each project have to raise their target amounts?</h3>
   	<p>Every project has 90 days in which to hit their target. This time may be extended at The Right Crowd's sole discretion.</p>

   	<h3>My project has hit its target. What happens now?</h3>
   	<p>Once your project reaches its target amount, the funds will be deposited into the project owners account 15 days after close. The Right Crowd team will contact successful project owners, and investors.</p>


   	<h3>What if I never reach the target amount?</h3>
   	<p>That depends on the funding model you have opted to use.</p>

   	<h4>With the 'all-or-nothing' funding model</h4>
	<p>If you do not hit your funding goal, your investors are never charged, and you are under no obligation to complete your project. This ensures that you will not be but into the position of having to deliver on an only partially-funded project.</p>

	<h4>The 'keep all you raise' model</h4>
	<p>This is better suited to projects that are likely to produce returns even if not fully funded. It allows the entrepreneur use all the money that investors pledge even if the funding goal is not met.</p>

	<h3>Are you compliant with regulations promulgated by the FCA (Financial Conduct Authority)?</h3>
	<p>The Right Crowd is fully compliant with Financial Conduct Authority regulations. The FCA is an independent body tasked with the regulation of UK financial services.</p>

	<h3>Once I hit my target amount, can I continue raising funds?</h3>
	<p>Yes. This is called overfunding, and it is encouraged.</p>

	<h3>How do I start fundraising with The Right Crowd?</h3>
	<p>First you must register for an account. Then you can start defining your project, and submit it for approval. Once your project is approved, you can begin fundraising.</p>

	<h3>Do project owners have to be PLCs (Public Limited Companies) to fund raise with The Right Crowd?</h3>
	<p>Not at all, should you look at listing your company in the future then it can be changed at a later date.</p>

	<h3>Who can register projects with The Right Crowd?</h3>
	<p>Any European Citizen with a business idea and or and existing business may attempt to register a project for fundraising.</p>

	<h3>Do I have to set a target amount?</h3>
	<p>Your project must have a target amount, and it cannot be changed after we publish it.</p>

	<h3>If my project is successfully funded, how will I receive my money?</h3>
	<p>The funds raised will be deposited into your bank account.</p>

	<h3>What fees does The Right Crowd charge?</h3>
	<p>It is free to register your account and start a project with The Right Crowd. If your project reaches its funding target, The Right Crowd charges a fee of 4.5% plus Vat on the amount raised and the same level of Warrants in the company, therefore aligning itself with both the investees and investors interests.</p>

	<h3>Is there maximum amount of funds that can be raised for a single project?</h3>
	<p>No, the amount that can be raised is not capped.</p>

	<h3>Can I run more than one project at a time?</h3>
	<p>No, each owner can only run one project at a time.</p>

	<h3>Can my project be edited while it is fundraising?</h3>
	<p>Yes, but any amendments must be approved by The Right Crowd before they are published.</p>

	<h3>Can an owner end a project early?</h3>
	<p>Yes, projects can be cancelled before the full 90 days has run. Please contact the The Right Crowd team to do so.</p>

	<h3>Can I back my own project?</h3>
	<p>You may, but you must actually invest the funds through the The Right Crowd platform if it is is part of the raising.</p>

	<h3>Can I notify investors and interested site members about changes to my project?</h3>
	<p>Yes, interested parties can be notified about project updates. Log in to your The Right Crowd account for full access to this and other social tools.</p>

	<h3>What claims can I make in my project?</h3>
	<p>The claims you make should be fully referenced and verifiable through an independent source. For example, if you plan to claim that your market is worth &euro;5 billion per year, you should have evidence to prove it.</p>

	<h3>Which social media platforms can I use with The Right Crowd?</h3>
	<p>We are now, or soon will be, on all major social networks including Facebook, Linkedin, twitter and Google+.</p>

	<h3>What happens to my money while waiting to find out if the target funding amount is reached?</h3>
	<p>If a project uses the 'all-or-nothing' funding model and does not hit its funding goal, the investors are never charged at all. Once the fundraising period is finished, if the goal has been reached investor's accounts will be debited.</p>
	<p>If the 'keep all you raise' model is used, investors debited immediately even if the funding goal is not met.</p>

	<h3>How can I invest with The Right Crowd projects?</h3>
	<p>You must first register as a member. Once you find a project you are interested in, simply click 'invest' and type the amount desired into the text box. The minimum amount you can invest is &pound;10.</p>

	<h3>Why should I back a project?</h3>
	<ul>
		<li>In hopes of realising a capital profit as the company grows.</li>
		<li>To help start a new business</li>
		<li>To support entrepreneurship</li>
	</ul>

	<h3>What do I risk by backing a project?</h3>
	<p>There are always some risks when investing, particularly in start-ups. You may lose some or all of the money you backed a project with. Never invest more than you can afford to lose.</p>

	<h3>Can I communicate with project owners?</h3>
	<p>Yes, we encourage it. You may ask project owners questions via email or The Right Crowd's social tools, and owners are encouraged to update interested members frequently.</p>

	<h3>How do I know if a project is a good one?</h3>
	<p>The Right Crowd only publishes projects that meet our strict standards and guidelines, but even so some will inevitably fail. Investors must use their own judgement to Asses a projects chances of success.</p>

	<h3>Do you publish the amount I invest?</h3>
	<p>No. The amount you have contributed to a project never appears to others on the  The Right Crowd website.</p>

	<h3>Will I be notified when a project I'm backing reaches its target amount?</h3>
	<p>Yes, all investors are notified via email when a project reaches its target amount.</p>

	<h3>Is it legal for people in my country to register and invest with The Right Crowd?</h3>
	<p>That varies from country to country, and any user of the The Right Crowd site should determine for themselves whether it is legal for them to view or invest in these projects in their current country.</p>

	<h3>What is diversification and how could it reduce the risk of investing?</h3>
	<p>Investment is never without risk, but many experts agree that investing in several different ventures at once leaves you less vulnerable than 'putting all your eggs in one basket'. Each investor must decide on their own strategy. The Right Crowd recommends looking at investing across a range of projects.</p>

	<h3>How can I make a complaint?</h3>
	<p>If you are dissatisfied with our service in any way, please do not hesitate to inform us via any of the methods on our contact us page. We will make every effort to resolve the issue satisfactorily.</p>

		<div class="content-join">
			<a href="{{ route('join-investor') }}">Get Started</a>
		</div>
	</div>
	<div class="col-sm-1"></div>
</div>

@stop
