@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>About Us</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">
		
		<p>We help businesses and entrepreneurs find the investors they need, and make sure they are ready to accept investment in compliance with all relevant regulations. We offer a straightforward and effective platform that brings investors and entrepreneurs together to build companies looking to offer potentially large gains for all parties.</p>

		<p>All companies are looked at by our Executive Board of industry veterans and Equity For Growth, a majority rule is used to determine who is and who isn't listed on The Right Crowd.</p>
				
		<div class="row">
			<div class="col-sm-6">
				<h2>Nigel Payne</h2>
				<p>Mr Payne has 27 years' experience at board level, covering a variety of industries: advertising, manufacturing, distribution, retail, finance and e-commerce. He has had wide-ranging exposure to various types of corporate activity, including acquisitions, flotations and fundraising. Nigel is currently Non-Executive Chairman of AIM-traded Hangar8 plc. Nigel was Chief Executive of Sportingbet UK Plc between 2000 and 2006 and a non-executive director from 2006 to 2013. Between 1995 and 2000 Nigel was group finance, business development and IT director of Polestar Magazines, the largest independent printer in Europe (operating in 19 countries). Between 1993 and 1995 Nigel was finance and IT director of Home Brewery Plc, a subsidiary of Scottish &amp; Newcastle Plc.</p>
			</div>	
			<div class="col-sm-6">
				<h2>George Rolls</h2>
				<p>Mr Rolls has been a Director, manager and adviser to a number of private companies over the past 30 years in a variety of sectors including manufacturing, print media, technology, aviation and consumer products. He co-founded Beaufort Securities of which he was a director between 1985 and 2006. Since selling his interest in Beaufort Securities in 2006, George has acted as a consultant for private high net worth individuals, assisted with the launch of a software technology fund and sits on the boards of AIM listed Hangar 8 Plc and Volare Aviation Limited as a director. He is also a trustee of the Geoffrey de Havilland Flying Foundation and the Honorary Secretary of The Air Squadron Limited.</p>
			</div>
		</div>
		
		<div class="row">

			<div class="col-sm-6">
				<h2>Richard Mew</h2>
				<p>Richard has over 30 years’ experience in the oil and gas industry at senior management and board level.  Richard is currently a Director of Burgate Exploration and Production, Delta Oil Company, and Vantica Resources, all upstream oil and gas companies.  He is also a Director of Koyos Resources, a company focused on providing business development, commercial and technical support to the international oil and gas industry.  Prior appointments include being CEO of Gold Oil plc, Business Development Director of Centrica Energy, and senior management positions both in the UK and abroad with Conoco, Amerada Hess, Chevron and Shell International.​</p>
			</div>	
			<div class="col-sm-6">
				<h2>Equity For Growth</h2>
				<p>Equity For Growth prides itself on it's expertise in helping companies grow with expert guidance and knowledge of the markets. Working alongside industry veterans and specialists in a wide range of disciplines, so that they can bring you the best advice possible and help your company to grow.</p>
			</div>
		</div>

	
			<div class="content-join">
			<a href="{{ route('join-investor') }}">Get Started</a>
		</div>
	</div>
	<div class="col-sm-1"></div>
</div>

@stop