@extends('layouts.companies')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h1>Case Studies</h1><p>Take a look at some of the businesses on The Right Crowd.</p></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


@include('partials.case_study_section')



@stop