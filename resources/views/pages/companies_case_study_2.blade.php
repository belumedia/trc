@extends('layouts.companies')

@section('content')

<div class="row content-section content-section-title">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>Ticket HQ Ltd.</h1>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-4 col-lg-2">
      <div class="thumbnail">
         <img src="{{ asset('img/sample_company_image.jpg') }}" class="content-company-profile-img">
      </div>
   </div>
   <div class="col-sm-8 col-lg-6 content-sub-page">

      <h3>About the Business</h3>
      <p>Quo cu movet consul repudiare, tale enim latine in pri, viderer qualisque quo id. Vero deleniti praesent ei has, sonet adversarium an pri, eu eam stet accusamus. Vix id autem gloriatur vituperata. Et eros perpetua duo, admodum consectetuer ea quo. Vel putent quidam delicatissimi ad, pertinacia voluptatibus conclusionemque vix ut.</p>
 
      <div class="row content-company-profile-financial-row">
         <div class="col-md-4">
            <lead>&pound;50,000</lead><br>
            <small>TARGET</small>
         </div>
         <div class="col-md-4">
            <lead>&pound;63,340</lead><br>
            <small>INVESTED</small>
         </div>
         <div class="col-md-4">
            <lead>126%</lead><br>
            <small>FUNDED</small>
         </div>
      </div>	
     
      <h3>The Funding Process</h3>
      <p>Quo cu movet consul repudiare, tale enim latine in pri, viderer qualisque quo id. Vero deleniti praesent ei has, sonet adversarium an pri, eu eam stet accusamus. Vix id autem gloriatur vituperata. Et eros perpetua duo, admodum consectetuer ea quo. Vel putent quidam delicatissimi ad, pertinacia voluptatibus conclusionemque vix ut.</p>
      <p>Admodum menandri democritum ad quo, ferri postulant cu per, unum iusto ei pro. Cum no molestie eleifend gubergren. Dicant dolorem et mel. Ne nobis feugiat labores per, ei eam falli congue. Persius quaeque inermis ad eos, errem vocibus albucius at vel. Alterum saperet mea in, vitae probatus ex eam, an labore blandit quo.</p>
      
      <hr>
      <a href="{{ route('join-company') }}" class="pull-right btn btn-primary" >Get Started <span class="glyphicon glyphicon-chevron-right"></span></a>	

   </div>
   <div class="col-sm-1 col-lg-2"></div>

</div>


@endsection
