@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>Investor Membership Terms and Conditions.</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">

	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">

<p>
   These terms and conditions (the <strong>"Terms"</strong>) set out the basis on which you will be
   permitted to use the site accessed via <a href="http://www.therightcrowd.com/">http://www.therightcrowd.com/</a> (the <strong>"Site"</strong>) to
   access information about potential investment opportunities and invest funds in the
   companies registered on the Site (<strong>"Investee Companies"</strong>) in exchange for an equity
   stake in such Investee Companies.
</p>

<p>
   The Site is operated by The Right Crowd Limited of 39 Steeple Close, Poole, Dorset
   BH17 9BJ (<strong>"The Right Crowd"</strong>, <strong>"us"</strong> or <strong>"we"</strong>).
</p>

<p>
   The Right Crowd Limited is an appointed representative of Equity For Growth Ltd which is
   authorised and regulated by the Financial Conduct Authority with registration number
   475953.
</p>

<p>
   References in these Terms to “you” and the “Investor Member” are references to
   you as the individual who has completed the Investor Member Application Form at
   <a href="http://www.therightcrowd.com/" target="_blank">http://www.therightcrowd.com/join</a> to
   which these Terms are attached.
</p>

<p>
   <strong>
      By accepting these Terms (by clicking the acceptance button on the Site) you
      agree to bound by these Terms and all documents referred to herein. If you do
      not agree to these Terms and such documents referred to herein, you must not
      accept these Terms and you will not be permitted to invest any monies in
      Investee Companies through the Site.
   </strong>
</p>

<ol>
   <li><strong>INVESTOR MEMBERS</strong></li>

   <ol>
      <li>
         By completing the online application form at <a href="http://www.therightcrowd.com/auth/register" target="_blank">http://www.therightcrowd.com/auth/register</a> and
         accepting these Terms, you confirm that you wish to be an <strong>"Investor Member"</strong>
         on the Site. By accepting these Terms you warrant that you have accepted
         and continue to comply with the Member Terms and Conditions and our
         Privacy Policy which can be found at <a href="http://www.therightcrowd.com/privacy-policy" target="_blank">http://www.therightcrowd.com/privacy-policy</a>
         <br/> </li>

      <li>
         As an Investor Member of the Site you are permitted to view the information
         and documentation uploaded to the Site in relation to the Investee Companies
         <strong>("Due Diligence Materials")</strong>. Subject to clause 1.3 below, you can view the
         Due Diligence Materials of any of the Investee Companies whilst such
         materials remain available on the Site.
      </li>

      <li>
         The Right Crowd does not verify and is not liable in respect of the accuracy of
         any of the Due Diligence Materials posted on the Site.
      </li>

      <li>
         You may be required to accept the terms of a non-disclosure agreement with
         each relevant Investee Company prior to obtaining access to any of the Due
         Diligence Materials available in relation to a particular Investee Company. The
         non-disclosure agreement will be a legally binding agreement between you
         and the Investee Company. The provisions of such non-disclosure
         agreement are in addition to the confidentiality provisions set out in the
         Member Terms and Conditions found at <a href="http://www.therightcrowd.com/membership-terms" target="_blank">http://www.therightcrowd.com/membership-terms</a>.
      </li>

      <li>
         Investor Members are required to complete a Certificate of High Net Worth, a
         Sophisticated Investor Certificate (Certified of Self Certified) or a Restricted
         Investor Certificate completed no earlier than 12 months before completion of
         the application form and at intervals of no greater than 12 months thereafter
         whilst ever you remain an Investor Member.
      </li>

      <li>
         We may contact you from time to time for clarification and confirmation of your
         investor categorisation by email, post, text or phone.
      </li>

      <li>
         Each Investor Member agrees that it will subscribe for one deferred share of
         £0.01 in the share capital of The Right Crowd and that it will hold such share
         in accordance with The Right Crowd’s articles of association, a copy of which
         can be viewed here <a href="http://www.therightcrowd.com/documents/new_articles_of_association.pdf" target="_blank">http://www.therightcrowd.com/documents/new_articles_of_association.pdf</a>
      </li>
   </ol>

   <li><strong>INVESTMENT PROCESS AND EQUITY SUBSCRIPTION</strong></li>
   <ol>
      <li>
         Each Investee Company registered on the Site will be available to accept
         online offers for investment (<strong>"Bids"</strong>, each a <strong>"Bid"</strong>) by Investor Members.
      </li>

      <li>
         Bids will only be accepted during a “Bid Period”. Full details of the Bid
         Periods are set out in The Right Crowd rules of the offer process at
         (the <strong>"Rules"</strong>). Investee Companies are under no obligation to accept a
         Bid.
      </li>

      <li>
         Full details of the minimum and maximum amounts which you can offer to
         invest pursuant to a Bid are set out on the Site in respect of the Investee
         Company.
      </li>

      <li>
         By accepting these Terms, you confirm that you accept and will be bound by
         the Rules in relation to all Bids that you make on the Site.
      </li>

      <li>
         A Bid is made on the Site by confirming the amount you wish to invest in the
         Company and confirming the same once the relevant agreement has been
         completed (by us on your behalf) on the Site (<strong>"Subscription Agreement"</strong>).
         You must confirm the amount you wish to invest in the Company otherwise
         your offer will be rejected. By providing the relevant confirmation and
         submitting the Subscription Agreement you agree to be bound by the terms
         attached thereto. Once the Subscription Agreement has been submitted on
         the Site, the Investee Company may, at its absolute discretion, choose
         whether or not to accept your Bid. You will be notified by The Right Crowd
         if your Bid has been accepted. If you do not receive notice of acceptance of
         your Bid, your Bid has not been accepted.
      </li>

      <li>
         A valid Bid is only made once you have made payment of the subscription
         amount set out in the Subscription Agreement <strong>("Investment Funds")</strong> to our
         third party payment provider in accordance with the instructions on the Site.
         Our third party payment provider will hold the subscription amount on your
         behalf until such time as the Investee Company decides whether or not your
         Bid has been accepted.
      </li>

      <li>
         If your Bid is accepted by the Investee Company, of which you will receive
         notification by email:
      </li>
      <ol>
         <li>the Subscription Agreement shall form a legally binding agreement
            between you and the Investee Company in connection with your
            investment in the Investee Company; and
         </li>

         <li>
            you hereby authorise the immediate release of the Investment Funds
            from our third party payment provider to the Investee Company
         </li>
      </ol>

      <li>
         The Right Crowd shall not be a party to the Subscription Agreement.
      </li>

      <li>
         If Investment Funds are not received by Investee Company once the Bid has
         been accepted the Bid will be deemed to be a <strong>"Default Bid"</strong> and we reserve
         the right to:
      </li>

      <ol>
         <li>suspend your membership of the Site until further notice;</li>
         <li>terminate your membership of the Site with immediate effect; and/or</li>
         <li>permanently ban you from applying to be a Member of the Site.</li>
      </ol>

      <li>
         In the event of a Default Bid, you shall be in breach of the Subscription
         Agreement pursuant to which the Investee Company shall be entitled to
         pursue you for the monies owed to it.
      </li>
   </ol>

   <li><strong>DISCLAIMER AND LIMITATION OF LIABILITY </strong></li>

   <ol>
      <li>
         The Right Crowd is engaged by and provides services to the Investee
         Company only. Nothing on the Site or in any written or oral correspondence
         between you and The Right Crowd shall be deemed to be advice of any
         nature given by The Right Crowd to you.
      </li>

      <li>
         Any advisers engaged by the Investee Company are engaged solely for the
         benefit of the Investee Company and do not intend to provide any legal,
         financial, tax or any other advice to you.
      </li>

      <li><strong>
            The Right Crowd strongly recommends that you seek independent legal,
            tax and financial advice before making any Bid on the Site.
         </strong>
      </li>

      <li>
         The Right Crowd shall under no circumstances whatever be liable to you,
         whether in contract, tort (including negligence), breach of statutory duty, or
         otherwise, for any direct, indirect, special or consequential losses or any pure
         economic loss, costs, expenses or damage, nor shall The Right Crowd be
         responsible for any loss of profit, income, production, loss of business,
         depletion of goodwill, loss of contract, loss of data or any accruals arising in
         any circumstances whatsoever, whether in contract, tort, under statute or
         otherwise and howsoever caused.
      </li>
   </ol>


   <li><strong>GENERAL</strong></li>
   <ol>
      <li>
         All notifications sent by The Right Crowd to you, shall be sent by email to the
         email address which you provide to us from time to time. It is your
         responsibility to ensure that we hold correct and accurate contact details for
         you. We shall not be responsible for any distribution of confidential
         information in the event that you provide us with incorrect contact details and
         you shall be liable to pay The Right Crowd on demand and hold The Right
         Crowd harmless in respect of all costs, charges or losses sustained or
         incurred by The Right Crowd that arise directly or indirectly from you providing
         incorrect contact details to The Right Crowd.
      </li>

      <li>
         If you wish to contact The Right Crowd in connection with these Terms,
         please email admin@therightcrowd.com.
      </li>

      <li>
         If any provision or part-provision of these Terms is or becomes invalid, illegal
         or unenforceable, it shall be deemed modified to the minimum extent
         necessary to make it valid, legal and enforceable. If such modification is not
         possible, the relevant provision or part-provision shall be deemed deleted.
         Any modification to or deletion of a provision or part-provision under this
         clause shall not affect the validity and enforceability of the rest of these
         Terms.
      </li>

      <li>
         If any provision or part-provision of these Terms is invalid, illegal or
         unenforceable, the parties shall negotiate in good faith to amend such
         provision so that, as amended, it is legal, valid and enforceable, and, to the
         greatest extent possible, achieves the intended commercial result of the
         original provision.
      </li>

      <li>
         The Right Crowd may amend these Terms from time to time. If The Right
         Crowd makes any material amendment to these Terms, it will inform you by
         email notification.
      </li>
   </ol>



	</div>
	<div class="col-sm-1"></div>
</div>

@stop
