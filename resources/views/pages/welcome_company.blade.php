@extends('layouts.companies')

@section('content')

@include('components.carousel')

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8  content-section-front-page">
		<div class="row">
			<div class="col-sm-4">
				<h2>We are the future of crowdfunding &amp; co-investment</h2>
			</div>
			<div class="col-sm-8">

				<p>We help businesses and entrepreneurs find the investors they need, and make sure they are ready to accept investments in a manner compliant with all relevant regulations.</p>
				<p>We offer a straightforward and effective platform that brings investors and entrepreneurs together to build companies increasing the chances of potential gains. </p>

				<hr>
				<!-- <a href="" class="btn btn-default">Watch A Video</a> -->
				<a href="{!! route('companies-get-started') !!}" class="btn btn-default hidden-xs">How to get Funded</a>
				<a href="{{ url('auth/register') }}" class="btn btn-primary pull-right">Get Started <span class="glyphicon glyphicon-chevron-right"></span></a>

			</div>
		</div>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<!--
<div class="row content-section content-section-title content-section-title-spacer-top">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h2>Previous Success Stories</h2></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@include('partials.case_study_section')
-->

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<h3>Corporate Finance House Backed</h3>
				<p>The Right Crowd is an appointed Representative of Equity For Growth Ltd an FCA Authorised Corporate finance house, with a team specialising in UK stock market Listings and Trade sales for companies.</p>
				<p>Through its deal with Equity For Growth, The Right Crowd investors are safe in the knowledge that companies funded through our platform are working with professionals looking to build their business to a level of profit realisation for shareholders.</p>
			</div>
			<div class="col-sm-4">
				<h3>Enjoy an uplift on us</h3>
				<p>Up to 4% increase in shares purchased in your first investment if you invest £500 or more.</p><p>​The Right Crowd takes half of its fee in Equity in the company raising funds to align our interests with that of you the investor.</p><p><a href="{{ route('special-offer-terms') }}">Deal Terms & Conditions Apply</a></p>
			</div>
			<div class="col-sm-4">
				<h3>Executive board review</h3>
				<p>Our Executive board is constantly reviewing new companies looking to list on The Right Crowd Platform, all companies are looked at with a view to are they going in the right direction to look at a trade sale or a stock market listing in the not too distant future.</p><p> We want to help investors not only make good investments in fledgling companies but also work to bring realisation to their portfolios to allow them to access some cash down the line to start the process again. </p>
			</div>
		</div>
	</div>
</div>


@stop
