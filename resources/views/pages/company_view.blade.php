@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-10 col-lg-8">
      <h1>{{ $start_up->full_business_name }}</h1>
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
   <div class="col-sm-1 col-lg-2"></div>
   <div class="col-sm-4 col-lg-2">
      <div class="thumbnail">
			@if(!empty($start_up->logo_file))
				<img src="{!! asset('img/company_photo_holder.png'); !!}" class="content-company-profile-img" style="background-image: url({!! asset('photo_uploads/resized_'.$start_up->logo_file.'.jpg'); !!});"> 
			@else
				<img src="{!! asset('img/no_photo_startup.jpg'); !!}" class="content-company-profile-img">
			@endif
      </div>
   </div>
   <div class="col-sm-8 col-lg-6 content-sub-page">
   		  		
      <h2>Company Details</h2>
      <hr>

      <h3>About the Business</h3>
      <p>{{ substr($start_up->business_summuary,0, 300) }}...</p>
      
      
      
      @if($start_up->funding_expire_days!=0)
		<div class="row content-company-profile-financial-row">
			@if($start_up->invested_total_integer<1000)
				<div class="col-lg-12">
					<lead>Just Launched</lead><br>
					<small>This startup has just been added to The Right Crowd</small>
				</div>
			@else
			
				<div class="col-md-3">
					<lead>{{ $start_up->display_investment_target() }}</lead><br>
					<small>TARGET</small>
				</div>
				<div class="col-md-2">
					<lead>{{ $start_up->funding_expire_days }}</lead><br>
					<small>DAYS LEFT</small>
				</div>
				<div class="col-md-3">
					<lead>{{ $start_up->invested_total_string }}</lead><br>
					<small>INVESTED</small>
				</div>
				<div class="col-md-2">
					<lead>{{ $start_up->invested_percentage }}</lead><br>
					<small>FUNDED</small>
				</div>
				<div class="col-md-2">
	            	<lead>{{ $start_up->equity_offered }}%</lead><br>
					<small>EQUITY</small>
	         	</div>
         	@endif
		</div>
			   	
	  @else
      
      <div class="row content-company-profile-financial-row">
      		@if($start_up->invested_total_integer<1000)
				<div class="col-lg-12">
					<lead>Just Launched</lead><br>
					<small>This startup has just been added to The Right Crowd</small>
				</div>
			@else
				<div class="col-md-3">
		            <lead>{{ $start_up->display_investment_target() }}</lead><br>
		            <small>TARGET</small>
		         </div>
		         <div class="col-md-3">
		            <lead>{{ $start_up->invested_total_string }}</lead><br>
		            <small>INVESTED</small>
		         </div>        
		         <div class="col-md-3">
		            <lead>{{ $start_up->invested_percentage }}</lead><br>
		            <small>FUNDED</small>
		         </div>
		         <div class="col-md-3">
		            <lead>{{ $start_up->equity_offered }}%</lead><br>
		            <small>EQUITY</small>
		         </div>
			@endif
      </div>
      	
	  @endif	


      <hr>
      <a href="{{ route('join-investor') }}" class="pull-right btn btn-primary" >Get Started <span class="glyphicon glyphicon-chevron-right"></span></a>
      
   </div>
   <div class="col-sm-1 col-lg-2"></div>
</div>


@endsection
