@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>Investors</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">

	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">
		<p>Banks not providing returns? Fed up with managers loosing your money? Its time to take control! Become your own manager with The Right Crowd.</p>
		<p>Read the Investee company documents, listen to their stories and ideas and ask your own probing questions before investing and taking control of your own future.</p>
		<hr>

		<h2>Register &amp; invest in 5 easy steps</h2>
		<br>

		<div class="row">
			<div class="col-sm-4">
				<img src="img/lg_icon_register.png" class="img-responsive">
				<h3>1. Register</h3>
				<p><a href="{{ url('auth/register') }}">Register with us </a></p>
				<ul>
					<li>Complete the declaration for Professional Investor, Self Certified High Net Worth or Restricted Investor</li>
					<li>Access full details and documentation on the companies listed on The Right Crowd</li>
				</ul>
			</div>
			<div class="col-sm-4">
				<img src="img/lg_icon_browse.png" class="img-responsive">
				<h3>2. Browse</h3>
				<p>Browse all companies pitches</p>
				<ul>
					<li>Review companies &amp; their documents</li>
					<li>Message the management team</li>
					<li>Read The Right Crowd Executive Board's comments</li>
				</ul>
			</div>
			<div class="col-sm-4">
				<img src="img/lg_icon_invest.png" class="img-responsive">
				<h3>3. Invest</h3>
				<p>Ready to invest, simply click Invest Now</p>
				<ul>
					<li>Select the company and click on the Invest Now button.</li>
					<li>Specify the amount you want to invest</li>
					<li>Pay with your credit or debit card</li>
				</ul>
			</div>
		</div>
		<br><br>

		<div class="row">
			<div class="col-sm-4">
				<img src="img/lg_icon_calendar.png" class="img-responsive">
				<h3>4. Transfer Date</h3>
				<p>Once the company is fully funded and reaches the closing date, your funds will be transferred and you will receive notification of your investment.</p>
			</div>
			<div class="col-sm-4">
				<img src="img/lg_icon_certificates.png" class="img-responsive">
				<h3>5. Certificates</h3>
				<p>You will receive your shares certificates by post to your registered address.</p>
			</div>
			<div class="col-sm-4">
				<img src="img/lg_icon_complete.png" class="img-responsive">
				<h3>Congratulations</h3>
				<p>Congratulations on taking your future into your own hands and keep checking our offerings for more potential capital gain makers in the future.</p>
			</div>
		</div>
		<br><br>

		<div class="row">
			<div class="col-sm-12">
				<div class="content-updates-box">
					<img src="img/icon_newspaper.png">
					<h3>Receive Regular Updates</h3>
					<p>All company updates will be sent to you by email notification as will any notices of activity needed by yourself.</p>
				</div>
			</div>

		</div>

		<div class="content-join">
			<a href="{{ route('join-investor') }}">Get Started</a>
		</div>
	</div>
	<div class="col-sm-1"></div>
</div>

@stop
