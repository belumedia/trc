@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>The Rules</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">

		<p>This document sets out the rules (“Rules”) which apply all Offers on this Website.  By making an offer for investment on the Website, you will be asked to confirm your acceptance of the Rules.  If you do not accept the Rules you will not be permitted to submit a Bid. </p>
		<p>In addition to the Rules, the following documents remain in full force and effect and apply to any Bid that is made on the Website:</p>
		<ol type="A">
			<li>Members Terms and Conditions;</li>
			<li>Privacy Policy; and</li>
			<li>Investor Terms and Conditions.</li>
		</ol>
		<p>If there is any conflict between the Rules and any of the above-mentioned documents, the Rules will prevail.  If you have any questions in relation to anything set out in the Rules, please contact The Right Crowd Limited (“The Right Crowd”) on admin@therightcrowd.com.</p>
		
		<table class="table">
			<thead>
				<tr>
					<th class="col-sm-2">Key Terms</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Acceptance Notification</td>
					<td>notification from The Right Crowd/Investee Company to an Investor that his Bid has been accepted and is deemed to be an Accepted Bid (by email or text using the details provided by the Investor to The Right Crowd)</td>
				</tr>
				<tr>
					<td>Accepted Bid</td>
					<td>a Bid which has been accepted by the Investee Company</td>
				</tr>	
				<tr>
					<td>Bid Amount</td>
					<td>the amount which an Investor has Bid</td>
				</tr>	
				<tr>
					<td>Bid Period</td>
					<td>the period in which a Bid can be submitted, as set out on the Website,</td>
				</tr>	
				<tr>
					<td>Bid</td>
					<td>a valid offer for Shares submitted by an Investor on the Website</td>
				</tr>	
				<tr>
					<td>Default Bid</td>
					<td>an Accepted Bid for which payment has become suspended, delayed, charged back or fails.</td>
				</tr>	
				<tr>
					<td>Investee Company</td>
					<td>the company which is offering Shares on the Website</td>
				</tr>	
				<tr>
					<td>Investor</td>
					<td>an investor member who wishes to purchase Shares </td>
				</tr>	
				<tr>
					<td>Maximum Bid Amount</td>
					<td>the maximum amount an Investor can Bid (as set out on the Website)</td>
				</tr>	
				<tr>
					<td>Maximum Cash Target</td>
					<td>the maximum amount the Investee Company may accept in respect of an Offer (as set out on the Website) and any Overflow</td>
				</tr>
				<tr>
					<td>Minimum Bid Amount</td>
					<td>the minimum amount an Investor can Bid</td>
				</tr>
				<tr>
					<td>Minimum Cash Target</td>
					<td>the minimum amount the Investee Company must receive for the Offer to remain open (as set out on the Website)</td>
				</tr>
				<tr>
					<td>Offer Cash Target</td>
					<td>the amount the Investee Company wishes to receive in consideration for the sale of its Shares to Investors (as set out on the Website)</td>
				</tr>
				<tr>
					<td>Offer End Date</td>
					<td>the date on which the Bid Period expires, as set out on the Website</td>
				</tr>
				<tr>
					<td>Offer</td>
					<td>an offer by the Investee Company to Investors for the sale of Shares to the Investors at an agreed subscription price per Share</td>
				</tr>
				<tr>
					<td>Overflow</td>
					<td>the amount of bids that the Investee Company is willing to accept in respect of the Offer notwithstanding that such bids may be over the maximum cash target set out on the Website, as agreed between the Investee Company and The Right Crowd.</td>
				</tr>
				<tr>
					<td>Payment Company</td>
					<td>the relevant third party payment provider chosen and used by the investor in accordance with the payment instructions on the Website</td>
				</tr>
				<tr>
					<td>Provisional Bid</td>
					<td>a Bid which is pending but has not been accepted by the Investee Company and has not been withdrawn by the Investor</td>
				</tr>
				<tr>
					<td>Shares</td>
					<td>such shares in the capital of the Investee Company which are available for Investors to purchase</td>
				</tr>
				<tr>
					<td>Subscription Amount</td>
					<td>the amount which an Investor wishes to invest in the Investee Company in consideration for which he will receive Shares </td>
				</tr>
				<tr>
					<td>Website</td>
					<td>the website accessible via http://www.therightcrowd.com</td>
				</tr>
				<tr>
					<td>Withdrawn Bid</td>
					<td>a Bid which is withdrawn by the Investor</td>
				</tr>
				<tr>
					<td>Withdrawn Bid Notification</td>
					<td>notification from The Right Crowd/Investee Company to an Investor that confirming that his Bid has been withdrawn</td>
				</tr>
						
			</tbody>
		</table>
		
		<h2>1. The Process</h2>
		<p>1.1 Once registered as an Investor an individual can submit a Bid in respect of any Investee Company which has an open Offer. Please see paragraph 2 below for the rules relating to how to submit a Bid.</p>
		<p>1.2 The Investee Company will set the parameters of the Offer within which Bids can be submitted.  Those details will be set out clearly on the Website in relation to that Offer and are non-negotiable.  The following information, which all Investors will need to be aware of before submitting a Bid, will be set out on the Website: 
			<ul>
				<li>Offer Cash Target</li>
				<li>Minimum Cash Target </li>
				<li>Maximum Cash Target</li>
				<li>Minimum Bid Amount</li>
				<li>Maximum Bid Amount</li>
			</ul>
		</p>
		<p>1.3 The Offer will close on the earlier of:
			<ol type="A">
				<li>the Maximum Cash Target being reached;</li>
				<li>the Offer End Date.</li>
			</ol>
		</p>
		<p>1.4 The Right Crowd may, after consultation with the Investee Company, elect to close the Offer at any time its sole discretion.</p>
		<p>1.5 The Right Crowd may, after consultation with the Investee Company, increase the  Maximum Cash Target at any time its sole discretion.</p>
		<p>1.6 Investors will not be able to submit any further Bids in respect of an Offer that has closed. </p>
		
		<h2>Bid Period</h2>
		<p>1.7 Bids can be submitted at any time during the Bid Period.</p>
		<p>1.8 If at the end of the Bid Period the aggregate value of all Bids are not at least equal to the Minimum Cash Target the Offer will automatically close.  Investors will not be able to submit any further Bids after the Offer has closed and any Bids which were made prior to the Offer closing will be cancelled and the Investor notified. </p>
		<p>1.9 All investors that have submitted a Bid will be notified by way of an Acceptance Notification if their Bid has been accepted within 48 hours of the closing of the Bid Period.  Please see paragraph 3 below for the rules relating to acceptance of Bids. </p>
		<p>1.10 The Offer will automatically close on the Offer End Date, provided the Maximum Cash Target has not been reached prior to that date. </p>
		<p>1.11 The Investee Company will, at its sole discretion, have the opportunity to extend the Offer End Date in the event that the Maximum Cash Target has not been achieved (the “Extension Period”).  Provided that there are Provisional Bids which have not yet been accepted by the Investee Company it may contact the Investors which have a Provisional Bid and request that they withdraw their Bid if they no longer wish to invest.  All Provisional Bids which remain on the Website after a period of 48 hours following notification from the Investee Company will be available for acceptance.  </p>
		<p>1.12 Within 48 hours of the Offer closing (or after any Extended Period), provided the Minimum Cash Target has been reached, all Investors who have made an Accepted Bid will receive a notification from The Right Crowd confirming the amount raised by the Investee Company and confirmation of the number of Shares to be allocated to the Investor.  Share certificates will be issued to the relevant Investors shortly thereafter and the Investee Company’s register of members and allotments will be updated accordingly.</p>
		<p>1.13 Any surplus cash received by the Payment Company in respect of a Bid will be returned to the relevant Investor as soon as reasonably practicable after the Offer has closed.  Neither the Payment Company, The Right Crowd nor the Investee Company will be liable to account for or pay to the Investor any interest which may accrue on any such surplus cash.</p>
		
		<h2>2. How to submit, change and withdraw a Bid</h2>
		<p>2.1 A Bid can only be submitted by individuals who are registered as Investors on the Website and whose account on the Website has not been suspended or terminated by The Right Crowd.</p>
		<p>2.2 Investors can submit a Bid at any time whilst the Offer remains open.  </p>
		<p>2.3 Bids are submitted by completing the online subscription form relevant to the Offer in which the Investor wishes to invest. </p>
		<p>2.4 The online subscription form will set out the number of Shares that the Investor will receive and the subscription price payable if his Bid is successful.  The number of Shares will be dependent on the price per Share (set by the Investee Company) and the amount invested. </p>
		<p>2.5 A Bid will only be valid and capable of being accepted if:
			<ol type="A">
				<li>it is made for at least the Minimum Bid Amount and does not exceed the Maximum Bid Amount; and</li>
				<li>the Investor has paid to the Payment Company an amount equal Subscription Amount, in accordance with the payment instructions on the Website.</li>
			</ol>
		</p>
		<p>2.6 A Bid cannot be changed once it has been submitted.  A Bid can be withdrawn at any time up to 24 hours after the Bid Period closes. </p>
		<p>2.7 An Investor will receive email confirmation of receipt of a valid Bid.  Confirmation of receipt is not evidence that the Bid has been accepted by the Investee Company. </p>
		
		<h2>Changing a Bid</h2>
		<p>2.8 A Bid cannot be changed once it has been submitted.</p>
		<p>2.9 If an Investor wishes to increase the amount he is to invest, he must submit another Bid for the additional amount which he wishes to invest.  The latter Bid will rank below the Investor’s first Bid and any other Bids which have been submitted by other Investors in the interim period.  Any additional Bid must be for an amount at least equal to the Minimum Bid Amount.  All Bids submitted by the same Investor must not exceed the Maximum Bid Amount. </p>
		<p>2.10 If an Investor wishes to increase the amount he is to invest by less than the Minimum Bid Amount, he will need to withdraw any Bid which has already been submitted and submit a new Bid for the revised amount.  Any new Bid must be for an amount at least equal to the Minimum Bid Amount and cannot exceed the Maximum Bid Amount.  The new Bid will rank according to the time at which it is submitted and not equal to the initial Bid (i.e. the initial Bid will lose its rank as soon as it is withdrawn). </p>
		<p>2.11 If an Investor wishes to decrease the amount of his Bid, he must withdraw the Bid and submit a new Bid for the revised investment amount.  The revised Bid must be for an amount at least equal to the Minimum Bid Amount and must not exceed the Maximum Bid Amount.  The revised Bid will rank according to the time at which it is submitted and the Withdrawn Bid will lose its ranking position. </p>
		<p>2.12 A Bid may not be withdrawn once it has been accepted by the Investee Company.</p>
		<p>2.13 An Investor may withdraw a Bid at any time during the Bid Period up to 24 hours after the expiry of the Bid Period.</p>
		<p>2.14 A Bid can only be withdrawn by following the instructions on the Website.</p>
		<p>2.15 Once withdrawn the Bid will be deemed to be a Withdrawn Bid. </p>
		<p>2.16 An Investor will receive confirmation of the Withdrawn Bid by email (Withdrawn Bid Confirmation).  If the Investor does not receive such email confirmation within a reasonable time, he must contact The Right Crowd. </p>
		<p>2.17 An Investor shall receive a refund of the amount paid to the Payment Company in respect of the Withdrawn Bid within 7 days of receiving the Withdrawn Bid Confirmation.</p>
		
		<h2>3. Acceptance of a Bid</h2>
		<p>3.1 The Investee Company reserves the right to accept or reject any Bid.</p>
		<p>3.2 Bids will be accepted by the Investee Company in the order in which they are submitted and will rank in such order.</p>
		<p>3.3 If the aggregate value of Accepted Bids exceeds the Maximum Cash Target, any Bids which are made thereafter will not be accepted and will remain Provisional Bids.  In the event that an Accepted Bid becomes a Default Bid, a Provisional Bid may be accepted (in accordance with the order in which the Bids are submitted to the Website). </p>
		<p>3.4 If accepting a Bid would result in the Maximum Cash Target being exceeded by less than 10% of Offer Cash Target (or a higher figure agreed between the Investee Company and The Right Crowd) the Investee Company may, at its sole discretion, accept the Bid.</p>
		<p>3.5 If accepting a Bid would result in the Maximum Cash Target being exceeded by more than 10% of Offer Cash Target (or a higher figure agreed between the Investee Company and The Right Crowd), the Investee Company may, at its sole discretion, accept the proportion of the Bid which would result in the Maximum Cash Target Amount being exceeded by less than 10% of Offer Cash Target (or such higher figure agreed between the Investee Company and The Right Crowd).</p>
		<p>3.6 All Investors will receive a notification within 48 hours of the Offer closing confirming whether or not their Bid has been accepted. If the Investor’s Bid has
			<ol type="A">
				<li>been accepted:
					<ol type="I">
						<li>the Bid will be deemed to be an Accepted Bid; and</li>
						<li>the Investor hereby authorises the Payment Company to immediately release the Subscription Amount to the Investee Company; and</li>
					</ol>
				</li>	
				<li>not been accepted, no further action from the Investor will be required and the Investor shall receive a refund of the amount paid to the Payment Company in respect of such Bid within 7 days of receiving such notification.</li>
			</ol>
		</p>
		<p>3.7 If, as a result of unforeseen bank charges or exchange rate issues, the amount paid by the Investor in respect of an Accepted Bid is less than the Subscription Amount required (by up to no more than 5% of the Subscription Amount), the Investee Company may (at is sole discretion) choose to accept the amount paid by the Investor and amend the number of Shares to be allotted to the Investor accordingly. </p>
		
		<h2>4 Default Bids</h2>
		
		<p>4.1 If for any reason the:
			<ol type="A">
				<li>payment from the Investor to the Payment Company; or</li>
				<li>payment from the Payment Company on behalf of the Investor to the Investee Company,</li>
			</ol>
			becomes suspended, delayed, charged back or fails after a Bid has been accepted, the Bid to which such payment relates will be deemed to be a Default Bid. 
		</p>
		<p>4.2 Where a Default Bid has occurred, The Right Crowd reserves the right to:
			<ol type="A">
				<li>suspend the relevant Investor’s account;</li>
				<li>investigate the circumstances in which the Default Bid occurred;</li>
				<li>close such Investor’s account on the Website will be closed; and </li>
				<li>prohibit such Investor from becoming a member of the Website at any time in the future. </li>
			</ol>
		</p>
		<p>4.3 The Investee Company reserves the right to pursue any Investor for any losses it may suffer as a result of a Default Bid.  </p>
					
	</div>
	<div class="col-sm-1"></div>
</div>

@stop