@extends('layouts.investors')

@section('content')

<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>Terms &amp; Conditions</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">
		
	<p>
                    Please read thoroughly all of the terms and conditions, legal documents and warnings governing your
                    relationship with The Right Crowd platform.
                </p>

                <h3>General Conditions</h3>
                <ul>
                    <li><a href="{{ route('website-terms') }}">Website Terms and Conditions</a></li>
                    <li><a href="{{ route('privacy-policy') }}" >Privacy Policy</a></li>
                    <li><a href="{{ route('membership-terms') }}">Member Terms and Conditions</a></li>
                </ul>

                <h3>Investor Conditions</h3>
                <ul>
                    <li><a href="{{ route('investor-terms') }}">Investor Membership Terms and Conditions.</a></li>
                    <li><a href="{{ route('investment-rules') }}">Offer Rules</a></li>
                    <li><a href="{{ route('risk-warning') }}">Risk Warning</a></li>
                </ul>


                <h3>Company Conditions</h3>
                <ul>
                    <li><a href="{{ route('company-terms') }}">Investee Company Terms and Conditions.</a></li>
                </ul>

                <h3>Special Offers</h3>
                <ul>
                    <li><a href="{{ route('special-offer-terms') }}">Terms and Conditions for Special Offers (June 2015)</a></li>
                </ul>
	

	</div>
	<div class="col-sm-1"></div>
</div>

@stop