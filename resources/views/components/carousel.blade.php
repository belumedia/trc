<div class="row hidden-xs">
	<div id="carousel-front-page" class="carousel slide" data-ride="carousel">

	  <div class="carousel-inner" role="listbox">

	    <div class="item active slides slides-sample">
	      <div class="carousel-text carousel-text-black">
	    	  <h1>Share bonus<br>for the first 10,000 investors</h1>
		      <p>First 10,000 Investments receive 1% increase on their shareholding. Plus an additional 3% for the first 2,000 Investments of £500 or more.</p>
	      </div>
	      <img src="img/slide_3.jpg" class="slides-img" style="width:100%;">
	    </div>
	    <div class="item  slides slides-sample">
		   <div class="carousel-text">
			   <h1>3 Year Advisory Contract<br> for Startups</h1>
			   <p>Upon successful fund raising, companies will receive a 3 year Corporate Finance Advisory contract helping steer towards future growth.</p>
		   </div>
	      <img src="img/slide_1.jpg" class="slides-img" style="width:100%;">
	    </div>
	    <div class="item  slides slides-sample">
		    <div class="carousel-text carousel-text-black">
				<h1>Early Stage <br>Alternative Funding</h1>
				<p>We pride ourselves on looking for the best companies to bring to The Right Crowd, offering investors the perfect diversification opportunities.</p>
		    </div>
	      <img src="img/slide_2.jpg" class="slides-img" style="width:100%;">
	    </div>

	  </div>

	  <a class="left carousel-control" href="#carousel-front-page" role="button" data-slide="prev">
	    <img src="img/arrow_left.png" border="0" aria-hidden="true" class="carousel-buttons">
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="right carousel-control" href="#carousel-front-page" role="button" data-slide="next">
	    <img src="img/arrow_right.png" border="0" aria-hidden="true" class="carousel-buttons">
	    <span class="sr-only">Next</span>
	  </a>

	</div>
</div>
<script>
	if (window.jQuery) {
		$('.carousel-front-page').carousel({
			interval: 6,
		})
	}
</script>
