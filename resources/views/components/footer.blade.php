<div class="row footer-newsletter" style="background-color:#FFF;border:none;">
	<img src="{!! asset('img/UKCF_MainLogo-300x121-300x121.jpg') !!}">
</div>
<div class="row footer-newsletter">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
      <p>Investors should be aware that there are risks to investing in shares of companies, especially if they are private companies as there may be little or no market for the shares to be traded and dividends are unlikely to be paid. Investments may go down as well as up and therefore investors may not recover their initial investment.</p>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>
<div class="row footer-social-bar">
	<a href="https://twitter.com/TRCFunding" target="_blank"><img src="{!! asset('img/social_twitter.png') !!}"></a>
	<a href="https://www.linkedin.com/company/therightcrowd-com" target="_blank"><img src="{!! asset('img/social_linkedin.png') !!}"></a>
	<a href="https://www.facebook.com/pages/The-Right-Crowd/1005482409478208" target="_blank"><img src="{!! asset('img/social_facebook.png') !!}"></a>
</div>
<div class="row footer-menu">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-2 col-lg-2">
		<ul>
			<li><a href="{!! route('home') !!}">Investment</a></li>
			<li><a href="{!! route('investors') !!}">Investors</a></li>
			<li><a href="{!! route('opportunities') !!}">Opportunities</a></li>
			<li><a href="{{ route('join-investor') }}">Join</a></li>
			<li><a href="{{ route('signin-investor') }}">Sign-In</a></li>
		</ul>
	</div>
	<div class="col-sm-2 col-lg-2">
		<ul>
			<li><a href="{!! route('companies') !!}">Funding</a></li>
			<li><a href="{!! route('companies-get-started') !!}">Companies</a></li>
			<!-- <li><a href="{!! route('case-studies') !!}">Case Studies</a></li> -->
			<li><a href="{{ route('join-company') }}">Join</a></li>
			<li><a href="{{ route('signin-company') }}">Sign-In</a></li>
		</ul>
	</div>
	<div class="col-sm-2 col-lg-2">
		<ul>
			<li><a href="{!! route('about-us') !!}">About Us</a></li>
			<li><a href="{!! route('faqs') !!}">FAQ's</a></li>
			<li><a href="{!! route('terms-conditions') !!}">Terms &amp; Conditions</a></li>
			<li><a href="{!! route('privacy-policy') !!}">Privacy Policy</a></li>
			<li><a href="{!! route('membership-terms') !!}">Member Terms</a></li>
			<li><a href="{!! route('risk-warning') !!}">Risk Warning</a></li>
		</ul>
	</div>
	<div class="col-sm-2">
		The Right Crowd Ltd.<br>
		39 Steeple Close<br>
		Poole<br>
		Dorset<br>
		BH17 9BJ<br><br>
		0845 862 0646<br>
		info@therightcrowd.com
	</div>
	<div class="col-sm-1  col-lg-2"></div>
</div>
<div class="row footer-menu footer-copyright">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 text-center">
		&copy; Copyright The Right Crowd Ltd. 2015<br>
		The Right Crowd Limited is a limited company registered in England and Wales under company number 09398506 and whose registered office is at 39 Steeple Close, Poole, Dorset BH17 9BJ.
The Right Crowd Limited is an appointed representative of Equity For Growth Ltd which is authorised and regulated by the Financial Conduct Authority with registration number 475953.
Equity For Growth Ltd is a limited liability partnership registered in England and Wales under company number 05410446 and whose registered office is at 2nd Floor Suite, 30 Clarendon Road, Watford, Hertfordshire, WD17 1JJ.
Investors should be aware that there are risks in investing in shares. Investments may go down as well as up and investors may therefore not recover their original investment. Please read our risk summary, which can be found <a href="{{ route('risk-warning') }}">here</a> before proceeding with any investments on The Right Crowd crowdfunding platform.
	</div>
</div>

</div><!-- End of Container Fluid in Header -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="{!! asset('js/script.min.js'); !!}"></script>
    @yield('footer-scripts')
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-50831514-4', 'auto');
	  ga('send', 'pageview');
	</script>

</body>
</html>
