@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		
		<h1>{{ $investor->first_name }} {{ $investor->last_name }} <span class="label label-default pull-right">
			<?php $investor->get_user_investment_status(); ?>
			@if($investor->investor_type_text) {{ $investor->investor_type_text }} @else Not Signed In @endif
		</span></h1>
		<p><strong>Tel:</strong> {{ $investor->telephone }} <strong>Email:</strong> {{ $investor->email }} </p>
		<p>{{ $investor->street }}, {{ $investor->street_2 }}, {{ $investor->city }}, {{ $investor->county }}, {{ $investor->postcode }}</p>
	</div>	
	<div class="col-sm-1 col-lg-2"></div>
</div>




<div class="row content-section content-section-spacer-base">
	<br><br>
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		
		
		<h2>Investments</h2>
		
			<table class="table table-striped">
			<thead>
				<tr>
					<th>Invested In</th>
					<th class="col-sm-3">Status</th>
					<th class="col-sm-3">Amount</th>
					<th class="col-sm-1"></th>
					<th class="col-sm-1"></th>
				</tr>
			</thead>
			<tbody>
				@if($investor->count_investments()>0)
				@foreach($investor->get_investments() as $investment)
					{!! $investment->get_investment_status() !!}
					<tr>
						<td>{{ $investment->startups->full_business_name }}</td>
						<td>{{ $investment->status }}</td>
						<td>{{ $investment->display_formatted_amount() }}</td>
						@if($investment->held==1)
							<td><a href="{{ route('sipp.update-investment', ['id' => $investment->id, 'status' => 2]) }}" class="btn btn-xs btn-primary">Transfer Funds</a></td>
							<td><a href="{{ route('sipp.update-investment', ['id' => $investment->id, 'status' => -2]) }}" class="btn btn-xs btn-danger">Decline Investment</a></td>
						@elseif($investment->transfered==1)
							<td><a href="{{ route('sipp.update-investment', ['id' => $investment->id, 'status' => -1]) }}" class="btn btn-xs btn-primary">Reverse Funds</a></td>
							<td><a href="{{ route('sipp.update-investment', ['id' => $investment->id, 'status' => -2]) }}" class="btn btn-xs btn-danger">Decline Investment</a></td>						
						@else
							<td><a href="{{ route('sipp.update-investment', ['id' => $investment->id, 'status' => 1]) }}" class="btn btn-xs btn-primary">Hold Funds</a></td>
							<td><a href="{{ route('sipp.update-investment', ['id' => $investment->id, 'status' => -2]) }}" class="btn btn-xs btn-danger">Decline Investment</a></td>
						@endif
						</td>
					</tr>
				@endforeach
				@else
					<tr>
						<td colspan="4" class="text-center"><h5>Investor hasn't made any investments.</h5></td>
					</tr>				
				@endif
			</tbody>
		</table>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection