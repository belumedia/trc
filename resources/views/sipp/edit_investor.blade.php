@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>Edit Investor</h1>
	</div>	
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<br><br>
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">

	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

	
	{!! Form::model($investor, ['route' => ['sipp.update-investor', $investor->id], 'method' => 'POST']) !!}

	<h4 class="body-form-top-margin">Investor Name</h4>

	<div class='form-group'>
		<label for='first_name'>First Name</label>
		{!! Form::text('first_name', null, ['class' => 'form-control']) !!}
	</div>
	<div class='form-group'>
		<label for='last_name'>Last Name</label>
		{!! Form::text('last_name', null, ['class' => 'form-control']) !!}
	</div>

	<h4 class="body-form-top-margin">Contact Details</h4>

	<div class='form-group'>
		<label for='email'>Email</label>
		{!! Form::text('email', null, ['class' => 'form-control']) !!}
	</div>
	<div class='form-group'>
		<label for='telephone'>Telephone</label>
		{!! Form::text('telephone', null, ['class' => 'form-control']) !!}
	</div>
	
	<h4 class="body-form-top-margin">Password</h4>
	<p>Please use this field if you would like to update the investors password otherwise leave it blank.</p>
	<div class='form-group'>
		{!! Form::text('new_password', null, ['class' => 'form-control']) !!}
	</div>
	
	
	<h4 class="body-form-top-margin">Address</h4>
	
	<div class='form-group'>
		<label for='street'>Street</label>
		{!! Form::text('street', null, ['class' => 'form-control']) !!}
	</div>
	<div class='form-group'>
		{!! Form::text('street_2', null, ['class' => 'form-control']) !!}
	</div>
	<div class='form-group'>
		<label for='city'>City</label>
		{!! Form::text('city', null, ['class' => 'form-control']) !!}
	</div>
	<div class='form-group'>
		<label for='county'>County</label>
		{!! Form::text('county', null, ['class' => 'form-control']) !!}
	</div>
	<div class='form-group'>
		<label for='postcode'>Postcode</label>
		{!! Form::text('postcode', null, ['class' => 'form-control']) !!}
	</div>

	
	<input type='submit' value='Update Investor' class='btn btn-primary'>
	{!! Form::hidden('id', null) !!}
	
	{!! Form::close() !!}
		


	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection