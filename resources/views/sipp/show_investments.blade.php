@extends('layouts.members')

@section('content')



<div class="row content-section content-section-spacer-base">
	<br><br>
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		
		
		<h2>Investments</h2>
		
			<table class="table table-striped">
			<thead>
				<tr>
					<th>Invested In</th>
					<th class="col-sm-3">Investor</th>
					<th class="col-sm-3">Status</th>
					<th class="col-sm-3">Amount</th>
				</tr>
			</thead>
			<tbody>
				@if(count($investments)>0)
				@foreach($investments as $investment)
					{!! $investment->get_investment_status() !!}
					<tr>
						<td>{{ $investment->startups->full_business_name }}</td>
						<td>{{ $investment->investor->first_name }} {{ $investment->investor->last_name }}</td>
						<td>{{ $investment->status }}</td>
						<td>{{ $investment->display_formatted_amount() }}</td>
						<td><a href="{{ route('sipp.view-investor', ['id' => $investment->investor->id]) }}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-eye-open"></span> View</a></td>
					</tr>
				@endforeach
				@else
					<tr>
						<td colspan="4" class="text-center"><h5>Your members have not made any investments.</h5></td>
					</tr>				
				@endif
			</tbody>
		</table>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection