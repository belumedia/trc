@extends('layouts.members')

@section('content')

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>Your Investors</h1>
		<a href="{{ route('sipp.add-investor') }}" class="btn btn-md btn-primary pull-right">Add Investor</a>
	</div>	
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<br><br>
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		
		@if(Session::has('message'))
			<br>
			<div class="alert alert-success" role="alert">{!! Session::get('message'); !!}</div>
		@endif
		
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Date Added</th>
					<th class="col-sm-2">Full Name</th>
					<th>Email</th>
					<th class="col-sm-2">Investor Status</th>
					<th class="col-sm-1">Investments</th>
					<th class="col-sm-1">Edit</th>
					<th class="col-sm-1">View</th>
				</tr>
			</thead>
			<tbody>
				@if(count($investors)>0)
				@foreach($investors as $investor)
					<tr>
						<td>{{ $investor->created_date_object()->format('jS M y') }}
						<td>{{ $investor->first_name }} {{ $investor->last_name }}</td>
						<td>{{ $investor->email }}</td>
						<td>{{ $investor->get_investment_status() }}</td>
						<td>{{ $investor->count_investments() }}</td>
						<td><a href="{{ route('sipp.edit-investor', ['id' => $investor->id]) }}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span> Edit</a></td>
						<td><a href="{{ route('sipp.view-investor', ['id' => $investor->id]) }}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-eye-open"></span> View</a></td>
					</tr>
				@endforeach
				@else
					<tr>
						<td colspan="8" class="text-center"><h5>You don't currently have any investors.</h5></td>
					</tr>				
				@endif
			</tbody>
		</table>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

@endsection