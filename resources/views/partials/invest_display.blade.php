<?php $start_up->total_investment(); ?>
<div class="row content-company-profile">
	
	<div class="col-sm-4">
		<a href="{{ route('member-company-view', ['id' => $start_up->id]) }}">
		@if(!empty($start_up->logo_file))
			<img src="{!! asset('img/company_photo_holder.png'); !!}" class="content-company-profile-img" style="background-image: url({!! asset('photo_uploads/resized_'.$start_up->logo_file.'.jpg'); !!});"> 
		@else
			<img src="{!! asset('img/no_photo_startup.jpg'); !!}" class="content-company-profile-img">
		@endif
		</a>
	</div>
	<div class="col-sm-8">
		<h2>{{ $start_up->full_business_name }}</h2>
		<div style="height: 100px; overflow: scroll;">
			<p>{{ $start_up->business_summuary }}</p>
		</div>
		<div class="row content-company-profile-financial-row">
			@if($start_up->invested_total_integer<1000)
				<div class="col-lg-12">
					<lead>Just Launched</lead><br>
					<small>This startup has just been added to The Right Crowd</small>
				</div>
			@else
				<div class="col-lg-4">
					<lead>{{ $start_up->display_investment_target() }}</lead><br>
					<small>TARGET</small>
				</div>
				<div class="col-lg-4">
					<lead>{{ $start_up->invested_total_string }}</lead><br>
					<small>INVESTED</small>
				</div>
				<div class="col-lg-4">
					<lead>{{ $start_up->invested_percentage }}</lead><br>
					<small>FUNDED</small>
				</div>
			@endif
			
		</div>	
		<br>
		<div class="row">
			<div class="col-sm-12">
				<a href="{{ route('member-company-view', ['id' => $start_up->id]) }}" class="btn btn-primary">View Company</a>
			</div>
		</div>
		<br>
	</div>
</div>