<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<div class="content-company-profile">
               <img src="img/sample_company_image.jpg" class="content-company-profile-img"><br>
               <img src="img/funded_flag.png" class="content-funded-flag"><br><br>
               <p><strong>Notbox UK Ltd.</strong></p>
               <p>These are very exciting times for both businesses and investors alike.  Historically private investors could only </p>
               <div class="row">
                  <div class="col-xs-12">
                     <hr>
                     <a href="{{ url('auth/register') }}" class="pull-right btn btn-primary" >Get Started <span class="glyphicon glyphicon-chevron-right"></span></a>
                     <a href="{{ route('case-studies-case-1') }}" class="btn btn-default" >View Case Study</a>
                     <br><br>
                  </div>
               </div>
            </div>
			</div>
			<div class="col-sm-4">
				<div class="content-company-profile">
               <img src="img/sample_company_image.jpg" class="content-company-profile-img"><br>
               <img src="img/funded_flag.png" class="content-funded-flag"><br><br>
               <p><strong>Ticket HQ Ltd.</strong></p>
               <p>These are very exciting times for both businesses and investors alike.  Historically private investors could only </p>
               <div class="row">
                  <div class="col-xs-12">
                     <hr>
                     <a href="{{ url('auth/register') }}" class="pull-right btn btn-primary" >Get Started <span class="glyphicon glyphicon-chevron-right"></span></a>
                     <a href="{{ route('case-studies-case-2') }}" class="btn btn-default" >View Case Study</a>
                     <br><br>
                  </div>
               </div>
            </div>
			</div>	
			<div class="col-sm-4">
				<div class="content-company-profile">
               <img src="img/sample_company_image.jpg" class="content-company-profile-img"><br>
               <img src="img/funded_flag.png" class="content-funded-flag"><br><br>
               <p><strong>Notbox UK Ltd.</strong></p>
               <p>These are very exciting times for both businesses and investors alike.  Historically private investors could only </p>
               <div class="row">
                  <div class="col-xs-12">
                     <hr>
                     <a href="{{ url('auth/register') }}" class="pull-right btn btn-primary" >Get Started <span class="glyphicon glyphicon-chevron-right"></span></a>
                     <a href="{{ route('case-studies-case-3') }}" class="btn btn-default" >View Case Study</a>
                     <br><br>
                  </div>
               </div>
            </div>
			</div>
		</div>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>