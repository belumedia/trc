<ul class="list-group">
  @if($active=="")
  	<li class="list-group-item"><strong>Getting Started</strong>  <span class="glyphicon glyphicon-chevron-right pull-right"></span></li>
  @else
  	<li class="list-group-item"><a href="{{ route('member-edit-startup-start', ['id' => $start_up->id]) }}">Getting Started</a> </li>
  @endif
  @if($active=="company")
  	<li class="list-group-item"><strong>Company Details</strong>  <span class="glyphicon glyphicon-chevron-right pull-right"></span></li>
  @else
  	<li class="list-group-item"><a href="{{ route('member-edit-startup-company', ['id' => $start_up->id]) }}">Company Details</a></li>
  @endif
  @if($active=="business")
	<li class="list-group-item"><strong>Business Plan</strong>  <span class="glyphicon glyphicon-chevron-right pull-right"></span></li>
  @else
	<li class="list-group-item"><a href="{{ route('member-edit-startup-business', ['id' => $start_up->id]) }}">Business Plan</a></li>
  @endif
  @if($active=="market")
	<li class="list-group-item"><strong>Market Research</strong>  <span class="glyphicon glyphicon-chevron-right pull-right"></span></li>
  @else
  	<li class="list-group-item"><a href="{{ route('member-edit-startup-market', ['id' => $start_up->id]) }}">Market Research</a></li>
  @endif
  @if($active=="team")
  	<li class="list-group-item"><strong>Executive Team</strong>  <span class="glyphicon glyphicon-chevron-right pull-right"></span></li>
  @else
	<li class="list-group-item"><a href="{{ route('member-edit-startup-team', ['id' => $start_up->id]) }}">Executive Team</a></li>
  @endif
  @if($active=="documents")
	<li class="list-group-item"><strong>Document Upload</strong>  <span class="glyphicon glyphicon-chevron-right pull-right"></span></li>
  @else
    <li class="list-group-item"><a href="{{ route('member-edit-startup-documents', ['id' => $start_up->id]) }}">Document Upload</a></li>
  @endif
  <!-- <li class="list-group-item"><a href="{{ route('member-edit-startup-business', ['id' => $start_up->id]) }}">NDA</a></li> -->
  @if($active=="logo")
	<li class="list-group-item"><strong>Logo</strong>  <span class="glyphicon glyphicon-chevron-right pull-right"></span></li>
  @else
	  <li class="list-group-item"><a href="{{ route('member-edit-startup-logo', ['id' => $start_up->id]) }}">Logo</a></li>
  @endif
  @if($active=="videos")
	<li class="list-group-item"><strong>Videos</strong>  <span class="glyphicon glyphicon-chevron-right pull-right"></span></li>
  @else
	  <li class="list-group-item"><a href="{{ route('member-edit-startup-videos', ['id' => $start_up->id]) }}">Videos</a></li>
  @endif
  @if($active=="seis")
	<li class="list-group-item"><strong>SEIS + EIS</strong>  <span class="glyphicon glyphicon-chevron-right pull-right"></span></li>
  @else
	  <li class="list-group-item"><a href="{{ route('member-edit-startup-seis', ['id' => $start_up->id]) }}">SEIS + EIS</a></li>
  @endif
  @if($active=="bank")
	<li class="list-group-item"><strong>Payments</strong>  <span class="glyphicon glyphicon-chevron-right pull-right"></span></li>
  @else
	<li class="list-group-item"><a href="{{ route('member-edit-startup-bank', ['id' => $start_up->id]) }}">Payments</a></li>
  @endif
</ul>

