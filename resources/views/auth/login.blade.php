<?php
$layout = Session::get('signin-layout');

if(!empty($layout)){
	 $layout = "layouts.".$layout;
} else {
	$layout = "layouts.investors";
}
?>

@extends($layout)

@section('content')
<div class="row content-section content-section-spacer-base">

	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8 content-page">

		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-3 text-right">
				<h2>Sign In</h2>
				<p class="text-muted">We help businesses and entrepreneurs find the investors they need, and make sure they are ready to accept investment in compliance with all relevant regulations.</p>
			</div>

			<div class="col-sm-6">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<br>
					<form role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">



						<div class="form-group">
							<label class="control-label">E-Mail Address</label>
							<input type="email" class="form-control" name="email" value="{{ old('email') }}">
						</div>

						<div class="form-group">
							<label class="control-label">Password</label>
							<input type="password" class="form-control" name="password">
						</div>


						<div class="form-group">

								<button type="submit" class="btn btn-primary" style="margin-right: 15px;">
									Sign In
								</button>

								<a href="{{ url('/password/email') }}">Forgot Your Password?</a>

						</div>
					</form>

			</div>

		</div>

	</div>
	<div class="col-sm-1"></div>
</div>
@endsection
