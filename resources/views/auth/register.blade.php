<?php
$layout = Session::get('join-layout');

if(!empty($layout)){
	 $layout = "layouts.".$layout;
} else {
	$layout = "layouts.investors";
}
?>

@extends($layout)


@section('content')
<div class="row content-section content-section-spacer-base">

	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8 content-page">

		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-4 text-right">
				<h2>Join</h2>
				<p class="text-muted">We help businesses and entrepreneurs find the investors they need, and make sure they are ready to accept investment in compliance with all relevant regulations.</p>
				<p><strong>Already have an account? <a href="{{ url('auth/login') }}">Sign In</a></strong></p>
				<p class="text-muted">To view and invest in companies you need to be a member of The Right Crowd.</p>
			</div>

			<div class="col-sm-6">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							Please check the following errors:<br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form  role="form" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<h4 class="body-form-top-margin">Your Name</h4>

						<div class="form-group">
							<label class="control-label">First Name</label>
							<input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
						</div>

						<div class="form-group">
							<label class="control-label">Last Name</label>
							<input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
						</div>



						<h4>Your Address</h4>

						<div class="form-group">
							<label class="control-label">Street</label>
							<input type="text" class="form-control" name="street" value="{{ old('street') }}">
						</div>

						<div class="form-group">
							<input type="text" class="form-control" name="street_2" value="{{ old('street_2') }}">
						</div>

						<div class="form-group">
							<label class="control-label">City / Town</label>
							<input type="text" class="form-control" name="city" value="{{ old('city') }}">
						</div>

						<div class="form-group">
							<label class="control-label">County</label>
							<input type="text" class="form-control" name="county" value="{{ old('county') }}">
						</div>

						<div class="form-group">
							<label class="control-label">Postcode</label>
							<input type="text" class="form-control" name="postcode" value="{{ old('postcode') }}">
						</div>



						<h4>Contact Information</h4>

						<div class="form-group">
							<label class="control-label">Telephone</label>
							<input type="text" class="form-control" name="telephone" value="{{ old('telephone') }}">
						</div>


						<div class="form-group">
							<label class="control-label">Email Address</label>
							<input type="email" class="form-control" name="email" value="{{ old('email') }}">
						</div>

						<div class="form-group">
							<label class="control-label">Confirm Email</label>
							<input type="email" class="form-control" name="email_confirmation" value="{{ old('email_confirmation') }}">
						</div>


						<h4>Password</h4>

						<div class="form-group">
							<label class="control-label">Password</label>
							<input type="password" class="form-control" name="password">
						</div>

						<div class="form-group">
							<label class="control-label">Confirm Password</label>
							<input type="password" class="form-control" name="password_confirmation">
						</div>

						<h4>Bonus Code</h4>

						<div class="form-group" style="background-color:#ABBE49;padding:10px;border-radius: 5px;">
							<label class="control-label" style="color:#FFF;">Bonus Code</label>
							<input type="text" class="form-control" name="bonus" value="{{ old('bonus') }}" style="margin-bottom:5px;">
						</div>

						<hr>

						<div class="checkbox">
                        	<label>
                        		{!! Form::checkbox('accept_terms', '1'); !!} <strong>Yes,</strong> I accept your <a href="{{ route('membership-terms') }}" target="_blank">Member Terms &amp; Conditions.</a></a>
                        	</label>
                    	</div>

                    	<hr>

						<div class="form-group">
							<button type="submit" class="btn btn-primary">
								Register
							</button>
						</div>
					</form>

			</div>
		</div>

	</div>
	<div class="col-sm-1"></div>
</div>

@endsection
