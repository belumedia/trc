<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAccountStripeUpdate extends Migration {

   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up() {
      Schema::table('users', function($table) {
         $table->boolean('stripe_connected')->default(false);
         $table->string('stripe_access_token')->nullable();
         $table->string('stripe_publishable_key')->nullable();
         $table->string('stripe_user_id')->nullable();
         $table->string('stripe_scope')->nullable();
         
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down() {
      Schema::table('users', function($table) {
         $table->dropColumn('stripe_connected');
         $table->dropColumn('stripe_access_token');
         $table->dropColumn('stripe_publishable_key');
         $table->dropColumn('stripe_user_id');
         $table->dropColumn('stripe_scope');
      });
   }

}
