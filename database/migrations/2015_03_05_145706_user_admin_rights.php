<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAdminRights extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
		    $table->boolean('allow_admin')->default(false);
		    $table->boolean('allow_admin_review')->default(false);
		    $table->boolean('allow_admin_options')->default(false);
			$table->boolean('allow_admin_users')->default(false);
			$table->boolean('allow_admin_investors')->default(false);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($table)
		{
		    $table->dropColumn('allow_admin');
		    $table->dropColumn('allow_admin_review');
		    $table->dropColumn('allow_admin_options');
		    $table->dropColumn('allow_admin_users');
		    $table->dropColumn('allow_admin_investors');
		    
		});
	}

}
