<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateToDocuments extends Migration {

   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up() {
      Schema::table('documents', function($table) {
         $table->string('file_type');
         $table->integer('file_size');
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down() {
      Schema::table('documents', function($table) {
         $table->dropColumn('file_type');
         $table->dropColumn('file_size');
      });
   }

}
