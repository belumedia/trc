<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserInvestorSettings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
		    $table->boolean('is_high_networth')->default(false);
		    $table->boolean('is_professional')->default(false);
		    $table->boolean('is_restricted')->default(false);
			$table->boolean('accept_terms_high_networth')->default(false);
			$table->boolean('accept_terms_professional')->default(false);
			$table->boolean('accept_terms_restricted')->default(false);
			$table->boolean('accept_general_terms')->default(false);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($table)
		{
		    $table->dropColumn('is_high_networth');
		    $table->dropColumn('is_professional');
		    $table->dropColumn('is_restricted');
		    $table->dropColumn('accept_terms_high_networth');
		    $table->dropColumn('accept_terms_professional');
		    $table->dropColumn('accept_terms_restricted');
		    $table->dropColumn('accept_general_terms');
		    
		});
	}

}
