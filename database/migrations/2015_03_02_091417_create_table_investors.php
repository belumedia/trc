<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInvestors extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_investor_type', function($table)
		{
			$table->increments('id');
		    $table->integer('user_id');
		    $table->boolean('self_certify_high_net_worth')->nullable();
			$table->boolean('self_certify_sophisticated_investor')->nullable();
		    $table->boolean('self_certify_restricted_investor')->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_investor_type');
	}

}
