<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team_members', function(Blueprint $table)
		{
			$table->string('id');
			$table->string('start_up_id');
			$table->timestamps();
			$table->string('name');
			$table->string('position')->nullable();
			$table->string('age')->nullable();
			$table->text('personal_bio')->nullable();
			$table->text('involvement')->nullable();
			$table->string('photo_url')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team_members');
	}

}
