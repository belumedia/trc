<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
    * log_transaction($from_id, $from_name, $to_id, $from_id, $item_name, $item_id, $amount, $charge_id = "", $refund_id = "") 
    * 
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaction_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
         $table->string("from_id")->nullable();
         $table->string("from_name")->nullable();
         $table->string("to_id")->nullable();
         $table->string("to_name")->nullable();
         $table->string("description")->nullable();
         $table->float("amount")->nullable();
         $table->string("stripe_charge_id")->nullable();
         $table->string("stripe_refund_id")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaction_logs');
	}

}
