<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_bonus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code');
		});

		DB::insert('insert into users_bonus (id, code) values (?, ?)', array(1, 'TM10'));
		DB::insert('insert into users_bonus (id, code) values (?, ?)', array(2, 'MM10'));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_bonus');
	}

}
