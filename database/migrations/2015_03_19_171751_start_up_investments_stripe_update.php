<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StartUpInvestmentsStripeUpdate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//startup_investments
      Schema::table('startup_investments', function($table) {
         $table->string('stripe_chrage_id')->nullable();
         $table->string('stripe_refund_id')->nullable();
      });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//startup_investments
      Schema::table('startup_investments', function($table) {
         $table->dropColumn('stripe_chrage_id');
         $table->dropColumn('stripe_refund_id');
      });
	}

}
