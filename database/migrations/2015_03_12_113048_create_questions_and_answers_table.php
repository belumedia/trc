<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsAndAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('questions_and_answers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
         $table->string('start_up_id');
         $table->integer('question_by_user_id');
         $table->string('question')->nullable();
         $table->string('answer')->nullable();
      });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('questions_and_answers');
	}

}
