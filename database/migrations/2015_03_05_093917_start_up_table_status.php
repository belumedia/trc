<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StartUpTableStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('start_ups', function($table)
		{
			$table->dropColumn('in_review_date');  
         $table->date('in_review_date')->nullable();
		    


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('start_ups', function($table)
		{
			$table->dropColumn('in_review');
			$table->dropColumn('in_review_date');
			
			$table->dropColumn('is_rejected');
			$table->dropColumn('is_rejected_explanation');
			$table->dropColumn('is_rejected_date');
			
			$table->dropColumn('is_approved');
			$table->dropColumn('is_approved_date');
			
			$table->dropColumn('is_open_for_investment');
			$table->dropColumn('is_open_for_investment_date');
			
			$table->dropColumn('is_funded');
			$table->dropColumn('is_funded_date');
			
			$table->dropColumn('is_failed_funding');
			$table->dropColumn('is_failed_funding_date');
			
			$table->dropColumn('is_complete');
			$table->dropColumn('is_complete_date');
			
			$table->dropColumn('is_success_story');		
			$table->dropColumn('is_featured');


		});
	}

}