<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StartUpsTopPickField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//startup_investments
      Schema::table('start_ups', function($table) {
         $table->boolean('top_pick')->default(false);
      });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//startup_investments
      Schema::table('start_ups', function($table) {
         $table->dropColumn('top_pick');
      });

	}

}
