<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStartUps extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('start_ups', function($table)
		{

			$table->string('id');
			$table->timestamps();
			
		    $table->integer('user_id');
		    $table->string('logo_file')->nullable();
			$table->string('full_business_name')->nullable();
		    $table->string('brand_name')->nullable();
		    $table->float('investment_total')->nullable();
		    $table->integer('equity_offered')->nullable();
		    $table->text('business_summuary')->nullable();
		    $table->text('investment_summuary')->nullable();
		    $table->text('business_address')->nullable();
			$table->string('business_postcode')->nullable();
			$table->string('company_number')->nullable();
			$table->date('incorporation_date')->nullable();
			
			$table->text('product_details')->nullable();
		    $table->text('market_impact')->nullable();
		    $table->text('acheivements')->nullable();
			$table->text('monetisation_strategy')->nullable();
		    $table->text('use_of_funds')->nullable();
		    
		    $table->text('market_analysis')->nullable();
		    $table->text('marketing_strategy')->nullable();
		    $table->text('competition_strategy')->nullable();
		    $table->text('relevant_market_experience')->nullable();
			
			$table->string('sector_1_id')->nullable();
		    $table->string('sector_2_id')->nullable();
		    $table->string('sector_3_id')->nullable();
		    
		    
			
			//SEIS / EIS
			$table->boolean('seis_eis_tax_relief')->nullable();
			$table->boolean('eis_shares_compliance')->nullable();
			$table->boolean('controlled_by_another_company')->nullable();
			$table->boolean('own_shares_in_other_company')->nullable();
			$table->boolean('partner_in_other_partnership')->nullable();
			$table->integer('gross_assets')->nullable();
			$table->integer('total_exmployees')->nullable();
			$table->boolean('business_engagement_confirmation')->nullable();
			
			//Checkout 
		    $table->text('stripe_secret_key')->nullable();
		    $table->text('stripe_publishable_key')->nullable();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('start_ups');
	}

}
