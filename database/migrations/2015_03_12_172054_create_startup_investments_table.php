<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartupInvestmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('startup_investments', function(Blueprint $table)
		{
			$table->string('id');
			$table->timestamps();
         
         $table->string('start_up_id');
			$table->integer('investor_id');
			$table->float('amount');
         $table->float('comission')->nullable();
         
         $table->boolean('held')->default(false);
         $table->timestamp('held_date')->nullable();
         $table->boolean('transfered')->default(false);
         $table->timestamp('transfered_date')->nullable();
         $table->boolean('reversed')->default(false);
         $table->timestamp('reversed_date')->nullable();
         
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('startup_investments');
	}

}
