<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StartUpInvestmentsWithStripeCapture extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//startup_investments
      Schema::table('startup_investments', function($table) {
         $table->string('stripe_customer_id')->nullable();
      });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//startup_investments
      Schema::table('startup_investments', function($table) {
         $table->dropColumn('stripe_customer_id');
      });
	}

}
