<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StartUpTableStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
      Schema::table('start_ups', function($table)
		{  
         $table->dropColumn('is_rejected_explanation');

         $table->dropColumn('in_review_date');
         $table->dropColumn('is_rejected_date');
         $table->dropColumn('is_approved_date');
         $table->dropColumn('is_open_for_investment_date');
         $table->dropColumn('is_funded_date');
         $table->dropColumn('is_failed_funding_date');
         $table->dropColumn('is_complete_date');


         $table->date('is_rejected_explanation')->nullable();         
         $table->date('in_review_date')->nullable();
         $table->date('is_rejected_date')->nullable();
         $table->date('is_approved_date')->nullable();
         $table->date('is_open_for_investment_date')->nullable();       			
         $table->date('is_funded_date')->nullable();
         $table->date('is_failed_funding_date')->nullable();
         $table->date('is_complete_date')->nullable();

      });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}

}
