<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartUpReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('start_up_reviews', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->string('user_id');
			$table->string('start_up_id');
			$table->string('rating');
			$table->text('review');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('start_up_reviews');
	}

}
