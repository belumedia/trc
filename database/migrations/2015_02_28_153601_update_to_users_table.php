<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
		    $table->string('first_name');
		    $table->string('last_name');
			$table->string('street');
		    $table->string('street_2')->nullable();
		    $table->string('city');
		    $table->string('county');
		    $table->string('postcode');
		    $table->string('telephone');
		    $table->boolean('accept_terms');
		    
		    $table->dropColumn('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($table)
		{
		    $table->dropColumn('first_name');
		    $table->dropColumn('last_name');
		    $table->dropColumn('street');
		    $table->dropColumn('street_2');
		    $table->dropColumn('city');
		    $table->dropColumn('county');
		    $table->dropColumn('postcode');
		    $table->dropColumn('telephone');
		    $table->dropColumn('accept_terms');
		    
		    $table->string('name');
		    
		});
	}

}
