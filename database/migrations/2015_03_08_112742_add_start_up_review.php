<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartUpReview extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('start_ups', function($table)
		{
		    $table->string('review')->nullable();
		    $table->string('review_score')->nullable();
		    $table->string('review_user_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('start_ups', function($table)
		{
		    $table->dropColumn('review');
		    $table->dropColumn('review_score');
		    $table->dropColumn('review_user_id');
		});
	}

}
