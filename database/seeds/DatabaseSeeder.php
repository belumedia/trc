<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		/* Populate Sectors */
		DB::table('sectors')->delete();
		DB::table('sectors')->insert([
			['value' => 'ARTS', 'name' => 'Arts'],
			['value' => 'CONSUMER', 'name' => 'Consumer Goods'],
			['value' => 'ONLINE_SALES', 'name' => 'Online Sales & E-commerce'],
			['value' => 'EDUCATION', 'name' => 'Education Services'],
			['value' => 'ENTERTAINMENT', 'name' => 'Entertainment'],
			['value' => 'GREEN', 'name' => 'Green Technology'],
			['value' => 'FASHION', 'name' => 'Fashion & Retail'],
			['value' => 'FINANCE', 'name' => 'Finance'],
			['value' => 'FOOD_DRINK', 'name' => 'Food & Drink'],
			['value' => 'GAMING', 'name' => 'Gaming'],
			['value' => 'HEALTHCARE', 'name' => 'Healthcare'],
			['value' => 'MARKETING', 'name' => 'Marketing'],
			['value' => 'MEDIA', 'name' => 'Media'],
			['value' => 'PRO_SERVICES', 'name' => 'Professional Services'],
			['value' => 'SOFTWARE', 'name' => 'Software & Information Technology'],
		]);
		
		DB::table('start_up_statuses')->delete();
		DB::table('start_up_statuses')->insert([
			['value' => 'DRAFT', 'name' => 'Draft'],
			['value' => 'REVIEW', 'name' => 'Review'],
			['value' => 'REJECTED', 'name' => 'Rejected'],
			['value' => 'APPROVED', 'name' => 'Approved'],
			['value' => 'OPEN', 'name' => 'Open For Investment'],
			['value' => 'FUNDED', 'name' => 'Funded'],
			['value' => 'FAILED', 'name' => 'Failed'],
			['value' => 'COMPLETE', 'name' => 'Complete'],
		]);

		
	}

}
