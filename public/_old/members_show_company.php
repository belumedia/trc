<?php include_once("inc_header_members.php"); ?>

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>Notbox UK Ltd.</h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		<a href="#" class="thumbnail">
			<img src="img/sample_company_image.jpg" alt="Company Image">
    	</a>
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">
		
		<h2>Company Details</h2>
		<hr>
		 	 
			<h3>About the Business</h3>
			<p>You can change the details of your course using the form below. A change of course title should be communicated to your students as soon as possible to avoid confusion. For peace of mind, we recommend that you regularly save a copy of your course to your PC. We also keep a history of your last 6 versions in case you need to roll back.</p>
			<p>You can change the details of your course using the form below. A change of course title should be communicated to your students as soon as possible to avoid confusion. For peace of mind, we recommend that you regularly save a copy of your course to your PC. We also keep a history of your last 6 versions in case you need to roll back.</p>
			<p>You can change the details of your course using the form below. A change of course title should be communicated to your students as soon as possible to avoid confusion. For peace of mind, we recommend that you regularly save a copy of your course to your PC. We also keep a history of your last 6 versions in case you need to roll back.</p>
	        
	        <div class="row content-company-profile-financial-row">
				<div class="col-md-4">
					<lead>&pound;888,888</lead><br>
					<small>TARGET</small>
				</div>
				<div class="col-md-4">
					<lead>&pound;888,888</lead><br>
					<small>INVESTED</small>
				</div>
				<div class="col-md-4">
					<lead>88%</lead><br>
					<small>FUNDED</small>
				</div>
			</div>	
		        
	        
	        <hr>
	        <button class="btn btn-primary pull-right">Invest</button>	
	        <button class="btn btn-default">Follow</button>
	        	 
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>
<div class="row content-section"><br></div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">

	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page content-sub-page-top-pad">
		<div role="tabpanel">
		
		  <ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#home" aria-controls="overview" role="tab" data-toggle="tab">Overview</a></li>
		    <li role="presentation"><a href="#profile" aria-controls="team" role="tab" data-toggle="tab">Executive Team</a></li>
		    <li role="presentation"><a href="#messages" aria-controls="qanda" role="tab" data-toggle="tab">Q&amp;As</a></li>
		    <li role="presentation"><a href="#settings" aria-controls="documents" role="tab" data-toggle="tab">Documents</a></li>
			<li role="presentation"><a href="#settings" aria-controls="videos" role="tab" data-toggle="tab">Videos</a></li>
		  </ul>
		  
		  <div class="tab-content">
		    <div role="tabpanel" class="tab-pane active" id="overview">
			    <div class="row">
				    <div class="col-sm-4">
					    <h3>Product or Service Details</h3>
					    <p>You can change the details of your course using the form below. A change of course title should be communicated to your students as soon as possible to avoid confusion. For peace of mind, we recommend that you regularly save a copy of your course to your PC. We also keep a history of your last 6 versions in case you need to roll back.</p>
				    </div>
				    <div class="col-sm-4">
					    <h3>Product or Service Details</h3>
					    <p>You can change the details of your course using the form below. A change of course title should be communicated to your students as soon as possible to avoid confusion. For peace of mind, we recommend that you regularly save a copy of your course to your PC. We also keep a history of your last 6 versions in case you need to roll back.</p>
				    </div>
				    <div class="col-sm-4">
					    <h3>Product or Service Details</h3>
					    <p>You can change the details of your course using the form below. A change of course title should be communicated to your students as soon as possible to avoid confusion. For peace of mind, we recommend that you regularly save a copy of your course to your PC. We also keep a history of your last 6 versions in case you need to roll back.</p>
				    </div>
			    </div>			    
		    </div>
		    <div role="tabpanel" class="tab-pane" id="team">...</div>
		    <div role="tabpanel" class="tab-pane" id="qanda">...</div>
		    <div role="tabpanel" class="tab-pane" id="documents">...</div>
		    <div role="tabpanel" class="tab-pane" id="videos">...</div>
		  </div>
		
		</div>
	</div>
</div>

<?php include_once("inc_footer_member.php"); ?>