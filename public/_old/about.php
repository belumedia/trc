<?php include_once("inc_header.php"); ?>


<div class="row content-section content-section-title">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8"><h1>About Us</h1></div>
	<div class="col-sm-1  col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10  col-lg-8 content-page">
		<p>We help businesses and entrepreneurs find the investors they need, and make sure they are ready to accept investment in compliance with all relevant regulations. We offer a straightforward and effective platform that brings investors and entrepreneurs together to build companies looking to offer potentially large gains for all parties.</p>

		<p>All companies are looked at by our Executive Board of industry veterans and voted on, a majority rule is used to determine who is and who isn't listed on The Right Crowd.</p>
				
		<div class="row">
			<div class="col-sm-6">
				<h2>Mark Elliot FCA</h2>
				<p>Currently Non-Exec Chairman of 21st Century Technology plc, CFO of Enables IT Group plc and a director of EU Supply plc, all AIM listed companies. Work with these companies has included preparation for and a successful fund raising IPO, planning and implementing a reverse takeover and several placing‘s. Also responsible for replacement and recruitment of an executive team and other board positions. Corporate activity has included M&A work in USA and Europe. Originally a partner in Baker Tilly following which several years‘ experience with a small Swiss based private equity operation that included a variety of board roles in private companies.</p>
			</div>
			<div class="col-sm-6">
				<h2>Nigel Payne</h2>
				<p>Mr Payne has 27 years‘ experience at board level, covering a variety of industries: advertising, manufacturing, distribution, retail, finance and e-commerce. He has had wide-ranging exposure to various types of corporate activity, including acquisitions, flotations and fundraising. Nigel is currently Non-Executive Chairman of AIM-traded Hangar8 plc. Nigel was Chief Executive of Sportingbet UK Plc between 2000 and 2006 and a non-executive director from 2006 to 2013. Between 1995 and 2000 Nigel was group finance, business development and IT director of Polestar Magazines, the largest independent printer in Europe (operating in 19 countries). Between 1993 and 1995 Nigel was finance and IT director of Home Brewery Plc, a subsidiary of Scottish & Newcastle Plc.</p>
			</div>	
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<h2>George Rolls</h2>
				<p>Mr Rolls has been a Director, manager and adviser to a number of private companies over the past 30 years in a variety of sectors including manufacturing, print media, technology, aviation and consumer products. He co-founded Beaufort Securities of which he was a director between 1985 and 2006. Since selling his interest in Beaufort Securities in 2006, George has acted as a consultant for private high net worth individuals, assisted with the launch of a software technology fund and sits on the boards of AIM listed Hangar 8 Plc and Volare Aviation Limited as a director. He is also a trustee of the Geoffrey de Havilland Flying Foundation and the Honorary Secretary of The Air Squadron Limited.</p>
			</div>
			<div class="col-sm-6">
				<h2>Andy Walker</h2>
				<p>Andy has over 35 years experience as a manager and director in the UK logistics industry ,initially with Unilever he moved on to Unigate and in 1992 became an executive director and shareholder of a large private company. Currently Managing Director of Gregory Distribution Holdings he has broad knowledge and understanding of a wide range of business sectors gained through experience of working closely with both fledgling owner managed enterprises and large corporate multinationals. He has lead and integrated acquisitions and acted as advisor for business start ups. His attitude to business is based on practical application of good ideas that are realistic and deliverable in meaningful timescales.</p>
			</div>	
		</div>
		
				
		<div class="content-join">
			<a href="join.php">Get Started</a>
		</div>
	</div>
	<div class="col-sm-1"></div>
</div>
	


<?php include_once("inc_footer.php"); ?>