<?php include_once("inc_header_members.php"); ?>

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h1>Your Investments</h1></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<?php include("partial_company_profile_member.php"); ?>
			</div>
			<div class="col-sm-4">
				<?php include("partial_company_profile_member.php"); ?>
			</div>	
			<div class="col-sm-4">
				<?php include("partial_company_profile_member.php"); ?>
			</div>
		</div>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<?php include_once("inc_footer_member.php"); ?>