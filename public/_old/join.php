<?php include_once("inc_header.php"); ?>


<div class="row content-section content-section-spacer-base">
	
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8 content-page">
		
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-3 text-right">
				<h2>Join - Investor</h2>
				<p class="text-muted">We help businesses and entrepreneurs find the investors they need, and make sure they are ready to accept investment in compliance with all relevant regulations.</p>
			</div>
			<div class="col-sm-6">
				
				<br>
			    <form>
				   
			        <div class="form-group">
			            <label for="exampleInputEmail1">First Name</label> <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
			        </div>
			        <div class="form-group">
			            <label for="exampleInputEmail1">Last Name</label> <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
			        </div>
			        <hr>
			        <div class="form-group">
			            <label for="exampleInputEmail1">Street</label> <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
			        </div>
			        <div class="form-group">
			            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="" >
			        </div>
			        <div class="form-group">
			            <label for="exampleInputEmail1">Town</label> <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
			        </div>
			        <div class="form-group">
			            <label for="exampleInputEmail1">County</label> <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
			        </div>
			        <div class="form-group">
			            <label for="exampleInputEmail1">Postcode</label> <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
			        </div>
					<hr>
					<div class="form-group">
			            <label for="exampleInputEmail1">Email Address</label> <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter your email address">
			        </div>
			        <div class="form-group">
			            <label for="exampleInputPassword1">Password</label> <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password 6-10 characters">
			        </div>
	
					<hr>
			        <div class="checkbox">
			            <label><input type="checkbox"> <strong>Yes,</strong> I accept The Right Crowds terms of service.</label>
			        </div>
			        <hr>
			        <button type="submit" class="btn btn-default">Register</button>
			    </form>


				
			</div>
			<div class="col-sm-1"></div>
		</div>

	</div>
	<div class="col-sm-1"></div>
</div>
	


<?php include_once("inc_footer.php"); ?>