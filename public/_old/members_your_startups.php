<?php include_once("inc_header_members.php"); ?>

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>Your Startups</h1>
		<a href="members_start_up_add.php" class="pull-right">Add Startup</a>
		
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Start Up Name</th>
					<th>Status</th>
					<th>Edit</th>
					<th>Delete</th>

				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Untitled - <em>No Name Provided</em></td>
					<td>Draft</td>
					<td><a href="members_start_up_add.php">Edit</a>
					<td><a href="members_start_up_add.php">Delete</a>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<?php include_once("inc_footer_member.php"); ?>