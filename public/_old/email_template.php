<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>[Email Subject] - The Right Crowd</title>
     
<style type="text/css">
     
.ExternalClass {width:100%;} 
 
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%;
    } 
table td {border-collapse:collapse;}    
                
p {margin:0; padding:0; margin-bottom:0;}

h1, h2, h3, h4, h5, h6 {
color: black; 
line-height: 100%; 
}
a, a:link {
color:#2A5DB0;
text-decoration: underline;
}
                 
               /****** END RESETTING DEFAULTS ********/
                
               /****** EDITABLE STYLES        ********/
 
body, #body_style {
    background:#E6E6E6;
    min-height:1000px;
    color:#000;
    font-family:Arial, Helvetica, sans-serif;
    font-size:12px;
} 

span.yshortcuts { color:#E6E6E6; background-color:none; border:none;}
span.yshortcuts:hover,
span.yshortcuts:active,
span.yshortcuts:focus {color:#E6E6E6; background-color:none; border:none;}
                   
/*Optional:*/
a:visited { color: #3c96e2; text-decoration: none} 
a:focus   { color: #3c96e2; text-decoration: underline}  
a:hover   { color: #3c96e2; text-decoration: underline}  
                         
.header-base {
	border-bottom: 10px solid #F2F2F2;
}

.body-text {
	padding: 25px;
}

p {
	font-size: 14px;
	margin-bottom: 20px;
}

.social-links {
	text-align: center;
	background-color: #122D56;
}

.social-links img {
	margin: 0px 5px;
}

.footer-text {
	color: #ffffff;
	padding: 25px;
}

.footer-text p {
	font-size: 12px !important;
}
 
</style>
 
 
</head>
<body style="background:#E6E6E6; min-height:1000px; color:#000; font-family:Arial, Helvetica, sans-serif; font-size:14px" alink="#FF0000" link="#FF0000" bgcolor="#000000" text="#FFFFFF" yahoo="fix"> 
<div id="body_style" style="padding:0px 15px 15px;"> 
        <table cellpadding="15" cellspacing="0" border="0" bgcolor="#ffffff" width="650" align="center">
            <tr>
                <td width="650" class="header-base"><img src="http://whitehousedigital.com/clients/therightcrowd/img/logo_investors.png" border="0"></td>
            </tr>
			<tr>
                <td width="650" class="body-text">
	                <p>Hi Michael,</p>

					<p><strong>Your Username:</strong> mwoolf@mcomi.net</p>
					
					<p>To access your account: <a href="http://www.therightcrowd.com/signin.php">http://www.therightcrowd.com/signin.php</a> </p>
					
					<p>Please ensure that you complete the company Sign up page with as much information as possible to ensure that the potential investors can understand your company and what you are looking to achieve.</p>
					
					<p>A member of The Right Crowd will contact you shortly to talk about your EIS/SEIS needs and Video for the site.</p>
					
					<p>In the mean time for any further requests please don’t hesitate to contact us….(links to site contact us page)</p>
					
					<p>Regards</p><br>
					
					<p>The Right Crowd Team<br>
					therightcrowd.com</p>
	                
                </td>
            </tr>
            
            <tr>
                <td width="650" bgcolor="#0C598B" color="#ffffff" class="social-links">
					<a href="https://twitter.com/TRCFunding" target="_blank"><img src="img/social_twitter.png"></a>
					<a href="https://www.linkedin.com/company/therightcrowd-com" target="_blank"><img src="img/social_linkedin.png"></a>
					<a href="https://www.facebook.com/pages/The-Right-Crowd/1005482409478208" target="_blank"><img src="img/social_facebook.png"></a>
                </td>
            </tr>            
            <tr>
                <td width="650" bgcolor="#0C598B" color="#ffffff" class="footer-text">
	                <p>Copyright © 2015 The Right Crowd All rights reserved. You are receiving this email because you opted in to receive notifications of service updates when you registered for The Right Crowd.</p>
                </td>
            </tr>
        </table> 

    </div> 
 
 
</body>
</html>