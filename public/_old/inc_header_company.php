<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo isset($page_description) ? $page_description : "The Right Crowd"; ?>">
	<meta name="keywords" content="<?php echo isset($page_keywords) ? $page_keywords : "The Right Crowd"; ?>">
	<meta name="author" content="The Right Crowd Ltd.">
    <title><?php echo isset($page_title) ? $page_title : "The Right Crowd"; ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700|Lora:700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/main_company.css" type="text/css">
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container-fluid">
	<div class="row header-top-menu">
		<div class="col-sm-1 col-lg-2"></div>
		<div class="col-sm-10 col-lg-8">
			<ul>
				<li class="header-top-menu-active">Companies</li>
				<li><a href="index.php">Investors</a></li>
			</ul>
		</div>
		<div class="col-sm-1 col-lg-2"></div>
	</div>
	<div class="row header-logo-bar">
		<div class="col-sm-1 col-lg-2"></div>
		<div class="col-sm-10 col-lg-8">
			<img src="img/logo_investors.png">
		</div>
		<div class="col-sm-1 col-lg-2"></div>
	</div>
	<div class="row header-main-menu">
		<div class="col-sm-1 col-lg-2"></div>
		<div class="col-sm-5 col-lg-4">
			<ul>
				<li><a href="company.php">Home</a></li>
				<li><a href="company_info.php">Companies</a></li>
				<li><a href="opportunities_company.php">Opportunities</a></li>
			</ul>
		</div>
		<div class="col-sm-5 col-lg-4">
			<ul class="header-right-menu">
				<li><a href="signin.php">Sign In</a></li>
				<li><a href="join_company.php">Join</a></li>
			</ul>
		</div>
		<div class="col-sm-1 col-lg-2"></div>
	</div>

