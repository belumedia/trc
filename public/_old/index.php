<?php include_once("inc_header.php"); ?>

<div class="row">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  
  <!-- Indicators -->
  <!--
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
  </ol>
  -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active slides slides-sample">
      <div class="carousel-text carousel-text-black">
    	  <h1>Share bonus for the first 10,000 investors</h1>
	      <p class="hidden-xs">1% Increase for the first 10,000 & 3% increase for the first 2,000 investors who invest over £500</p>
      </div>
      <img src="img/slide_3.jpg" class="slides-img" style="width:100%;">
    </div>
    <div class="item  slides slides-sample">
	   <div class="carousel-text">
		   <h1>3 Year Advisory Contract for Startups</h1>
		   <p class="hidden-xs">All companies that complete their capital raising get a 3 year advisory contract.</p>
	   </div>
      <img src="img/slide_1.jpg" class="slides-img" style="width:100%;">
    </div>
    <div class="item  slides slides-sample">
	    <div class="carousel-text carousel-text-black">
			<h1>We are the first in alternative funding</h1>
			<p class="hidden-xs">We bring the best oppurtunities to intelligent investors looking to take control of their portfolios </p>
	    </div>
      <img src="img/slide_2.jpg" class="slides-img" style="width:100%;">
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <img src="img/arrow_left.png" border="0" aria-hidden="true" class="carousel-buttons">
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <img src="img/arrow_right.png" border="0" aria-hidden="true" class="carousel-buttons">
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

<div class="row content-section">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8 content-floating-box-container">
		<div class="row content-floating-box">
			<div class="col-sm-4 content-floating-box-title">
				<h2>We are the future of<br> crowdfunding &amp;<br> co-investment</h2>
			</div>
			<div class="col-sm-8">
				<p>We help businesses and entrepreneurs find the investors they need, and make sure they are ready to accept investment in compliance with all relevant regulations.</p> 
				<p>We offer a straightforward and effective platform that brings investors and entrepreneurs together to build companies looking to offer potentially large gains for all parties</p>
				<ul>
					<li><a href=""><img src="img/bt_icon_tv.png"><br>Watch a Video</a></li>
					<li><a href="company.php"><img src="img/bt_icon_money.png"><br>Raise Capital</a></li>
					<li><a href=""><img src="img/bt_icon_chart.png"><br>Invest Capital</a></li>
					<li><a href="join_company.php"><img src="img/bt_icon_user.png"><br>Get Started</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="clearfix"></div>

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h2>Investment Opportunities</h2></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>	
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>
		</div>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<?php include_once("inc_footer.php"); ?>