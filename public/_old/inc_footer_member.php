<div class="row footer-social-bar">
	<a href="https://twitter.com/TRCFunding" target="_blank"><img src="img/social_twitter.png"></a>
	<a href="https://www.linkedin.com/company/therightcrowd-com" target="_blank"><img src="img/social_linkedin.png"></a>
	<a href="https://www.facebook.com/pages/The-Right-Crowd/1005482409478208" target="_blank"><img src="img/social_facebook.png"></a>
</div>
<div class="row footer-menu">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-2 col-lg-2">
		<ul>
			<li><a href="about.php">About Us</a></li>
			<li><a href="faq.php">FAQs</li>
			<li><a href="http://www.satiogrowth.com" target="_blank">Corporate Finance</li>
		</ul>
	</div>
	<div class="col-sm-2 col-lg-2">
		<ul>
			<li><a href="#">Menu Item #1</a></li>
			<li><a href="#">Menu Item #2</a></li>
			<li><a href="#">Menu Item #3</a></li>
		</ul>
	</div>
	<div class="col-sm-4 col-lg-2"></div>
	<div class="col-sm-2">
		The Right Crowd Ltd.<br>
		38 Richmond Hill<br>
		Bournemouth<br>
		Dorset<br>
		BH4 8HB
	</div>
	<div class="col-sm-1  col-lg-2"></div>
</div>
<div class="row footer-menu footer-copyright">
	<div class="col-sm-1  col-lg-2"></div>
	<div class="col-sm-10  col-lg-8">
		(c) Copyright The Right Crowd Ltd. 2014<br>
		FSA INFORMATION REQUIRED HERE
	</div>
</div>

</div><!-- End of Container Fluid in Header -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-50831514-4', 'auto');
	  ga('send', 'pageview');
	</script>
</body>
</html>