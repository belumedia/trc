<?php include_once("inc_header_members.php"); ?>

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h1>Your Profile - Michael</h1></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<h2>Contact Information</h2>
				<h3>Email Address</h3>
				
				<h3>Telephone</h3>
				
				<h3>Address</h3>
				<p>38 Richmond Hill,<br>
					Bournemouth,<br>
					Dorset<br>
					BH2 6LN</p>
			</div>
			<div class="col-sm-4">
				<h2>Investment Settings</h2>
				<h3>My Investments</h3>
				<p>You are invested in <strong>3</strong> companies at the moment.</p>
				
				<h3>My Payment Method</h3>
				<p><strong>Stripe</strong> all payments will be taken using Stripe.</p>
				
				<h3>Privacy</h3>
				<p><strong>Yes,</strong> make my investments public</p>
			</div>
			<div class="col-sm-4">
				<h2>Account Options</h2>
				
				<a href="" class="btn btn-primary btn-block">Change Contact Details</a><br>
				<a href="" class="btn btn-default btn-block">Update Your Address</a><br>
				<a href="" class="btn btn-default btn-block">Change Password</a><br>
				<a href="" class="btn btn-default btn-block">Change Payment Method</a><br>
				<a href="" class="btn btn-default btn-block">Set Privacy Settings</a>
			</div>
		</div>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">


	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<?php include_once("inc_footer_member.php"); ?>