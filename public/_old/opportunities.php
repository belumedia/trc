<?php include_once("inc_header.php"); ?>


<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h1>Opportunities</h1><p>Take a look at some of the businesses on The Right Crowd.</p></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>	
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>	
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>	
			<div class="col-sm-4">
				<?php include("partial_company_profile.php"); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<?php include_once("inc_footer.php"); ?>