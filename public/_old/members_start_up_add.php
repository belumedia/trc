<?php include_once("inc_header_members.php"); ?>

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<h1>Untitled - <em>No name provided</em></h1>
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-4 col-lg-2">
		
		<ul class="list-group">
		  <li class="list-group-item"><a href="members_start_up_add.php">Company Details</a></li>
		  <li class="list-group-item"><a href="#" class="text-muted"><span class="glyphicon glyphicon-ok"></span> Business Plan</a></li>
		  <li class="list-group-item"><a href="#" class="text-muted"><span class="glyphicon glyphicon-ok"></span> Market Research</a></li>
		  <li class="list-group-item"><a href="#" class="text-muted"><span class="glyphicon glyphicon-ok"></span> Executive Team</a></li>
		  <li class="list-group-item"><a href="#">Document Upload</a></li>
		  <li class="list-group-item"><a href="#">NDA</a></li>
		  <li class="list-group-item"><a href="#">SEIS + EIS</a></li>
		  <li class="list-group-item"><a href="#">Bank Account</a></li>
		</ul>
		
	</div>
	<div class="col-sm-8 col-lg-6 content-sub-page">
		
		<h2>Company Details</h2>
		<hr>
		 <form>
			 
			<h3>About the Business</h3>
	        <div class="form-group">
	            <label for="exampleInputEmail1">Full Business Name</label> 
	            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Acme Inc.">
				<span id="helpBlock" class="help-block">Full Business Name including limited company status</span>
	        </div>
	        <div class="form-group">
	            <label for="exampleInputEmail1">Brand Name</label> 
	            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
	        </div>
	        
			<hr>
	        <h3>Investment Requirements</h3>
	        <div class="form-group">
	            <label for="exampleInputEmail1">Investment sought</label> 
	            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="£100,000">
	            <span id="helpBlock" class="help-block">Investment sought in &pound; (GBP) and displayed as full sum</span>
	        </div>
	        <div class="form-group">
	            <label for="exampleInputEmail1">Equity offered</label> 
	            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="30%">
	            <span id="helpBlock" class="help-block">Equity offered as a percentage of total share</span>
	        </div>
	        
	        
	        
	        <hr>
	        <button class="btn btn-default pull-right">Save &amp; Exit</button>
	        <button class="btn btn-primary">Save &amp; Continue</button>

		 </form>
		
	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<?php include_once("inc_footer_member.php"); ?>