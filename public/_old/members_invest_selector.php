<?php include_once("inc_header_members.php"); ?>

<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h1>Welcome Members Name</h1><p>You 1 new notification.</p></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>


<div class="row content-section content-section-title">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8"><h2>Your Investments</h2></div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<div class="row content-section content-section-spacer-base">
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-10 col-lg-8">
		<div class="row">
			<div class="col-sm-4">
				<div class="panel panel-default">
				  <div class="panel-body">
				   	<h2>Self Certify - High Net Worth</h2>
				   	<p>I confirm for the purposes of the Financial Conduct Authority that I am a High Net Worth Investor.</p>
				  </div>
				</div>
			</div>
			<div class="col-sm-4">
				<?php include("partial_company_profile_member.php"); ?>
			</div>	
			<div class="col-sm-4">
				<?php include("partial_company_profile_member.php"); ?>
			</div>
		</div>

	</div>
	<div class="col-sm-1 col-lg-2"></div>
</div>

<?php include_once("inc_footer_member.php"); ?>